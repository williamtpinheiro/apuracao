<?php
require($_SERVER['DOCUMENT_ROOT'].'/apuracao/library/adodb/adodb.inc.php');
/**
* Metodo responsável em conectar com o banco de dados.
* @name AdodbConnect
* @package com.algar.app-config
* @return $db Variável com a conexão.
*/

function PegaConexao() {
	// Construtor da Classe. Estes automaticamente são definidos a cada nova instância.
	$db = NewADOConnection("mysql");
	$db->Connect('localhost', 'root', '','unimedbh_wpuapuracao');
	$db->debug=false;
	//ATENÇÃO! O arquivo /library/phpjasperxml_0.8/setting.php também deverá conter os dados de conexão com o banco de dados para que os relátorios sejam gerados com sucesso. 
	return $db;
}

?>