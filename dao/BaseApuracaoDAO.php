<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/dao/GruposEspecialidadesDAO.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/dao/LocaisVotacaoDAO.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/model/vo/Apuracao.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/model/ApuracaoCandidato.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/app-config/AdodbConnect.php"); 
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/model/vo/AcompanhaOnline.php");
	
	/**
	* Classe de acesso ao banco de dados referente à Base de Apuração
	* @name BaseApuracaoDAO
	* @version v 1.2 24/03/2014
	* @package com.algartecnologia.dao
	* @access public
	*/	
	Class BaseApuracaoDAO {
 			
		/**
		* Metodo responsável em consutlar se já existe uma base de apuração gerada.
		* @name verificarExistenciaApuracao
		* @return $existeApuracao Int
		*/
		function verificarExistenciaApuracao() {  		  
			$existeApuracao=0;
			$db = PegaConexao();
			$query = "select * from AP_APURACAO_LOCAL LIMIT 1";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {   			
					$existeApuracao++;
				}
			}
			return $existeApuracao;  
	 	}
		
		/**
		* Metodo responsável em consultar os locais de votação na apuração atual.
		* @name verificarExistenciaLocaisApuracao
		* @return $locaisVotacaoArr LocalVotacao
		*/
		function verificarExistenciaLocaisApuracao() {  		  
			$locaisVotacaoArr;
			$db = PegaConexao();
			$query = "select DISTINCT(L.ID_LOCAL), L.DES_LOCAL, L.CIDADE, L.QTD_PRESENTES
						from AP_LOCAL L,
							 AP_APURACAO_LOCAL AL
						where L.ID_LOCAL = AL.ID_LOCAL ORDER BY L.DES_LOCAL";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {   			
					$localVotacao = new LocalVotacao();
					$localVotacao->setIdLocal($row['ID_LOCAL']);
					$localVotacao->setDesLocal($row['DES_LOCAL']);
					$localVotacao->setCidade($row['CIDADE']);
					$localVotacao->setQtdPresentes($row['QTD_PRESENTES']);
					$locaisVotacaoArr[] = $localVotacao;
				}
			}
			return $locaisVotacaoArr;  
	 	}
	
		/**
		* Metodo responsável em gerar a base de apuração na base de dados.
		* @name gerarBaseApuracao
		* @return Int
		*/
		function gerarBaseApuracao() {
			//Recupero a lista de Grupos de Especialidades cadastradas.
			$daoEspecialidades = new GruposEspecialidadesDAO();
			$listaEspecialidades = $daoEspecialidades->listarGruposEspecialidades();
			//Recupero a lista de Locais de Votação cadastrados.
			$daoLocais = new LocaisVotacaoDAO();
			$listaLocais = $daoLocais->listarLocaisVotacao();
			//Abro uma conexão
			$db = PegaConexao();
			//Seto variável de controle do processo
			$ocorreuErro=0;
			//Para cada local insiro todas as especialidades disponíveis
			foreach($listaLocais as $local){
				foreach($listaEspecialidades as $especialidade){
					$idLocal = $local->getIdLocal();
					$idEspecialidade = $especialidade->getIdEspecialidade();
					$query = "INSERT INTO AP_APURACAO_LOCAL (ID_LOCAL, ID_ESPECIALIDADE, QTD_VOTOS_BRANCOS, QTD_VOTOS_NULOS, QTD_CEDULAS_ENVIADAS, QTD_CEDULAS_DEVOLVIDAS, QTD_CEDULAS_INVALIDADAS) VALUES ($idLocal, $idEspecialidade, 0, 0, 0, 0, 0)";
					$result = $db->Execute($query);
					if(empty($result)){
						$ocorreuErro=1;
					}
				}
			}
			//Para cada Local de Apuração cadastros os médicos de acordo com suas especialidades.
			//Recupero todos os locais cadastrados na apuração
			$query = "SELECT * FROM AP_APURACAO_LOCAL";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {
					//Para cada local verifico quais os médicos vinculados a especialidade
					$idEspecialidadeLocal = $row['ID_ESPECIALIDADE'];
					$idApuracaoLocal = $row['ID_APURACAO_LOC'];
					$queryA = "SELECT * FROM AP_CANDIDATO_ESPECIALIDADE WHERE ID_ESPECIALIDADE = $idEspecialidadeLocal";
					$resultA = $db->Execute($queryA);
					if(!empty($resultA)){
						//Para cada médico encontrado, cadastro ele na apuração.
						foreach ($resultA as $rowA) {
							$crm = $rowA['CRM'];
							$queryB = "INSERT INTO AP_APURACAO_CANDID (ID_APURACAO_LOC, CRM, QTD_VOTOS_CANDID) VALUES ($idApuracaoLocal, $crm, 0)";
							$resultB = $db->Execute($queryB);
							if(empty($resultB)){
								$ocorreuErro=1;
							}
						}
					}
				}
			}
			//Verifico se ocorreu algum erro, caso positivo apago a base de apuração que gravou até o erro.
			if($ocorreuErro){
				$queryC = "DELETE FROM AP_APURACAO_CANDID";
				$resultC = $db->Execute($queryC);
				$queryD = "DELETE FROM AP_APURACAO_LOCAL";
				$resultD = $db->Execute($queryD);
			}
			
			return $ocorreuErro;
			
		}
		
		/**
		* Metodo responsável em excluir a base de votação na base de dados.
		* @name excluirBaseApuracao
		* @return Int
		*/
		function excluirBaseApuracao() {
			$db = PegaConexao();
			
			//Inicio uma transação controlada
			$db->StartTrans();
			//Seto variável de controle do processo
			$ocorreuErro=0;
			//Deleto todos os registros de apuração dos candidatos
			$query = "DELETE FROM AP_APURACAO_CANDID";
			$result = $db->Execute($query);
			if(empty($result)){
				$db->FailTrans();
				$ocorreuErro=1;
			}
			//Deleto todos os locais de apuração
			$queryA = "DELETE FROM AP_APURACAO_LOCAL";
			$resultA = $db->Execute($queryA);
			if(empty($resultA)){
				$db->FailTrans();
				$ocorreuErro=1;
			}
			//Este metodo faz a verificação, caso não ocorreu nenhum erro ele realiza o commit, caso ele encontre algum erro ou caia em alguma excessão forçada pelo comando FailTrans, ele realiza o roolback da transação ativa.
			$db->CompleteTrans();
			
			return $ocorreuErro;
			
		}
		
		/**
		* Metodo responsável em carregar a apuração de um determinado local de votação e especialidade.
		* @param $idLocal Identificador do Local de Votação que deseja Apurar
		* @param $idEspecialidade Identificador do Grupo de Especialidade que deseja apurar de um determinado Local de Votação
		* @name carregarApuracao
		* @return $apuracao Apuracao
		*/
		function carregarApuracao($idLocal = null, $idEspecialidade = null) {  		  
			$apuracao = new Apuracao();
			$db = PegaConexao();
			//Faço a busca pelo Local e Grupo de Especialidade passados como parâmetro.
			$query = "SELECT AL.ID_APURACAO_LOC, AL.ID_LOCAL, AL.ID_ESPECIALIDADE, AL.QTD_VOTOS_NULOS, AL.QTD_VOTOS_BRANCOS, L.QTD_PRESENTES, QTD_CEDULAS_ENVIADAS, QTD_CEDULAS_DEVOLVIDAS, QTD_CEDULAS_INVALIDADAS, NUM_MESA_APURACAO, QTD_VOTANTES_SESSAO, BIT_STATUS 
					  FROM AP_APURACAO_LOCAL AL,
					  	   AP_LOCAL L
					  WHERE AL.ID_LOCAL = L.ID_LOCAL
					  	AND AL.ID_LOCAL = $idLocal
						AND AL.ID_ESPECIALIDADE = $idEspecialidade";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {   			
					$apuracao->setIdApuracaoLoc($row['ID_APURACAO_LOC']);
					$apuracao->setIdLocal($row['ID_LOCAL']);
					$apuracao->setIdEspecialidade($row['ID_ESPECIALIDADE']);
					$apuracao->setNumVotosNulos($row['QTD_VOTOS_NULOS']);
					$apuracao->setNumVotosBrancos($row['QTD_VOTOS_BRANCOS']);
					$apuracao->setNumVotosCPE($row['QTD_PRESENTES']);
					$apuracao->setNumVotosValidos(0);
					$apuracao->setNumVotosLocal(0);
					$apuracao->setQtdCedulasEnviadas($row['QTD_CEDULAS_ENVIADAS']);
					$apuracao->setQtdCedulasDevolvidas($row['QTD_CEDULAS_DEVOLVIDAS']);
					$apuracao->setQtdCedulasInvalidadas($row['QTD_CEDULAS_INVALIDADAS']);
					$apuracao->setNumMesaApuracao($row['NUM_MESA_APURACAO']);
					$apuracao->setQtdVotantesSessao($row['QTD_VOTANTES_SESSAO']);
					$apuracao->setBitStatus($row['BIT_STATUS']);
					//Faço a busca dos Candidatos do Local selecionado e faço a contagem de votos validos
					$queryCandidatos = "SELECT AC.ID_APURACAO_CADID, AC.CRM, C.NOME, AC.QTD_VOTOS_CANDID
					  FROM AP_APURACAO_CANDID AC,
					  	   AP_CANDIDATO C
					  WHERE AC.CRM = C.CRM
					  	AND AC.ID_APURACAO_LOC = ".$apuracao->getIdApuracaoLoc();
					$resultCandidatos = $db->Execute($queryCandidatos);
									
					if(!empty($resultCandidatos)){
						$candidatosArr;
						foreach ($resultCandidatos as $rowCandidato) {   	
							$candidato = new ApuracaoCandidato();
							$candidato->setIdApuracaoCadid($rowCandidato['ID_APURACAO_CADID']);
							$candidato->setCRM($rowCandidato['CRM']);
							$candidato->setNome($rowCandidato['NOME']);
							$candidato->setQtdVotosCandid($rowCandidato['QTD_VOTOS_CANDID']);
							$apuracao->setNumVotosValidos($apuracao->getNumVotosValidos()+$candidato->getQtdVotosCandid());
							$candidatosArr[] = $candidato;
						}
						$apuracao->setListaCandidatosArr($candidatosArr);
					}
					//Faço a soma dos votos validos contabilizados no local selecionado.
					$querySomaVotosValidosLocal = "SELECT SUM(AC.QTD_VOTOS_CANDID) TOTAL_VOTOS_VALIDOS_LOCAL
						FROM AP_APURACAO_CANDID AC,
							 AP_APURACAO_LOCAL AL
						WHERE AC.ID_APURACAO_LOC = AL.ID_APURACAO_LOC
						AND AL.ID_LOCAL = ".$idLocal;
					$resultSomaVotosValidosLocal = $db->Execute($querySomaVotosValidosLocal);
					$somaVotosLocal = 0;				
					if(!empty($resultSomaVotosValidosLocal)){
						foreach ($resultSomaVotosValidosLocal as $rowResultadoValido) {   	
							$somaVotosLocal += $rowResultadoValido['TOTAL_VOTOS_VALIDOS_LOCAL'];
						}
					}
					//Faço a soma dos votos invalidados contabilizados no local selecionado.
					$querySomaVotosInvalidosLocal = "SELECT SUM(QTD_VOTOS_NULOS) + SUM(QTD_VOTOS_BRANCOS) TOTAL_VOTOS_INVALIDOS_LOCAL
					FROM AP_APURACAO_LOCAL AL
					WHERE AL.ID_LOCAL = ".$idLocal;
					$resultSomaVotosInvalidosLocal = $db->Execute($querySomaVotosInvalidosLocal);				
					if(!empty($resultSomaVotosInvalidosLocal)){
						foreach ($resultSomaVotosInvalidosLocal as $rowResultadoInvalido) {   	
							$somaVotosLocal += $rowResultadoInvalido['TOTAL_VOTOS_INVALIDOS_LOCAL'];
						}
					}
					//Seto o resultado da soma dos votos validos e invalidos do local de votação selecionado.
					$apuracao->setNumVotosLocal($somaVotosLocal);
					//Contabilizo os votos geral do local da especialidade selecionada
					$apuracao->setNumVotosGeral($apuracao->getNumVotosNulos()+$apuracao->getNumVotosBrancos()+$apuracao->getNumVotosValidos());
				}
			}
			return $apuracao;  
	 	}
		
		
		/**
		* Metodo responsável em carregar um candidato de uma determinada apuração e local.
		* @name consultarCandidatoByApuracaoLocal
		* @idApuracaoCadid Idendificador do Candidato no Local de Apuracao que deseja recuperar.
		* @return $apuracaoCandidato ApuracaoCandidato
		*/
		function consultarCandidatoByApuracaoLocal($idApuracaoCadid) {  		  
			$apuracaoCandidato = new ApuracaoCandidato();
			$db = PegaConexao();
			$query = "SELECT AC.ID_APURACAO_CADID, AC.CRM, C.NOME, AC.QTD_VOTOS_CANDID
					  FROM AP_APURACAO_CANDID AC,
					  	   AP_CANDIDATO C
					  WHERE AC.CRM = C.CRM
					  	AND AC.ID_APURACAO_CADID =  $idApuracaoCadid";	  
			//echo $query;
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {   			
					$apuracaoCandidato = new ApuracaoCandidato();
					$apuracaoCandidato->setIdApuracaoCadid($row['ID_APURACAO_CADID']);
					$apuracaoCandidato->setCRM($row['CRM']);
					$apuracaoCandidato->setNome($row['NOME']);
					$apuracaoCandidato->setQtdVotosCandid($row['QTD_VOTOS_CANDID']);
				}
			}
			return $apuracaoCandidato;  
	 	}
		
		
		/**
		* Metodo responsável em atualizar os votos do local de votação de determinada especialidade.
		* @param $idLocal Identificador do Local de Votação
		* @param $idEspecialidade Identificador do Grupo de Especialidade
		* @param $qtdVotosNulos Quatidade de votos nulos desta condição
		* @param $qtdVotosBrancos Quantidade de votos brancos desta condição
		* @param $qtdCedulasEnviadas Quantidade de cédulas enviadas para o local de votação
		* @param $qtdCedulasDevolvidas Quantidade de cédulas devolvidas sem utilização
		* @param $qtdCedulasInvalidadas Quantidade de cédulas invalidadas durante a votação
		* @param $numMesaApuracao Número da mesa responsável pela contabilização dos votos durante a apuração
		* @param $qtdVotantesSessao Quantidade de Votantes na Sessão Eleitoral
		* @param $bitStatus Status se a Sessão Eleitoral está válida. 1 = Válida 0 = Nula
		* @name salvarApuracaoLocal
		* @return Int
		*/
		function salvarApuracaoLocal($idLocal, $idEspecialidade, $qtdVotosNulos, $qtdVotosBrancos, $qtdCedulasEnviadas, $qtdCedulasDevolvidas, $qtdCedulasInvalidadas, $numMesaApuracao, $qtdVotantesSessao, $bitStatus) {
			$db = PegaConexao();
			$query = "UPDATE AP_APURACAO_LOCAL SET QTD_VOTOS_NULOS = $qtdVotosNulos, QTD_VOTOS_BRANCOS = $qtdVotosBrancos, QTD_CEDULAS_ENVIADAS = $qtdCedulasEnviadas, QTD_CEDULAS_DEVOLVIDAS = $qtdCedulasDevolvidas, QTD_CEDULAS_INVALIDADAS = $qtdCedulasInvalidadas, NUM_MESA_APURACAO = $numMesaApuracao, QTD_VOTANTES_SESSAO = $qtdVotantesSessao, BIT_STATUS = $bitStatus WHERE ID_LOCAL = $idLocal AND ID_ESPECIALIDADE = $idEspecialidade";	
			$result = $db->Execute($query);
			if(!empty($result)){
				return 1;
			} else {
				return 0;
			} 
		}
		
		/**
		* Metodo responsável em atualizar os votos do candidato vinculado a um local de votação de determinada especialidade.
		* @param $idApuracaoCadid Identificador do Candidato na Apuração desta condição
		* @param $qtdVotosCandid Quatidade de votos válidos desta condição para o candidato
		* @name salvarApuracaoCandidato
		* @return Int
		*/
		function salvarApuracaoCandidato($idApuracaoCadid, $qtdVotosCandid) {
			$db = PegaConexao();
			$query = "UPDATE AP_APURACAO_CANDID SET QTD_VOTOS_CANDID = $qtdVotosCandid WHERE ID_APURACAO_CADID = $idApuracaoCadid";	
			$result = $db->Execute($query);
			if(!empty($result)){
				return 1;
			} else {
				return 0;
			} 
		}
		
		/**
		* Metodo responsável em carregar a lista de quantidade de votos nulos por local.
		* @name listarVotosNulosLocais
		* @return $votosNulosLocaisArr
		*/
		function listarVotosNulosLocais() {  		  
			$votosNulosLocaisArr;
			$db = PegaConexao();
			$query = "SELECT SUM( AL.QTD_VOTOS_NULOS ) VOTOS_NULOS_LOCAL
					  FROM AP_APURACAO_LOCAL AL, AP_LOCAL L
					  WHERE AL.ID_LOCAL = L.ID_LOCAL
					  GROUP BY AL.ID_LOCAL
					  ORDER BY L.DES_LOCAL";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {   			
					$votosNulosLocaisArr[] = $row['VOTOS_NULOS_LOCAL'];
				}
			}
			return $votosNulosLocaisArr;  
	 	}
		
		/**
		* Metodo responsável em carregar a lista de quantidade de votos brancos por local.
		* @name listarVotosBrancosLocais
		* @return $votosBrancosLocaisArr
		*/
		function listarVotosBrancosLocais() {  		  
			$votosBrancosLocaisArr;
			$db = PegaConexao();
			$query = "SELECT SUM( AL.QTD_VOTOS_BRANCOS ) VOTOS_BRANCOS_LOCAL
					  FROM AP_APURACAO_LOCAL AL, AP_LOCAL L
					  WHERE AL.ID_LOCAL = L.ID_LOCAL
					  GROUP BY AL.ID_LOCAL
					  ORDER BY L.DES_LOCAL";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {   			
					$votosBrancosLocaisArr[] = $row['VOTOS_BRANCOS_LOCAL'];
				}
			}
			return $votosBrancosLocaisArr;  
	 	}
	 	
	 	/**
		* Metodo responsável em carregar a situação atual da apuração.
		* @name consultarSituacaoApuracao
		* @return $acompanhaOnline acompanhaOnline
		*/
		function consultarSituacaoApuracao() {  		  
			$acompanhaOnline = new AcompanhaOnline();
			$db = PegaConexao();
			$query = "select
					E.ID_ESPECIALIDADE ID_ESPECIALIDADE,
					E.DES_ESPECIALIDADE DES_ESPECIALIDADE,
					E.QTD_VAGAS QTD_VAGAS,
					(SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM 	AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 1) TOTAL_CHAPA_1,
					(CONCAT(REPLACE(FORMAT(((SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM 	AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 1)*100 /
					    ((SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 1) +
						(SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM 	AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 2) +
						(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE) +
						(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE))),2),'.',','),' %')) TOTAL_PERC_CHAPA_1,
					(SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM 	AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 2) TOTAL_CHAPA_2,
					(CONCAT(REPLACE(FORMAT(((SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM 	AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 2)*100 /
					    ((SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 1) +
						(SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM 	AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 2) +
						(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE) +
						(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE))),2),'.',','),' %')) TOTAL_PERC_CHAPA_2,
					((SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM 	AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 1) +
					(SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM 	AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 2)) TOTAL_VALIDOS,
					(CONCAT(REPLACE(FORMAT((((SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM 	AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 1) +
						(SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM 	AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 2))*100 /
					    ((SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 1) +
						(SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM 	AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 2) +
						(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE) +
						(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE))),2),'.',','),' %')) TOTAL_PERC_VALIDOS,
					(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE) TOTAL_VOTOS_BRANCOS,
					(CONCAT(REPLACE(FORMAT(((SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE)*100 /
					    ((SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 1) +
						(SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM 	AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 2) +
						(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE) +
						(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE))),2),'.',','),' %')) TOTAL_PERC_BRANCOS,
					(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE) TOTAL_VOTOS_NULOS,
					(CONCAT(REPLACE(FORMAT(((SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE)*100 /
					    ((SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 1) +
						(SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM 	AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 2) +
						(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE) +
						(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE))),2),'.',','),' %')) TOTAL_PERC_NULOS,
					((SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 1) +
						(SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM 	AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 2) +
						(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE) +
						(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE)) SOMA_TOTAL,
					(CONCAT(REPLACE(FORMAT((((SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 1) +
						(SELECT SUM(AC1.QTD_VOTOS_CANDID) FROM 	AP_APURACAO_CANDID AC1 WHERE AC1.CRM = 2) +
						(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE  AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE) +
						(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE)) /
						(SELECT SUM(L1.QTD_PRESENTES) from AP_LOCAL L1))*100,2),'.',','),' %')) SOMA_PERC_TOTAL
				from AP_ESPECIALIDADE E";	  
			//echo $query;
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {   		
					$acompanhaOnline = new AcompanhaOnline();
					$acompanhaOnline->setIdEspecialidade($row['ID_ESPECIALIDADE']);
					$acompanhaOnline->setDesEspecialidade($row['DES_ESPECIALIDADE']);
					$acompanhaOnline->setQtdVagas($row['QTD_VAGAS']);
					$acompanhaOnline->setTotalChapa1($row['TOTAL_CHAPA_1']);
					$acompanhaOnline->setTotalPercChapa1($row['TOTAL_PERC_CHAPA_1']);
					$acompanhaOnline->setTotalChapa2($row['TOTAL_CHAPA_2']);
					$acompanhaOnline->setTotalPercChapa2($row['TOTAL_PERC_CHAPA_2']);
					$acompanhaOnline->setTotalValidos($row['TOTAL_VALIDOS']);
					$acompanhaOnline->setTotalPercValidos($row['TOTAL_PERC_VALIDOS']);
					$acompanhaOnline->setTotalBrancos($row['TOTAL_VOTOS_BRANCOS']);
					$acompanhaOnline->setTotalPercBrancos($row['TOTAL_PERC_BRANCOS']);
					$acompanhaOnline->setTotalNulos($row['TOTAL_VOTOS_NULOS']);
					$acompanhaOnline->setTotalPercNulos($row['TOTAL_PERC_NULOS']);
					$acompanhaOnline->setSomaTotal($row['SOMA_TOTAL']);
					$acompanhaOnline->setSomaPercTotal($row['SOMA_PERC_TOTAL']);
				}
			}
			return $acompanhaOnline;  
	 	}
	}

?>