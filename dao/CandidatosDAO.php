<?php
 	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/model/Candidato.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/app-config/AdodbConnect.php");

	/**
	* Classe de acesso ao banco de dados referente � Candidatos
	* @name CandidatosDAO
	* @version v 1.0 20/08/2011
	* @package com.algartecnologia.dao
	* @access public
	*/
	Class CandidatosDAO {

		/**
		* Metodo respons�vel em carregar um candidato cadastrado na base de dados.
		* @name consultarCandidatoByCRM
		* @crm CRM do Candidato que deseja recuperar.
		* @return $candidato Candidato
		*/
		function consultarCandidatoByCRM($crm) {
			$candidato = new Candidato();
			$db = PegaConexao();
			$query = "select * from AP_CANDIDATO where CRM = $crm";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {					
					$candidato->setCRM($row['CRM']);
					$candidato->setNome($row['NOME']);
					$candidato->setDatCooperacao(data($row['DAT_COOPERACAO']));
					$candidato->setDatNascimento(data($row['DAT_NASCIMENTO']));
				}
			}
			return $candidato;
	 	}

		/**
		* Metodo respons�vel em consultar se j� existe um candidato com o mesmo CRM cadastrado, em caso de edi��o ele desconsidera o registro que est� sendo atualizado.
		* @name verificarExistenciaCandidato
		* @param $crm CRM do Candidato que esta sendo atualizado ou inclu�do.
		* @return $candidatosArr Candidato
		*/
		function verificarExistenciaCandidato($crm) {
			$candidatosArr = array();
			$db = PegaConexao();
			$query = "select * from AP_CANDIDATO where CRM = $crm";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {
					$candidato = new Candidato();
					$candidato->setCRM($row['CRM']);
					$candidato->setNome($row['NOME']);
					$candidato->setDatCooperacao(data($row['DAT_COOPERACAO']));
					$candidato->setDatNascimento(data($row['DAT_NASCIMENTO']));
					$candidatosArr[] = $candidato;
				}
			}
			return $candidatosArr;
	 	}

		/**
		* Metodo respons�vel em consultar se j� existe uma apura��o para um determinado candidato.
		* @name verificarExistenciaApuracao
		* @param $crm CRM do Candidato que est� sendo verificado.
		* @return $qtdApuracaoVinculadas Int
		*/
		function verificarExistenciaApuracao($crm) {
			$qtdApuracaoVinculadas=0;
			$db = PegaConexao();
			$query = "select * from AP_APURACAO_CANDID where CRM = $crm";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {
					$qtdApuracaoVinculadas++;
				}
			}
			return $qtdApuracaoVinculadas;
	 	}

		/**
		* Metodo respons�vel em consultar se j� existe uma especialidade para um determinado candidato.
		* @name verificarExistenciaEspecialidade
		* @param $crm CRM do Candidato que est� sendo verificado.
		* @return $qtdEspecialidadesVinculadas Int
		*/
		function verificarExistenciaEspecialidade($crm) {
			$qtdEspecialidadesVinculadas=0;
			$db = PegaConexao();
			$query = "select * from AP_CANDIDATO_ESPECIALIDADE where CRM = $crm";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {
					$qtdEspecialidadesVinculadas++;
				}
			}
			return $qtdEspecialidadesVinculadas;
	 	}

		/**
		* Metodo respons�vel em carregar a lista de candidatos cadastrado na base de dados.
		* @param $crm Informado quando deseja filtrar algum crm
		* @param $nome Informado quando deseja filtrar algum nome
		* @name listarCandidatos
		* @return $candidatosArr Candidato
		*/
		function listarCandidatos($crm = null, $nome = null) {
			$candidatosArr = array();
			$db = PegaConexao();
			$query = "select * from AP_CANDIDATO WHERE 1=1";
			if(!is_null($crm)){
				$query .= " AND CRM = $crm";
			}
			if(!is_null($nome)){
				$nome = strtoupper($nome);
				$query .= " AND UPPER(NOME) LIKE '%$nome%'";
			}
			$query .= " order by NOME";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {
					$candidato = new Candidato();
					$candidato->setCRM($row['CRM']);
					$candidato->setNome($row['NOME']);
					$candidato->setDatCooperacao(data($row['DAT_COOPERACAO']));
					$candidato->setDatNascimento(data($row['DAT_NASCIMENTO']));
					$candidatosArr[] = $candidato;
				}
			}
			return $candidatosArr;
	 	}

		/**
		* Metodo respons�vel em atualizar um candidato cadastrado na base de dados.
		* @param $crm CRM do Candidato
		* @param $nome Nome do Candidato
		* @param $datCooperacao Data de Coopera��o
		* @param $datNascimento Data de Nascimento
		* @name atualizarCandidato
		* @return Int
		*/
		function atualizarCandidato($crm, $nome, $datCooperacao, $datNascimento) {
			$db = PegaConexao();
			$datCooperacao = data2($datCooperacao);
			$datNascimento = data2($datNascimento);
			$query = "UPDATE AP_CANDIDATO SET NOME = '$nome', DAT_COOPERACAO = '$datCooperacao', DAT_NASCIMENTO = '$datNascimento' WHERE CRM = $crm";
			$result = $db->Execute($query);
			if(!empty($result)){
				return 1;
			} else {
				return 0;
			}
		}

		/**
		* Metodo respons�vel em incluir um candidato na base de dados.
		* @param $crm CRM do Candidato
		* @param $nome Nome do Candidato
		* @param $datCooperacao Data de Coopera��o
		* @param $datNascimento Data de Nascimento
		* @name incluirCandidato
		* @return Int
		*/
		function incluirCandidato($crm, $nome, $datCooperacao, $datNascimento) {
			$db = PegaConexao();
			$datCooperacao = data2($datCooperacao);
			$datNascimento = data2($datNascimento);
			$query = "INSERT INTO AP_CANDIDATO(CRM, NOME, DAT_COOPERACAO, DAT_NASCIMENTO) VALUES ($crm,'$nome', '$datCooperacao', '$datNascimento')";
			$result = $db->Execute($query);
			if(!empty($result)){
				return 1;
			} else {
				return 0;
			}
		}

		/**
		* Metodo respons�vel em excluir um candidato na base de dados.
		* @param $crm CRM do Candidato
		* @name excluirCandidato
		* @return Int
		*/
		function excluirCandidato($crm) {
			$db = PegaConexao();
			$query = "DELETE FROM AP_CANDIDATO WHERE CRM = $crm";
			$result = $db->Execute($query);
			if(!empty($result)){
				return 1;
			} else {
				return 0;
			}
		}
	}

?>