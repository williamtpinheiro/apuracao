<?php
 	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/model/Cargo.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/app-config/AdodbConnect.php"); 
	
	/**
	* Classe de acesso ao banco de dados referente � Grupo de Especialidades
	* @name CargosDAO
	* @version v 1.0
	* @package com.algartecnologia.dao
	* @access public
	*/	
	Class CargosDAO {
 		
		/**
		* Metodo respons�vel em carregar um grupo de especialidade cadastrado na base de dados.
		* @name consultarGrupoEspecialidadeByIdEspecialidade
		* @idGrupoEspecialidade ID do Grupo de Especialidade que deseja recuperar.
		* @return $grupoEspecialidade GrupoEspecialidade
		*/
		function consultarGrupoEspecialidadeByIdEspecialidade($idGrupoEspecialidade) {  		  
			$db = PegaConexao();
			$query = "select * from AP_ESPECIALIDADE where ID_ESPECIALIDADE = $idGrupoEspecialidade";	  
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {   			
					$grupoEspecialidade = new Cargo();
					$grupoEspecialidade->setIdEspecialidade($row['ID_ESPECIALIDADE']);
					$grupoEspecialidade->setDesEspecialidade($row['DES_ESPECIALIDADE']);
					$grupoEspecialidade->setQtdVagas($row['QTD_VAGAS']);
				}
			}
			return $grupoEspecialidade;  
	 	}
		
		/**
		* Metodo respons�vel em consultar se j� existe um grupo de especialidade com o mesmo nome cadastrado, em caso de edi��o ele desconsidera o registro que est� sendo atualizado.
		* @name verificarExistenciaGrupoEspecialidade
		* @param $desEspecialidade Descri��o do Grupo de Especialidade que esta sendo atualizado ou inclu�do.
		* @param $idEspecialidadeAtual ID do Grupo de Especialidade que est� sendo atualizado.
		* @return $gruposEspecialidadesArr GrupoEspecialidade
		*/
		function verificarExistenciaGrupoEspecialidade($desEspecialidade,$idEspecialidadeAtual = null) {  		  
			$gruposEspecialidadesArr;
			$desEspecialidade = strtoupper($desEspecialidade);
			$db = PegaConexao();
			$query = "select * from AP_ESPECIALIDADE where UPPER(DES_ESPECIALIDADE) = '$desEspecialidade'";
			if(!is_null($idEspecialidadeAtual)){
				$query .= " AND ID_ESPECIALIDADE <> $idEspecialidadeAtual";
			}	  
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {   			
					$grupoEspecialidade = new Cargo();
					$grupoEspecialidade->setIdEspecialidade($row['ID_ESPECIALIDADE']);
					$grupoEspecialidade->setDesEspecialidade($row['DES_ESPECIALIDADE']);
					$grupoEspecialidade->setQtdVagas($row['QTD_VAGAS']);
					$gruposEspecialidadesArr[] = $grupoEspecialidade;
				}
			}
			return $gruposEspecialidadesArr;  
	 	}
		
		/**
		* Metodo respons�vel em consultar se j� existe uma apura��o para um determinado grupo de especialidade.
		* @name verificarExistenciaApuracao
		* @param $idEspecialidade ID do Grupo de Especialidade que est� sendo verificado.
		* @return $qtdApuracaoVinculadas Int
		*/
		function verificarExistenciaApuracao($idEspecialidade) {  		  
			$qtdApuracaoVinculadas=0;
			$db = PegaConexao();
			$query = "select * from AP_APURACAO_LOCAL where ID_ESPECIALIDADE = $idEspecialidade";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {   			
					$qtdApuracaoVinculadas++;
				}
			}
			return $qtdApuracaoVinculadas;  
	 	}
		
		/**
		* Metodo respons�vel em consultar se j� existe um candidato para um determinado grupo de especialidade.
		* @name verificarExistenciaCandidato
		* @param $idEspecialidade ID do Grupo de Especialidade que est� sendo verificado.
		* @return $qtdCandidatosVinculados Int
		*/
		function verificarExistenciaCandidato($idEspecialidade) {  		  
			$qtdCandidatosVinculados=0;
			$db = PegaConexao();
			$query = "select * from AP_CANDIDATO_ESPECIALIDADE where ID_ESPECIALIDADE = $idEspecialidade";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {   			
					$qtdCandidatosVinculados++;
				}
			}
			return $qtdCandidatosVinculados;  
	 	}
		
		/**
		* Metodo respons�vel em carregar a lista de grupos de especialidades cadastrado na base de dados.
		* @param $desEspecialidade Informado quando deseja filtrar algum grupo de especialidade
		* @param $qtdVagas Informado quando deseja filtrar alguma qtde de vagas
		* @name listaGruposEspecialidades
		* @return $gruposEspecialidadesArr GrupoEspecialidade
		*/
		function listarGruposEspecialidades($desEspecialidade = null, $qtdVagas = null) {  		  
			$gruposEspecialidadesArr;
			$db = PegaConexao();
			$query = "select * from AP_ESPECIALIDADE WHERE 1=1";
			if(!is_null($desEspecialidade)){
				$desEspecialidade = strtoupper($desEspecialidade);
				$query .= " AND UPPER(DES_ESPECIALIDADE) LIKE '%$desEspecialidade%'";
			}
			if(!is_null($qtdVagas)){
				$query .= " AND QTD_VAGAS = $qtdVagas";
			}
			$query .= " order by DES_ESPECIALIDADE";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {   			
					$grupoEspecialidade = new Cargo();
					$grupoEspecialidade->setIdEspecialidade($row['ID_ESPECIALIDADE']);
					$grupoEspecialidade->setDesEspecialidade($row['DES_ESPECIALIDADE']);
					$grupoEspecialidade->setQtdVagas($row['QTD_VAGAS']);
					$gruposEspecialidadesArr[] = $grupoEspecialidade;
				}
			}
			return $gruposEspecialidadesArr;  
	 	}
		
		/**
		* Metodo respons�vel em atualizar um grupo de especialidade cadastrado na base de dados.
		* @param $idEspecialidade Identificador do Grupo de Especialidade
		* @param $desEspecialidade Descri��o do Grupo de Especialidade
		* @param $qtdVagas Quantidade de Vagas
		* @name atualizarGrupoEspecialidade
		* @return Int
		*/
		function atualizarGrupoEspecialidade($idEspecialidade, $desEspecialidade, $qtdVagas) {
			$db = PegaConexao();
			$query = "UPDATE AP_ESPECIALIDADE SET DES_ESPECIALIDADE = '$desEspecialidade', QTD_VAGAS = $qtdVagas WHERE ID_ESPECIALIDADE = $idEspecialidade";	  
			$result = $db->Execute($query);
			if(!empty($result)){
				return 1;
			} else {
				return 0;
			} 
		}
	
		/**
		* Metodo respons�vel em incluir um grupo de especialidade na base de dados.
		* @param $desEspecialidade Descri��o do Grupo de Especialidade
		* @param $qtdVagas Quantidade de Vagas
		* @name incluirGrupoEspecialidade
		* @return Int
		*/
		function incluirGrupoEspecialidade($desEspecialidade, $qtdVagas) {
			$db = PegaConexao();
			$query = "INSERT INTO AP_ESPECIALIDADE(DES_ESPECIALIDADE, QTD_VAGAS) VALUES ('$desEspecialidade', $qtdVagas)";
			$result = $db->Execute($query);
			if(!empty($result)){
				return $db->Insert_ID();
			} else {
				return 0;
			} 
		}
		
		/**
		* Metodo respons�vel em excluir um grupo de especialidade na base de dados.
		* @param $idEspecialidade Identificador do Grupo de Especialidade
		* @name excluirGrupoEspecialidade
		* @return Int
		*/
		function excluirGrupoEspecialidade($idEspecialidade) {
			$db = PegaConexao();
			$query = "DELETE FROM AP_ESPECIALIDADE WHERE ID_ESPECIALIDADE = $idEspecialidade";
			$result = $db->Execute($query);
			if(!empty($result)){
				return 1;
			} else {
				return 0;
			} 
		}
	}

?>