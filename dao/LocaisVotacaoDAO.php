<?php
 	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/model/LocalVotacao.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/app-config/AdodbConnect.php"); 
	
	/**
	* Classe de acesso ao banco de dados referente � Locais de Vota��o
	* @name LocaisVotacaoDAO
	* @version v 1.0
	* @package com.algartecnologia.dao
	* @access public
	*/	
	Class LocaisVotacaoDAO {
 		
		/**
		* Metodo respons�vel em carregar um local de vota��o cadastrado na base de dados.
		* @name carregaLocalVotacao
		* @idLocalVotacao ID do Local de Vota��o que deseja recuperar.
		* @return $localVotacao LocalVotacao
		*/
		function consultarLocalVotacaoByIdLocal($idLocalVotacao) {  		  
			$db = PegaConexao();
			$query = "select * from AP_LOCAL where ID_LOCAL = $idLocalVotacao";	  
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {   			
					$localVotacao = new LocalVotacao();
					$localVotacao->setIdLocal($row['ID_LOCAL']);
					$localVotacao->setDesLocal($row['DES_LOCAL']);
					$localVotacao->setCidade($row['CIDADE']);
					$localVotacao->setQtdPresentes($row['QTD_PRESENTES']);
				}
			}
			return $localVotacao;  
	 	}
		
		/**
		* Metodo respons�vel em consutlar se j� existe um local com o mesmo nome cadastrado, em caso de edi��o ele desconsidera o registro que est� sendo atualizado.
		* @name verificarExistenciaLocalVotacao
		* @param $desLocal Descri��o do Local que esta sendo atualizado ou inclu�do.
		* @param $idLocalAtual ID do Local que est� sendo atualizado.
		* @return $locaisVotacaoArr LocalVotacao
		*/
		function verificarExistenciaLocalVotacao($desLocal,$idLocalAtual = null) {  		  
			$locaisVotacaoArr = array();
			$desLocal = strtoupper($desLocal);
			$db = PegaConexao();
			$query = "select * from AP_LOCAL where UPPER(DES_LOCAL) = '$desLocal'";
			if(!is_null($idLocalAtual)){
				$query .= " AND ID_LOCAL <> $idLocalAtual";
			}	  
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {   			
					$localVotacao = new LocalVotacao();
					$localVotacao->setIdLocal($row['ID_LOCAL']);
					$localVotacao->setDesLocal($row['DES_LOCAL']);
					$localVotacao->setCidade($row['CIDADE']);
					$localVotacao->setQtdPresentes($row['QTD_PRESENTES']);
					$locaisVotacaoArr[] = $localVotacao;
				}
			}
			return $locaisVotacaoArr;  
	 	}
		
		/**
		* Metodo respons�vel em consutlar se j� existe uma apura��o para um determinado local.
		* @name verificarExistenciaApuracao
		* @param $idLocal ID do Local que est� sendo verificado.
		* @return $qtdApuracaoVinculadas Int
		*/
		function verificarExistenciaApuracao($idLocal) {  		  
			$qtdApuracaoVinculadas=0;
			$db = PegaConexao();
			$query = "select * from AP_APURACAO_LOCAL where ID_LOCAL = $idLocal";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {   			
					$qtdApuracaoVinculadas++;
				}
			}
			return $qtdApuracaoVinculadas;  
	 	}
		
		/**
		* Metodo respons�vel em carregar a lista de locais de vota��o cadastrado na base de dados.
		* @name listaLocaisVotacao
		* @return $locaisVotacoesArr LocalVotacao
		*/
		function listarLocaisVotacao($desLocal = null, $cidade = null) {  		  
			$locaisVotacoesArr = array();
			$db = PegaConexao();
			$query = "select * from AP_LOCAL WHERE 1=1";
			if(!is_null($desLocal)){
				$desLocal = strtoupper($desLocal);
				$query .= " AND UPPER(DES_LOCAL) LIKE '%$desLocal%'";
			}
			if(!is_null($cidade)){
				$query .= " AND CIDADE = '$cidade'";
			}
			$query .= " order by DES_LOCAL";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {   			
					$localVotacao = new LocalVotacao();
					$localVotacao->setIdLocal($row['ID_LOCAL']);
					$localVotacao->setDesLocal($row['DES_LOCAL']);
					$localVotacao->setCidade($row['CIDADE']);
					$localVotacao->setQtdPresentes($row['QTD_PRESENTES']);
					$locaisVotacoesArr[] = $localVotacao;
				}
			}
			return $locaisVotacoesArr;  
	 	}
		
		/**
		* Metodo respons�vel em atualizar um local de vota��o cadastrado na base de dados.
		* @param $idLocal Identificador do Local de Vota��o
		* @param $desLocal Descri��o do Local de Vota��o
		* @param $cidade Nome da Cidade
		* @name atualizaLocalVotacao
		* @return Int
		*/
		function atualizarLocalVotacao($idLocal, $desLocal, $cidade) {
			$db = PegaConexao();
			$query = "UPDATE AP_LOCAL SET DES_LOCAL = '$desLocal', CIDADE = '$cidade' WHERE ID_LOCAL = $idLocal";	  
			$result = $db->Execute($query);
			if(!empty($result)){
				return 1;
			} else {
				return 0;
			} 
		}
		
		/**
		* Metodo respons�vel em atualizar a quantidade de presentes em um local de vota��o cadastrado na base de dados.
		* @param $idLocal Identificador do Local de Vota��o
		* @param $qtdPresentes Quantidade de Presentes no Local de Vota��o
		* @name atualizarQtdPresentes
		* @return Int
		*/
		function atualizarQtdPresentes($idLocal, $qtdPresentes) {
			$db = PegaConexao();
			$query = "UPDATE AP_LOCAL SET QTD_PRESENTES = $qtdPresentes WHERE ID_LOCAL = $idLocal";	  
			$result = $db->Execute($query);
			if(!empty($result)){
				return 1;
			} else {
				return 0;
			} 
		}
	
		/**
		* Metodo respons�vel em incluir um local de vota��o na base de dados.
		* @param $desLocal Descri��o do Local de Vota��o
		* @param $cidade Nome da Cidade
		* @name incluirLocalVotacao
		* @return Int
		*/
		function incluirLocalVotacao($desLocal, $cidade) {
			$db = PegaConexao();
			$query = "INSERT INTO AP_LOCAL(DES_LOCAL, CIDADE) VALUES ('$desLocal', '$cidade')";
			$result = $db->Execute($query);
			if(!empty($result)){
				return $db->Insert_ID();
			} else {
				return 0;
			} 
		}
		
		/**
		* Metodo respons�vel em excluir um local de vota��o na base de dados.
		* @param $idLocal Identificador do Local de Vota��o
		* @name excluirLocalVotacao
		* @return Int
		*/
		function excluirLocalVotacao($idLocal) {
			$db = PegaConexao();
			$query = "DELETE FROM AP_LOCAL WHERE ID_LOCAL = $idLocal";
			$result = $db->Execute($query);
			if(!empty($result)){
				return 1;
			} else {
				return 0;
			} 
		}
	}

?>