<?php
 	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/model/CandidatoEspecialidade.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/app-config/AdodbConnect.php");

	/**
	* Classe de acesso ao banco de dados referente � Candidatos e os grupos de especialidades
	* @name CandidatosEspecialidadesDAO
	* @version v 1.0 22/08/2011
	* @package com.algartecnologia.dao
	* @access public
	*/
	Class CandidatosEspecialidadesDAO {

		/**
		* Metodo respons�vel em carregar um local de vota��o cadastrado na base de dados.
		* @name carregaLocalVotacao
		* @idLocalVotacao ID do Local de Vota��o que deseja recuperar.
		* @return $localVotacao LocalVotacao
		*/
		function consultarCanditatoEspecialidadeByIdCanditEspec($idCanditEspec) {
			$db = PegaConexao();
			$query = "select CE.ID_CANDIT_ESPEC, CE.CRM, CE.ID_ESPECIALIDADE, E.DES_ESPECIALIDADE, C.NOME from AP_CANDIDATO_ESPECIALIDADE CE, AP_ESPECIALIDADE E, AP_CANDIDATO C WHERE CE.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE AND CE.CRM = C.CRM AND CE.ID_CANDIT_ESPEC = $idCanditEspec";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {
					$candidatoEspecialidade = new CandidatoEspecialidade();
					$candidatoEspecialidade->setIdCanditEspec($row['ID_CANDIT_ESPEC']);
					$candidatoEspecialidade->setIdEspecialidade($row['ID_ESPECIALIDADE']);
					$candidatoEspecialidade->setCRM($row['CRM']);
					$candidatoEspecialidade->setDesEspecialidade($row['DES_ESPECIALIDADE']);
					$candidatoEspecialidade->setNome($row['NOME']);
				}
			}
			return $candidatoEspecialidade;
	 	}

		/**
		* Metodo respons�vel em consutlar se j� existe um candidato inscrito em determinado grupo de especialidade.
		* @name verificarExistenciaCandidatoEspecialidade
		* @param $crm CRM do candidato que esta sendo vinculado.
		* @param $idEspecialidade ID da Especialidade que est� sendo vinculado ao candidato.
		* @return $candidatoEspecialidadeArr CandidatoEspecialida[]
		*/
		function verificarExistenciaCandidatoEspecialidade($crm,$idEspecialidade) {
			$candidatoEspecialidadeArr = array();
			$db = PegaConexao();
			$query = "select * from AP_CANDIDATO_ESPECIALIDADE where ID_ESPECIALIDADE = $idEspecialidade AND CRM = $crm";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {
					$candidatoEspecialidade = new CandidatoEspecialidade();
					$candidatoEspecialidade->setIdCanditEspec($row['ID_CANDIT_ESPEC']);
					$candidatoEspecialidade->setIdEspecialidade($row['ID_ESPECIALIDADE']);
					$candidatoEspecialidade->setCRM($row['CRM']);
					$candidatoEspecialidadeArr[] = $candidatoEspecialidade;
				}
			}
			return $candidatoEspecialidadeArr;
	 	}

		/**
		* Metodo respons�vel em consutlar se j� existe uma base de apura��o gerada.
		* @name verificarExistenciaApuracao
		* @return $existeApuracao Int
		*/
		function verificarExistenciaApuracao() {
			$existeApuracao=0;
			$db = PegaConexao();
			$query = "select * from AP_APURACAO_LOCAL LIMIT 1";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {
					$existeApuracao++;
				}
			}
			return $existeApuracao;
	 	}

		/**
		* Metodo respons�vel em carregar a lista de Grupos de Especialidades e seus Candidatos cadastrado na base de dados.
		* @name listarCandidatosEspecilidades
		* @param crm CRM do m�dico quando se utiliza na consulta
		* @param idEspecialidade Identificador do Grupo de Especialidade quando se utilia na consulta
		* @return $candidatosEspecialidadesArr CandidatoEspecialidade[]
		*/
		function listarCandidatosEspecilidades($crm = null, $idEspecialidade = null) {
			$candidatosEspecialidadesArr = array();
			$db = PegaConexao();
			$query = "select CE.ID_CANDIT_ESPEC, CE.CRM, CE.ID_ESPECIALIDADE, E.DES_ESPECIALIDADE, C.NOME from AP_CANDIDATO_ESPECIALIDADE CE, AP_ESPECIALIDADE E, AP_CANDIDATO C WHERE CE.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE AND CE.CRM = C.CRM";
			if(!is_null($crm)){
				$query .= " AND CE.CRM = $crm";
			}
			if(!is_null($idEspecialidade)){
				$query .= " AND CE.ID_ESPECIALIDADE = $idEspecialidade";
			}
			$query .= " order by E.DES_ESPECIALIDADE, C.NOME";
			$result = $db->Execute($query);
			if(!empty($result)){
				foreach ($result as $row) {
					$candidatoEspecialidade = new CandidatoEspecialidade();
					$candidatoEspecialidade->setIdCanditEspec($row['ID_CANDIT_ESPEC']);
					$candidatoEspecialidade->setIdEspecialidade($row['ID_ESPECIALIDADE']);
					$candidatoEspecialidade->setCRM($row['CRM']);
					$candidatoEspecialidade->setDesEspecialidade($row['DES_ESPECIALIDADE']);
					$candidatoEspecialidade->setNome($row['NOME']);
					$candidatosEspecialidadesArr[] = $candidatoEspecialidade;
				}
			}
			return $candidatosEspecialidadesArr;
	 	}

		/**
		* Metodo respons�vel em vincular um candidato a um grupo de especialidade.
		* @param $crm CRM do candidato
		* @param $idEspecialidade Especialidade que o candidato ser� vinculado
		* @name incluirCandidatoEspecialidade
		* @return Int
		*/
		function incluirCandidatoEspecialidade($crm, $idEspecialidade) {
			$db = PegaConexao();
			$query = "INSERT INTO AP_CANDIDATO_ESPECIALIDADE(CRM, ID_ESPECIALIDADE) VALUES ($crm, $idEspecialidade)";
			$result = $db->Execute($query);
			if(!empty($result)){
				return $db->Insert_ID();
			} else {
				return 0;
			}
		}

		/**
		* Metodo respons�vel em desvincular um candidato a um grupo de especialidade na base de dados.
		* @param $idCanditEspec Identificador do Vinculo de Candidato e Grupo de Especialidade
		* @name excluirCandidatoEspecialidade
		* @return Int
		*/
		function excluirCandidatoEspecialidade($idCanditEspec) {
			$db = PegaConexao();
			$query = "DELETE FROM AP_CANDIDATO_ESPECIALIDADE WHERE ID_CANDIT_ESPEC = $idCanditEspec";
			$result = $db->Execute($query);
			if(!empty($result)){
				return 1;
			} else {
				return 0;
			}
		}
	}

?>