<?php
require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/business/CandidatosBO.php"); 
 
$acao = isset($_REQUEST["action"])?$_REQUEST["action"]:""; 
$acao = empty($acao)?$_REQUEST["botao"]:$acao;
$bo = new CandidatosBO($acao);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once('head.php');?>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="gerenciaBotoesGerirCandidato()">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
      <tr>
        <td colspan="2"><?php include_once('cabecalho.php');?></td>
      </tr>
      <tr>
        <td width="210" valign="top"><?php include_once('menu.php');?></td>
        <td valign="top">
         <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="form" name="form" enctype="multipart/form-data"  onsubmit="return OnSubmitFormGerirCandidatos();" >
        <table width="99%" border="0" cellspacing="0" cellpadding="0">
	        <tr>
				<td height="10">&nbsp;</td>
			</tr>
			<tr>
				<td class="TituloSecao"><img src="images/spacer.gif" width="15"									border="0" alt="">GERIR CANDIDATOS
				</td>
			</tr>
			<tr>
				<td height="10">&nbsp;</td>
			</tr>
            <tr>
				<td height="10"> 
				<?php 
					$msgSucessos = $bo->getMsgSucessosArr(); 
					if (!empty($msgSucessos)){
				?>
            <div class="MensagemSucesso">
            	<?php 
					foreach ($msgSucessos as $row) {
						echo $row;
					}
				?>
			</div>
            	<?php 
					} else { echo "&nbsp;"; }
					$msgErros = $bo->getMsgErrosArr();
					if(!empty($msgErros)){
				?>
            <div class="MensagemErro">
				<?php 
					foreach ($msgErros as $row) {
						echo $row;
					}
				?>
			</div>
            <?php 
					} else { echo "&nbsp;"; }
			?>
</td>
			</tr>
            <tr>
				<td height="10">&nbsp;</td>
			</tr>
			<tr>
			  <td valign="top">
             
              <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td colspan="2">
                  
                  <?php 
				  
				  	if($bo->getCandidato() != null) { 
						$registro = $bo->getCandidato();	
				  ?>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="19%">CRM <span class="campoObrigatorio">*</span>
                        <input type="hidden" name="action" id="action" value="<?php echo $acao; ?>" />                     </td>
                      <td colspan="2">Nome do Candidato <span class="campoObrigatorio">*</span></td>
                    </tr>
                    <tr>
                      <td><input name="crm" type="text" id="crm" size="10" maxlength="6" value="<?php echo $registro->getCRM(); ?>" <?php if(isset($_GET["crm"])) { echo "readonly='readonly'"; } ?>  onkeypress="return Bloqueia_Caracteres(event);"/></td>
                      <td colspan="2"><input name="nome" type="text" id="nome" size="60" maxlength="100" value="<?php echo $registro->getNome(); ?>" /></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td width="19%">Data de Cooperação <span class="campoObrigatorio">*</span><img src="images/spacer.gif" width="1" height="20"									border="0" alt="" /></td>
                      <td width="62%">Data de Nascimento <span class="campoObrigatorio">*</span><img src="images/spacer.gif" width="1" height="20"									border="0" alt="" /></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td><input name="datCooperacao" type="text" id="datCooperacao" size="15" maxlength="10" value="<?php echo $registro->getDatCooperacao(); ?>" onKeypress="return Ajusta_Data(this, event);" /></td>
                      <td><input name="datNascimento" type="text" id="datNascimento" size="15" maxlength="10" value="<?php echo $registro->getDatNascimento(); ?>" onKeypress="return Ajusta_Data(this, event);" /></td>
                    </tr>
                    <tr>
                      <td colspan="3">&nbsp;</td>
                    </tr>
                  </table>
                  <?php } else { ?>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="19%">CRM <span class="campoObrigatorio">*</span>
                        <input type="hidden" name="action" id="action" value="<?php echo $acao; ?>" />                     </td>
                      <td colspan="2">Nome do Candidato <span class="campoObrigatorio">*</span></td>
                    </tr>
                    <tr>
                      <td><input name="crm" type="text" id="crm" size="10" maxlength="6" onkeypress="return Bloqueia_Caracteres(event);"/></td>
                      <td colspan="2"><input name="nome" type="text" id="nome" size="60" maxlength="100" /></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td width="19%">Data de Cooperação <span class="campoObrigatorio">*</span><img src="images/spacer.gif" width="1" height="20"									border="0" alt="" /></td>
                      <td width="62%">Data de Nascimento <span class="campoObrigatorio">*</span><img src="images/spacer.gif" width="1" height="20"									border="0" alt="" /></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td><input name="datCooperacao" type="text" id="datCooperacao" size="15" maxlength="10" onKeypress="return Ajusta_Data(this, event);" /></td>
                      <td><input name="datNascimento" type="text" id="datNascimento" size="15" maxlength="10" onKeypress="return Ajusta_Data(this, event);" /></td>
                    </tr>
                    <tr>
                      <td colspan="3">&nbsp;</td>
                    </tr>
                  </table>
                  <?php } ?>                  </td>
                </tr>
                <tr>
                  <td width="11%" align="right"><input type="button" name="botao" id="btnImportar" value="Importar Dados" class="BotaoMaior" onclick="window.open('enviarArquivoXML.php', 'XML','status=no,scrollbars=yes,width=500,height=450, location=no, toolbar=no, menubar=no')"/></td>
                  <td width="89%" align="right"><input type="submit" name="botao" id="btnIncluir" value="Incluir" class="BotaoMaior" onclick="document.pressed=this.value" />
                    <input type="submit" name="botao" id="btnConsultar" value="Consultar" class="BotaoMaior" onclick="document.pressed=this.value" />
                    <input type="submit" name="botao" id="btnAlterar" value="Alterar" class="BotaoMaior" onclick="document.pressed=this.value" />
                  <input type="submit" name="botao" id="btnExcluir" value="Excluir" class="BotaoMaior" onclick="document.pressed=this.value" /></td>
                </tr>
                <tr>
                  <td colspan="2" align="right">&nbsp;</td>
                </tr>
              </table>
              
              <br /></td>
		  </tr>
			<tr>
			  <td valign="top">
              	<fieldset >
			      <legend>Candidatos Cadastrados </legend>
            <?php 
				  		$lista = $bo->getListaCandidatosArr();
						if(!empty($lista)){
				  ?>
			      <table class="paginador" width="98%">
							<tr>
								<td width="13%" class="header"><div align="center">CRM</div></td>
								<td width="59%" class="header">Nome do Candidato</td>
								<td width="14%" class="header">Dt Cooperação</td>
								<td width="14%" class="header">Dt Nascimento</td>
							</tr>					    
							
							<?php
                            	foreach ($lista as $row) {
							?>
			                  <tr onMouseOver="this.className='selecionada'" onMouseOut="this.className='inicial'">
									<td class="body"><div align="center"><a href="gerirCandidatos.php?action=edit&crm=<?php echo $row->getCRM(); ?>" class="TabelaLink"><?php echo $row->getCRM(); ?></a></div></td>
									<td class="body"><a href="gerirCandidatos.php?action=edit&crm=<?php echo $row->getCRM(); ?>" class="TabelaLink"><?php echo $row->getNome(); ?></a></td>
									<td class="body"><div align="right"><a href="gerirCandidatos.php?action=edit&crm=<?php echo $row->getCRM(); ?>" class="TabelaLink"><?php echo $row->getDatCooperacao(); ?></a></div></td>
									<td class="body"><div align="right"><a href="gerirCandidatos.php?action=edit&crm=<?php echo $row->getCRM(); ?>" class="TabelaLink"><?php echo $row->getDatNascimento(); ?></a></div></td>
					</tr>
                    		<?php } ?>
			    </table>
	    <table>
                	<tr>
                    	<td>
					    <input type="submit" name="btnPrimeira" id="btnPrimeira" value="" onclick="document.pressed='Primeira'" class="PaginacaoPrimeira"/>&nbsp;
                        <input type="submit" name="btnAnterior" id="btnAnterior" value="" onclick="document.pressed='Anterior'" class="PaginacaoAnterior"/>&nbsp;
                        <td valign="middle">
                        <input type="hidden" id="paginaAtual" name="paginaAtual" value="<?php echo $bo->getPaginaAtual(); ?>"/><?php echo $bo->getPaginaAtual(); ?>  / <?php echo $bo->getNumPaginas(); ?> <input type="hidden" id="totalPaginas" name="totalPaginas" value="<?php echo $bo->getNumPaginas(); ?>"/> &nbsp;
                        </td>
                        <td>
                        <input type="submit" name="btnPosterior" id="btnPosterior" value="" onclick="document.pressed='Posterior'" class="PaginacaoPosterior"/>&nbsp;
                        <input type="submit" name="btnUltima" id="btnUltima" value="" onclick="document.pressed='Ultima';" class="PaginacaoUltima"/>
                  	<?php 
				  		} else {
							if($_REQUEST["botao"] != "Consultar"){
								echo "&nbsp;&nbsp;&nbsp;N&atilde;o foram encontrados registros neste cadastro.";
							} else {
								echo "&nbsp;&nbsp;&nbsp;N&atilde;o foram encontrados registros nesta consulta.";
							}
						}	
					?>
                    	</td>
                    </tr>
                </table>
					</fieldset>
</td>
		  </tr>
		</table>
		</form>
        </td>
      </tr>
      <tr>
        <td colspan="2"><?php include_once('rodape.php');?></td>
      </tr>
</table>

</body>
</html>
