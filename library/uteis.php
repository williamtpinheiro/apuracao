<?php
/**
* Arquivo de funções úteis globais para o sistema
* @name Funções
* @version v 1.0
* @package com.algar.library
*/	

	/**
	 * Função que imprime na tela o conteúdo de um array formatado
	 *
	 * @param $array
	 * @return void
	 */
	function debugCode($array)
	{ 
		echo '<pre>';
		print_r($array);
		echo '</pre>';
	}
	
	
	/**
	 * Função que retira toda a acentuacao de um texto
	 * @param $texto
	 * @return String
	 */
	function retira_acentos( $texto )
	{ 
	  $var = $texto;
	
		$var = ereg_replace("[ÁÀÂÃ]","A",$var);
		$var = ereg_replace("[áàâãª]","a",$var);
		$var = ereg_replace("[ÉÈÊ]","E",$var);
		$var = ereg_replace("[éèê]","e",$var);
		$var = ereg_replace("[ÓÒÔÕ]","O",$var);
		$var = ereg_replace("[óòôõº]","o",$var);
		$var = ereg_replace("[ÚÙÛ]","U",$var);
		$var = ereg_replace("[úùû]","u",$var);
		$var = str_replace("Ç","C",$var);
		$var = str_replace("ç","c",$var);
	
	return $var;


	} 

	   function AntiSqlInjection($recebe)
		{
			// Verifica a configuração se "magic_quotes_gpc" esta setado, caso não esteja aplicamos o addslashes
		  if (!get_magic_quotes_gpc())
		  {
			addslashes($recebe);
		  }
			// Limpamos as tags HTML
			$recebe = strip_tags($recebe);
		  
		   // Procuramos as palavras reservadas sql e deletamos
			@$recebe = preg_replace(sql_regcase("/(from|select|insert|delete|where|drop table|show tables)/"),'', $recebe);  
			return $recebe;
		}
	 

	function popup($url, $titulo = '', $opcoes = '') 
	{ 
		echo '<script type="text/javascript">';
		echo "  window.open('$url','$titulo','$opcoes')";	
		echo '</script>';
	}

	function valData($Data)
	{
		
		$dtExplode = explode('/', $Data);
		$Dia = $dtExplode[0];
		$Mes = $dtExplode[1];
		$Ano = $dtExplode[2];
		return @checkdate ( $Mes, $Dia, $Ano);
	}
	
	function valNumero($Numero)
	{
		return preg_match ("/^([0-9]+)$/", $Numero);
	}
	
	function redirecionaURL($acao)
	{	
		echo "<script language='javascript'> window.location=('$acao'); </script>";
	}

	/**
	* Metodo responsável em adicionar dias a uma data.
	* @name adicionaDiasNaData
	* @param Date $date
 	* @param Integer $days
	* @return Date 
	*/
	function adicionaDiasNaData($date,$days) {
	     $thisyear = substr ( $date, 0, 4 );
	     $thismonth = substr ( $date, 4, 2 );
	     $thisday =  substr ( $date, 6, 2 );
	     $nextdate = mktime ( 0, 0, 0, $thismonth, $thisday + $days, $thisyear );
	     return strftime("%Y%m%d", $nextdate);
	}
	
	/**
	* Metodo responsável em subtrair dias de uma data.
	* @name subtrairDiasNaData
	* @param Date $date
 	* @param Integer $days
	* @return Date 
	*/
	function subtrairDiasNaData($date,$days) {
	     $thisyear = substr ( $date, 0, 4 );
	     $thismonth = substr ( $date, 5, 2 );
	     $thisday =  substr ( $date, 8, 2 );
	     $nextdate = mktime ( 0, 0, 0, $thismonth, $thisday - $days, $thisyear );
	     return strftime("%Y%m%d", $nextdate);
	}

	function formataData($data)	{
		// Tratamento da DATA
		$pData_Quebrada = split("/",$data);
		$pData_US=$pData_Quebrada[2].$pData_Quebrada[1].$pData_Quebrada[0];
		$pDataResult = $pData_Quebrada[0].'-';
		
		switch($pData_Quebrada[1]) {
			case '01':
				$pDataResult .= 'Jan';
				break;
			case '02':
				$pDataResult .= 'Fev';
				break;
			case '03':
				$pDataResult .= 'Mar';
				break;
			case '04':
				$pDataResult .= 'Abr';
				break;
			case '05':
				$pDataResult .= 'Mai';
				break;
			case '06':
				$pDataResult .= 'Jun';
				break;
			case '07':
				$pDataResult .= 'Jul';
				break;
			case '08':
				$pDataResult .= 'Ago';
				break;
			case '09':
				$pDataResult .= 'Set';
				break;
			case  '10':
				$pDataResult .= 'Out';
				break;
			case '11':
				$pDataResult .= 'Nov';
				break;
			case '12':
				$pDataResult .= 'Dez';
				break;
		}
		
		$pDataResult .= '-'.$pData_Quebrada[2];
		return $pDataResult;
		
	}
	
	function formataDataBr($data)	{
		// Tratamento da DATA
		$pData_Quebrada = split("/",$data);
		$pDataResult = $pData_Quebrada[0].' de ';
		
		switch($pData_Quebrada[1]) {
			case 1:
				$pDataResult .= 'Janeiro';
				break;
			case 2:
				$pDataResult .= 'Fevereiro';
				break;
			case 3:
				$pDataResult .= 'Março';
				break;
			case 4:
				$pDataResult .= 'Abril';
				break;
			case 5:
				$pDataResult .= 'Maio';
				break;
			case 6:
				$pDataResult .= 'Junho';
				break;
			case 7:
				$pDataResult .= 'Julho';
				break;
			case 8:
				$pDataResult .= 'Agosto';
				break;
			case 9:
				$pDataResult .= 'Setembro';
				break;
			case 10:
				$pDataResult .= 'Outubro';
				break;
			case 11:
				$pDataResult .= 'Novembro';
				break;
			case 12:
				$pDataResult .= 'Dezembro';
				break;
		}
		
		$pDataResult .= ' de '.$pData_Quebrada[2];
		return $pDataResult;
		
	}	
	
	function data($data)
	{
		$dia = substr($data,8,2);
		$mes = substr($data,5,2);
		$ano = substr($data,0,4);
		return("$dia/$mes/$ano");
	}
	
	function data2($data)
	{
		list($d,$m,$a) = explode('/',$data);
		return("$a/$m/$d");
	}
	
	function upload($arquivo,$caminho){
		if(!(empty($arquivo))){
			$arquivo1 = $arquivo;
			$arquivo_minusculo = strtolower($arquivo1['name']);
			$caracteres = array("ç","~","^","]","[","{","}",";",":","´",",",">","<","-","/","|","@","$","%","ã","â","á","à","é","è","ó","ò","+","=","*","&","(",")","!","#","?","`","ã"," ","©");
			$arquivo_tratado = str_replace($caracteres,"",$arquivo_minusculo);
			$destino = $caminho."/".$arquivo_tratado;
			if(move_uploaded_file($arquivo1['tmp_name'],$destino)){
				return $destino;
			}else{
				return 0;
			}
		}
	}
	
	function verExtensao($nomeArquivo) {
		 $extensao = array_reverse(explode(".",$nomeArquivo));
		 return $extensao[0];
	}

?>