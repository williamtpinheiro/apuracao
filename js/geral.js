// JavaScript Document
	if (navigator.appName.indexOf('Microsoft') != -1){   
		clientNavigator = "IE";   
	}else{   
		clientNavigator = "Other";   
	}  

	function OnSubmitForm(){
		if(document.pressed == 'Excluir'){
		   	if (confirm('Tem certeza que deseja excluir?')){ 
			 	return true;
	   		}else{
				document.pressed = '';
				return false;
			}
	  	}
	}
	
	function gerenciaBotoes(){
		var acao = document.getElementById('action').value;
		if (acao != ""){
			if(acao == 'edit'){
				if(document.getElementById('id') != null) {
					if(document.getElementById('id').value != "") {
						document.getElementById('btnExcluir').disabled = false;
						document.getElementById('btnAlterar').disabled = false;
						document.getElementById('btnConsultar').disabled = true;
						document.getElementById('btnIncluir').disabled = true;	
					} else {
						document.getElementById('btnExcluir').disabled = true;
						document.getElementById('btnAlterar').disabled = true;
						document.getElementById('btnConsultar').disabled = false;
						document.getElementById('btnIncluir').disabled = false;
					}
				} else {
					document.getElementById('btnExcluir').disabled = true;
					document.getElementById('btnAlterar').disabled = true;
					document.getElementById('btnConsultar').disabled = false;
					document.getElementById('btnIncluir').disabled = false;
				}
			} else {
				document.getElementById('btnExcluir').disabled = true;
				document.getElementById('btnAlterar').disabled = true;
				document.getElementById('btnConsultar').disabled = false;
				document.getElementById('btnIncluir').disabled = false;
			}
		} else {
			document.getElementById('btnExcluir').disabled = true;
			document.getElementById('btnAlterar').disabled = true;
			document.getElementById('btnConsultar').disabled = false;
			document.getElementById('btnIncluir').disabled = false;
		}
	}
	
 
 	function Bloqueia_Caracteres(evnt){   
	//Fun��o permite digita��o de n�meros   
		if (clientNavigator == "IE"){   
			if (evnt.keyCode < 48 || evnt.keyCode > 57){   
				return false   
			}   
		}else{   
			if ((evnt.charCode < 48 || evnt.charCode > 57) && evnt.keyCode == 0){   
				return false   
			}   
		}   
	}   
	
	
	  
	function Ajusta_Data(input, evnt){   
	//Ajusta m�scara de Data e s� permite digita��o de n�meros   
		if (input.value.length == 2 || input.value.length == 5){   
			if(clientNavigator == "IE"){   
				input.value += "/";   
			}else{   
				if(evnt.keyCode == 0){   
					input.value += "/";   
				}   
			}   
		}   
	//Chama a fun��o Bloqueia_Caracteres para s� permitir a digita��o de n�meros   
		return Bloqueia_Caracteres(evnt);   
	}  
 
 function Verifica_Data(data, obrigatorio){   
	//Se o par�metro obrigat�rio for igual � zero, significa que ele pode estar vazio, caso contr�rio, n�o, data � o nome do campo no form   
		var data = document.getElementById(data);   
		var strdata = data.value;  
		
		if((obrigatorio == 1) || (obrigatorio == 0 && strdata != "")){   
			//Verifica a quantidade de digitos informada esta correta.   
			if (strdata.length != 10){   
				alert("Formato da data n�o � v�lido.\nFormato correto:\n- dd/mm/aaaa.");
				data.focus();   
				return false   
			}   
			  
			//Verifica m�scara da data   
			if ("/" != strdata.substr(2,1) || "/" != strdata.substr(5,1)){   
				alert("Formato da data n�o � v�lido.\nFormato correto:\n- dd/mm/aaaa.");   
				data.focus();   
				return false   
			}   
			dia = strdata.substr(0,2)   
			mes = strdata.substr(3,2);   
			ano = strdata.substr(6,4);   
			//Verifica o dia   
			if (isNaN(dia) || dia > 31 || dia < 1){   
				alert("Formato do dia n�o � v�lido.");   
				data.focus();   
				return false   
			}   
			if (mes == 4 || mes == 6 || mes == 9 || mes == 11){   
				if (dia == "31"){   
					alert("O m�s informado n�o possui 31 dias.");   
					data.focus();   
					return false   
				}   
			}   
			if (mes == "02"){   
				bissexto = ano % 4;   
				if (bissexto == 0){   
					if (dia > 29){   
						alert("O m�s informado possui somente 29 dias.");   
						data.focus();   
						return false   
					}   
				}else{   
					if (dia > 28){   
						alert("O m�s informado possui somente 28 dias.");   
						data.focus();   
						return false   
					}   
				}   
			}   
		//Verifica o m�s   
			if (isNaN(mes) || mes > 12 || mes < 1){   
				alert("Formato do m�s n�o � v�lido.");   
				data.focus();   
				return false   
			}   
			//Verifica o ano   
			if (isNaN(ano)){   
				alert("Formato do ano n�o � v�lido.");   
				data.focus();   
				return false   
			} 
		}   
		
	}  
	
	function getUrlVars()
	{
		var vars = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	 
		for(var i = 0; i < hashes.length; i++)
		{
			hash = hashes[i].split('=');
			hash[1] = unescape(hash[1]);
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
	 
		return vars;
	}

function timedRefresh(timeoutPeriod) {
	setTimeout("location.reload(true);",timeoutPeriod);
}
	 








//Scripts da Aplica��o
	
	function ValidaGerirCandidatos(){     
		if(Verifica_Data("datCooperacao", 1) == false){ return false } 
		if(Verifica_Data("datNascimento", 1) == false){ return false }
	}
	
	function OnSubmitFormGerirCandidatos(){
		if(OnSubmitForm()){
			return true;
		}else{
			if (document.pressed == 'Consultar'){ 
				if(document.getElementById('datCooperacao').value != ''){
					if(Verifica_Data("datCooperacao", 1) == false){ return false }
				} else if(document.getElementById('datNascimento').value != ''){
					if(Verifica_Data("datNascimento", 1) == false){ return false }
				}
			}else if(document.pressed == ''){
				return false;
			}else if((document.pressed == 'Primeira')||(document.pressed == 'Anterior')||(document.pressed == 'Posterior')||(document.pressed == 'Ultima')){
				return true;
			}else{
				return ValidaGerirCandidatos();
			}
		}
	}
	
	function gerenciaBotoesGerirCandidato(){
		var acao = document.getElementById('action').value;
		var variaveis = getUrlVars();
		if (acao != ""){
			if(acao == 'edit'){
				if(document.getElementById('crm') != null) {
					if(document.getElementById('crm').value != "") {
						if (variaveis["crm"] == null){
							document.getElementById('btnExcluir').disabled = true;
							document.getElementById('btnAlterar').disabled = true;
							document.getElementById('btnConsultar').disabled = false;
							document.getElementById('btnIncluir').disabled = false;
							document.getElementById('btnImportar').disabled = false;
						}else{
							document.getElementById('btnExcluir').disabled = false;
							document.getElementById('btnAlterar').disabled = false;
							document.getElementById('btnConsultar').disabled = true;
							document.getElementById('btnIncluir').disabled = true;
							document.getElementById('btnImportar').disabled = true;
						}
					} else {
						document.getElementById('btnExcluir').disabled = true;
						document.getElementById('btnAlterar').disabled = true;
						document.getElementById('btnConsultar').disabled = false;
						document.getElementById('btnIncluir').disabled = false;
						document.getElementById('btnImportar').disabled = false;
					}
				} else {
					document.getElementById('btnExcluir').disabled = true;
					document.getElementById('btnAlterar').disabled = true;
					document.getElementById('btnConsultar').disabled = false;
					document.getElementById('btnIncluir').disabled = false;
					document.getElementById('btnImportar').disabled = false;
				}
			} else {
				document.getElementById('btnExcluir').disabled = true;
				document.getElementById('btnAlterar').disabled = true;
				document.getElementById('btnConsultar').disabled = false;
				document.getElementById('btnIncluir').disabled = false;
				document.getElementById('btnImportar').disabled = false;
			}
		} else {
			document.getElementById('btnExcluir').disabled = true;
			document.getElementById('btnAlterar').disabled = true;
			document.getElementById('btnConsultar').disabled = false;
			document.getElementById('btnIncluir').disabled = false;
			document.getElementById('btnImportar').disabled = false;
		}
	}
	
	function OnSubmitFormVincularCandidatoEspecialidade(){
		if(document.pressed == 'Desvincular'){
		   	if (confirm('Tem certeza que deseja desvincular?')){ 
			 	return true;
	   		}else{
				document.pressed = '';
				return false;
			}
	  	}
	}
	
	function OnSubmitFormGerirBaseApuracao(){
		if(document.pressed == 'Excluir Base'){
		   	if (confirm('Deseja realmente excluir toda a Base de Apuracao?')){ 
				document.getElementById('aguarde').style.display='inline';
			 	return true;
	   		}else{
				document.pressed = '';
				document.getElementById('aguarde').style.display='none';
				return false;
			}
	  	}
	}