<?php
require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/business/QtdeVotantesLocaisBO.php"); 

$acao = isset($_REQUEST["action"])?$_REQUEST["action"]:""; 
$acao = empty($acao)?$_REQUEST["botao"]:$acao;
$bo = new QtdeVotantesLocaisBO($acao);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once('head.php');?>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="gerenciaBotoes()">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
      <tr>
        <td colspan="2"><?php include_once('cabecalho.php');?></td>
      </tr>
      <tr>
        <td width="210" valign="top"><?php include_once('menu.php');?></td>
        <td valign="top">
         <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="form" name="form" enctype="multipart/form-data" onsubmit="return OnSubmitForm();" >
        <table width="99%" border="0" cellspacing="0" cellpadding="0">
	        <tr>
				<td height="10">&nbsp;</td>
			</tr>
			<tr>
				<td class="TituloSecao"><img src="images/spacer.gif" width="15"									border="0" alt="">INFORMAR QTDE VOTANTES CPE
				</td>
			</tr>
			<tr>
				<td height="10">&nbsp;</td>
			</tr>
            <tr>
				<td height="10"> 
				<?php 
					$msgSucessos = $bo->getMsgSucessosArr(); 
					if (!empty($msgSucessos)){
				?>
            <div class="MensagemSucesso">
            	<?php 
					foreach ($msgSucessos as $row) {
						echo $row;
					}
				?>
			</div>
            	<?php 
					} else { echo "&nbsp;"; }
					$msgErros = $bo->getMsgErrosArr();
					if(!empty($msgErros)){
				?>
            <div class="MensagemErro">
				<?php 
					foreach ($msgErros as $row) {
						echo $row;
					}
				?>
			</div>
            <?php 
					} else { echo "&nbsp;"; }
			?>
</td>
			</tr>
            <tr>
				<td height="10">&nbsp;</td>
			</tr>
			<tr>
			  <td valign="top">
             
              <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td align="right"><input type="submit" name="botao" id="btnSalvar" value="Salvar" class="BotaoMaior" /></td>
                </tr>
              </table>
              </td>
		  </tr>
			<tr>
			  <td valign="top">
              	<fieldset >
			      <legend>Locais Cadastrados </legend>
                  <?php 
				  		$lista = $bo->getListaLocaisVotacaoArr();
						if(!empty($lista)){
				  ?>
                  <input type="hidden" name="action" id="action" value="<?php echo $acao; ?>" />
			      <table class="paginador" width="98%">
							<tr>
								<td width="85%" class="header">Local</td>
								<td width="15%" class="header"><div align="center">Qtde. Votantes</div></td>
							</tr>					    
							
							<?php
                            	foreach ($lista as $row) {
							?>
			                  <tr onMouseOver="this.className='selecionada'" onMouseOut="this.className='inicial'">
									<td class="body"><?php echo $row->getDesLocal(); ?><input name="idLocaisVotacao[]" type="hidden" id="idLocaisVotacao[]" value="<?php echo $row->getIdLocal(); ?>" /></td>
									<td class="body">
									  <div align="right">
									    <input name="qtdPresentes[]" type="text" id="qtdPresentes[]" size="15" maxlength="10" value="<?php echo $row->getQtdPresentes(); ?>" onkeypress="return Bloqueia_Caracteres(event);"  autocomplete="off"/>
							          </div></td>
					</tr>
                    		<?php } ?>
			    </table>
                  	<?php 
				  		} else {
							echo "&nbsp;&nbsp;&nbsp;N&atilde;o foram encontrados registros nesta consulta.";		
						}	
					?>
				</fieldset>
</td>
		  </tr>
		</table>
		 </form>
        </td>
      </tr>
      <tr>
        <td colspan="2"><?php include_once('rodape.php');?></td>
      </tr>
</table>

</body>
</html>
