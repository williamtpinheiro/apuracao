<?php 
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/library/phpjasperxml_0.8/class/fpdf/fpdf.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/library/phpjasperxml_0.8/class/PHPJasperXML.inc.php");
	include_once ($_SERVER['DOCUMENT_ROOT']."/apuracao/library/phpjasperxml_0.8/setting.php");
	include_once ($_SERVER['DOCUMENT_ROOT']."/apuracao/library/uteis.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/dao/BaseApuracaoDAO.php");
	$daoBaseApuracao = new BaseApuracaoDAO();
	$existeApuracao = $daoBaseApuracao->verificarExistenciaApuracao();
	if($existeApuracao){
		$qtdLocaisVotacaoApuracao = $daoBaseApuracao->verificarExistenciaLocaisApuracao();
		
		if(count($qtdLocaisVotacaoApuracao) >= 23){
			$listaVotosNulosLocaisArr = $daoBaseApuracao->listarVotosNulosLocais();
			$listaVotosBrancosLocaisArr = $daoBaseApuracao->listarVotosBrancosLocais();
			$listaLocaisArr = $daoBaseApuracao->verificarExistenciaLocaisApuracao();
			$nomesLocaisArr;
			$idLocaisArr;
			foreach($listaLocaisArr as $row){
				$nomeLocal = utf8_decode($row->getDesLocal());
				$idLocal = $row->getIdLocal();
				$nomesLocaisArr[] = $nomeLocal;
				$idLocaisArr[] = $idLocal;
			}
			
		
			
			$diretorio = getcwd(); 
			$diretorio.= "/reports/";
			$xml = simplexml_load_file($diretorio."votosGrupoLocal.jrxml");
			$PHPJasperXML = new PHPJasperXML();
			$PHPJasperXML->debugsql=false; 
			//Passo os parametros
			$PHPJasperXML->arrayParameter=array("TITULO"=>'Elei��o de Delegados / Conselho Social � APURA��O',"SUBTITULO"=>'TOTAL DE VOTOS POR GRUPO/LOCAL', "DATA"=>formataDataBr(date("d/m/Y")), "NOME_LOCAL1"=>$nomesLocaisArr[0], "NOME_LOCAL2"=>$nomesLocaisArr[1], "NOME_LOCAL3"=>$nomesLocaisArr[2], "NOME_LOCAL4"=>$nomesLocaisArr[3], "NOME_LOCAL5"=>$nomesLocaisArr[4], "NOME_LOCAL6"=>$nomesLocaisArr[5], "NOME_LOCAL7"=>$nomesLocaisArr[6], "NOME_LOCAL8"=>$nomesLocaisArr[7], "NOME_LOCAL9"=>$nomesLocaisArr[8], "NOME_LOCAL10"=>$nomesLocaisArr[9], "NOME_LOCAL11"=>$nomesLocaisArr[10], "NOME_LOCAL12"=>$nomesLocaisArr[11], "NOME_LOCAL13"=>$nomesLocaisArr[12], "NOME_LOCAL14"=>$nomesLocaisArr[13], "NOME_LOCAL15"=>$nomesLocaisArr[14], "NOME_LOCAL16"=>$nomesLocaisArr[15], "NOME_LOCAL17"=>$nomesLocaisArr[16], "NOME_LOCAL18"=>$nomesLocaisArr[17], "NOME_LOCAL19"=>$nomesLocaisArr[18], "NOME_LOCAL20"=>$nomesLocaisArr[19], "NOME_LOCAL21"=>$nomesLocaisArr[20], "NOME_LOCAL22"=>$nomesLocaisArr[21], "NOME_LOCAL23"=>$nomesLocaisArr[22],"ID_LOCAL1"=>$idLocaisArr[0],"ID_LOCAL2"=>$idLocaisArr[1],"ID_LOCAL3"=>$idLocaisArr[2],"ID_LOCAL4"=>$idLocaisArr[3],"ID_LOCAL5"=>$idLocaisArr[4],"ID_LOCAL6"=>$idLocaisArr[5],"ID_LOCAL7"=>$idLocaisArr[6],"ID_LOCAL8"=>$idLocaisArr[7],"ID_LOCAL9"=>$idLocaisArr[8],"ID_LOCAL10"=>$idLocaisArr[9],"ID_LOCAL11"=>$idLocaisArr[10],"ID_LOCAL12"=>$idLocaisArr[11],"ID_LOCAL13"=>$idLocaisArr[12],"ID_LOCAL14"=>$idLocaisArr[13],"ID_LOCAL15"=>$idLocaisArr[14],"ID_LOCAL16"=>$idLocaisArr[15],"ID_LOCAL17"=>$idLocaisArr[16],"ID_LOCAL18"=>$idLocaisArr[17],"ID_LOCAL19"=>$idLocaisArr[18],"ID_LOCAL20"=>$idLocaisArr[19],"ID_LOCAL21"=>$idLocaisArr[20],"ID_LOCAL22"=>$idLocaisArr[21],"ID_LOCAL23"=>$idLocaisArr[22],"NUM_VOTOS_NULOS_LOCAL1"=>$listaVotosNulosLocaisArr[0],"NUM_VOTOS_NULOS_LOCAL2"=>$listaVotosNulosLocaisArr[1],"NUM_VOTOS_NULOS_LOCAL3"=>$listaVotosNulosLocaisArr[2],"NUM_VOTOS_NULOS_LOCAL4"=>$listaVotosNulosLocaisArr[3],"NUM_VOTOS_NULOS_LOCAL5"=>$listaVotosNulosLocaisArr[4],"NUM_VOTOS_NULOS_LOCAL6"=>$listaVotosNulosLocaisArr[5],"NUM_VOTOS_NULOS_LOCAL7"=>$listaVotosNulosLocaisArr[6],"NUM_VOTOS_NULOS_LOCAL8"=>$listaVotosNulosLocaisArr[7],"NUM_VOTOS_NULOS_LOCAL9"=>$listaVotosNulosLocaisArr[8],"NUM_VOTOS_NULOS_LOCAL10"=>$listaVotosNulosLocaisArr[9],"NUM_VOTOS_NULOS_LOCAL11"=>$listaVotosNulosLocaisArr[10],"NUM_VOTOS_NULOS_LOCAL12"=>$listaVotosNulosLocaisArr[11],"NUM_VOTOS_NULOS_LOCAL13"=>$listaVotosNulosLocaisArr[12],"NUM_VOTOS_NULOS_LOCAL14"=>$listaVotosNulosLocaisArr[13],"NUM_VOTOS_NULOS_LOCAL15"=>$listaVotosNulosLocaisArr[14],"NUM_VOTOS_NULOS_LOCAL16"=>$listaVotosNulosLocaisArr[15],"NUM_VOTOS_NULOS_LOCAL17"=>$listaVotosNulosLocaisArr[16],"NUM_VOTOS_NULOS_LOCAL18"=>$listaVotosNulosLocaisArr[17],"NUM_VOTOS_NULOS_LOCAL19"=>$listaVotosNulosLocaisArr[18],"NUM_VOTOS_NULOS_LOCAL20"=>$listaVotosNulosLocaisArr[19],"NUM_VOTOS_NULOS_LOCAL21"=>$listaVotosNulosLocaisArr[20],"NUM_VOTOS_NULOS_LOCAL22"=>$listaVotosNulosLocaisArr[21],"NUM_VOTOS_NULOS_LOCAL23"=>$listaVotosNulosLocaisArr[22],"NUM_VOTOS_BRANCOS_LOCAL1"=>$listaVotosBrancosLocaisArr[0],"NUM_VOTOS_BRANCOS_LOCAL2"=>$listaVotosBrancosLocaisArr[1],"NUM_VOTOS_BRANCOS_LOCAL3"=>$listaVotosBrancosLocaisArr[2],"NUM_VOTOS_BRANCOS_LOCAL4"=>$listaVotosBrancosLocaisArr[3],"NUM_VOTOS_BRANCOS_LOCAL5"=>$listaVotosBrancosLocaisArr[4],"NUM_VOTOS_BRANCOS_LOCAL6"=>$listaVotosBrancosLocaisArr[5],"NUM_VOTOS_BRANCOS_LOCAL7"=>$listaVotosBrancosLocaisArr[6],"NUM_VOTOS_BRANCOS_LOCAL8"=>$listaVotosBrancosLocaisArr[7],"NUM_VOTOS_BRANCOS_LOCAL9"=>$listaVotosBrancosLocaisArr[8],"NUM_VOTOS_BRANCOS_LOCAL10"=>$listaVotosBrancosLocaisArr[9],"NUM_VOTOS_BRANCOS_LOCAL11"=>$listaVotosBrancosLocaisArr[10],"NUM_VOTOS_BRANCOS_LOCAL12"=>$listaVotosBrancosLocaisArr[11],"NUM_VOTOS_BRANCOS_LOCAL13"=>$listaVotosBrancosLocaisArr[12],"NUM_VOTOS_BRANCOS_LOCAL14"=>$listaVotosBrancosLocaisArr[13],"NUM_VOTOS_BRANCOS_LOCAL15"=>$listaVotosBrancosLocaisArr[14],"NUM_VOTOS_BRANCOS_LOCAL16"=>$listaVotosBrancosLocaisArr[15],"NUM_VOTOS_BRANCOS_LOCAL17"=>$listaVotosBrancosLocaisArr[16],"NUM_VOTOS_BRANCOS_LOCAL18"=>$listaVotosBrancosLocaisArr[17],"NUM_VOTOS_BRANCOS_LOCAL19"=>$listaVotosBrancosLocaisArr[18],"NUM_VOTOS_BRANCOS_LOCAL20"=>$listaVotosBrancosLocaisArr[19],"NUM_VOTOS_BRANCOS_LOCAL21"=>$listaVotosBrancosLocaisArr[20],"NUM_VOTOS_BRANCOS_LOCAL22"=>$listaVotosBrancosLocaisArr[21],"NUM_VOTOS_BRANCOS_LOCAL23"=>$listaVotosBrancosLocaisArr[22]); 
			$PHPJasperXML->xml_dismantle($xml);
			//$PHPJasperXML->connect($server,$user,$pass,$db);
			$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db);
			$PHPJasperXML->outpage("I");
		} else {
			echo "<script>alert('Para exibicao deste relatorio, a Apuracao deve conter 23 locais de votacao.');</script> <a href='javascript:window.close()'>Clique aqui para fechar esta Janela e retornar ao sistema.</a>";
		}
	} else {
		echo "<script>alert('Nao existe base de apuracao para exibicao deste relatorio.');</script> <a href='javascript:window.close()'>Clique aqui para fechar esta Janela e retornar ao sistema.</a>";
	}

?>