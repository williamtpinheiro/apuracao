<?php 
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/business/CandidatosBO.php"); 
	$bo = new CandidatosBO($acao);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once('head.php');?>
</head>

<body>
<form id="form" name="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
  &nbsp;
  <table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
				<td height="10">&nbsp;</td>
	</tr>
			<tr>
				<td class="TituloSecao"><img src="images/spacer.gif" width="15"									border="0" alt="">Enviar Arquivo XML
				</td>
			</tr>
			<tr>
				<td height="10">&nbsp;</td>
			</tr>
            <tr>
				<td height="10"> 
				<?php 
					$msgSucessos = $bo->getMsgSucessosArr(); 
					if (!empty($msgSucessos)){
				?>
            <div class="MensagemSucesso">
            	<?php 
					foreach ($msgSucessos as $row) {
						echo $row;
					}
				?>
			</div>
            	<?php 
					} else { echo "&nbsp;"; }
					$msgErros = $bo->getMsgErrosArr();
					if(!empty($msgErros)){
				?>
            <div class="MensagemErro">
				<?php 
					foreach ($msgErros as $row) {
						echo $row;
					}
				?>
			</div>
            <?php 
					} else { echo "&nbsp;"; }
			?>
			  </td>
			</tr>
            <tr>
				<td height="10">&nbsp;</td>
			</tr>
    <tr>
      <td width="284">Selecione o Arquivo <span class="campoObrigatorio">*</span><img src="images/spacer.gif" width="1" height="20"									border="0" alt="" /></td>
    </tr>
    <tr>
      <td><input type="file" name="arquivoXML" id="arquivoXML" /></td>
    </tr>
    <tr>
      <td>&nbsp;<div align="center"><div align="center" id="aguarde" style="display: none;"> <img src="images/ajax-loader.gif" width="31" height="31" /><br />  
Aguarde...</div></div></td>
    </tr>
    <tr>
      <td><input type="submit" name="botao" id="btnIncluir" value="Enviar" class="BotaoMaior" onclick="document.pressed=this.value;document.getElementById('aguarde').style.display='inline'" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
			  <td valign="top">
              <?php 
				  		$resultado = $bo->getResultadoArquivo();
						if(!is_null($resultado)){
							$lista = $resultado->getListaRegistrosArr();
							if(($resultado->getTotalRegistrosIncluido() + $resultado->getTotalRegistrosErro() ) != 0){
				  ?>
              	<fieldset >
			      <legend>Resultado do Processamento </legend>
                  
			      <table class="paginador" width="98%">
                    <tr onmouseover="this.className='selecionada'" onmouseout="this.className='inicial'">
                      <td width="44%" class="header">Total de itens processados</td>
                      <td width="56%" class="body"><?php echo $resultado->getTotalRegistrosIncluido() + $resultado->getTotalRegistrosErro(); ?></td>
                    </tr>
                    <tr onmouseover="this.className='selecionada'" onmouseout="this.className='inicial'">
                      <td class="header">Com Sucesso</td>
                      <td class="body"><?php echo $resultado->getTotalRegistrosIncluido(); ?></td>
                    </tr>
                    <tr onmouseover="this.className='selecionada'" onmouseout="this.className='inicial'">
                      <td class="header">Com Erro</td>
                      <td class="body"><?php echo $resultado->getTotalRegistrosErro(); ?></td>
                    </tr>
                  </table>
			      <br />
                  <?php if(!empty($lista)){ ?>
			      Lista de Erros
			      <table class="paginador" width="98%">
							<tr>
								<td width="14%" class="header"><div align="center">Item</div></td>
								<td width="86%" class="header">Mensagem</td>
							</tr>					    
							
							<?php
                            	foreach ($lista as $row) {
									if($row->getCodigo() == 2){
							?>
			                  <tr onMouseOver="this.className='selecionada'" onMouseOut="this.className='inicial'">
									<td class="body"><div align="center"><?php echo $row->getItem(); ?></div></td>
									<td class="body"><?php echo $row->getMsg(); ?></td>
					</tr>
                    		<?php } } ?>
			    </table>
	      
                  	<?php 
				  		} 
					?>
				</fieldset>
                <?php } } ?>
</td>
		  </tr>
		</table>

        </td>
      </tr>
  </table>
      &nbsp;&nbsp;&nbsp;  
</form>
</body>
</html>
