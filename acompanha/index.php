<?php
require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/business/ApuracaoBO.php"); 

//$acao = isset($_REQUEST["action"])?$_REQUEST["action"]:""; 
//$acao = empty($acao)?$_REQUEST["botao"]:$acao;
$bo = new ApuracaoBO('AcompanhaOnline');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/head.php");?>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="JavaScript:timedRefresh(20000);">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
      <tr>
        <td colspan="2"><?php include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/acompanha/cabecalho.php");?></td>
      </tr>
      <tr>
        <td colspan="2" valign="top">
         <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="form" name="form" enctype="multipart/form-data" >
        <table width="99%" border="0" cellspacing="0" cellpadding="0">
	        <tr>
				<td height="10">&nbsp;</td>
			</tr>
			<tr>
				<td class="TituloSecaoMobile" align="center"><br/><img src="../images/spacer.gif" width="15"									border="0" alt="">ACOMPANHAMENTO ONLINE DA APURAÇÃO</td>
			</tr>
			<tr>
				<td height="10">&nbsp;</td>
			</tr>
            <tr>
				<td height="10"> 
				<?php 
					$msgSucessos = $bo->getMsgSucessosArr(); 
					if (!empty($msgSucessos)){
				?>
            <div class="MensagemSucesso">
            	<?php 
					foreach ($msgSucessos as $row) {
						echo $row;
					}
				?>
			</div>
            	<?php 
					} else { echo "&nbsp;"; }
					$msgErros = $bo->getMsgErrosArr();
					if(!empty($msgErros)){
				?>
            <div class="MensagemErro">
				<?php 
					foreach ($msgErros as $row) {
						echo $row;
					}
				?>
			</div>
            <?php 
					} else { echo "&nbsp;"; }
			?>
</td>
			</tr>
            <tr>
				<td height="10">&nbsp;</td>
			</tr>
			<tr>
			  <td valign="top">
             
              <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td>&nbsp;</td>
                </tr>
               
              </table>
              </td>
		  </tr>
			<tr>
			  <td valign="top">
                 		<?php 
					$lista = $bo->getAcompanhaOnline();
					 
					if(!empty($lista)){
						
						
				  ?>
                 		 <input type="hidden" name="action" id="action" value="<?php echo $acao; ?>" />
			      
						      <table class="tabelaMobile" width="98%" align="center">
								<tr>
									<td width="50% "align="right">CARGO:</td>
									<td width="50%" align="left"><?php echo $lista->getDesEspecialidade(); ?></td>
								</tr>
								<tr>
									<td align="right">VAGAS:</td>
									<td align="left"><?php echo $lista->getQtdVagas(); ?></td>
								</tr>
								<tr>
									<td align="right">Experiência e Confiança:</td>
									<td align="left"><?php echo $lista->getTotalChapa1()." / ".$lista->getTotalPercChapa1(); ?></td>
								</tr>
								<tr>
									<td align="right">Unimed de Todos Nós:</td>
									<td align="left"><?php echo $lista->getTotalChapa2()." / ".$lista->getTotalPercChapa2(); ?></td>
								</tr>
								<tr>
									<td align="right"><B>VOTOS VALIDOS:</B></td>
									<td align="left"><B><?php echo $lista->getTotalValidos()." / ".$lista->getTotalPercValidos(); ?></B></td>
								</tr>
								<tr>
									<td align="right">Votos Brancos:</td>
									<td align="left"><?php echo $lista->getTotalBrancos()." / ".$lista->getTotalPercBrancos(); ?></td>
								</tr>
								<tr>
									<td align="right">Votos Nulos:</td>
									<td align="left"><?php echo $lista->getTotalNulos()." / ".$lista->getTotalPercNulos(); ?></td>
								</tr>
								<tr>
									<td align="right"><B>TOTAL VOTOS:</B></td>
									<td align="left"><B><?php echo $lista->getSomaTotal(); ?></B></td>
								</tr>
								<tr>
									<td align="right"><B>VOTOS APURADOS:</B></td>
									<td align="left"><B><?php echo $lista->getSomaPercTotal(); ?></B></td>
								</tr>
						      </table>
                  			<?php 
				  	    
				  	    } else {
							echo "&nbsp;&nbsp;&nbsp;N&atilde;o foram encontrados dados nesta consulta.";		
					    }	
					?>
				
			</td>
		  </tr>
		</table>
        </td>
      </tr>
       <tr>
                  <td align="center"><br/><input type="submit" name="botao" id="btnAtualizar" value="ATUALIZAR" class="BotaoMaiorMobile" onClick="document.location.reload(true)"/><br/><br/></td>
                </tr>
      <tr>
        <td colspan="2"><?php include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/acompanha/rodape.php");
?></td>
      </tr>
</table>

</body>
</html>