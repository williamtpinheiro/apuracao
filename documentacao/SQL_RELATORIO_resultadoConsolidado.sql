(SELECT DISTINCT(C.NOME) NOME,
E.ID_ESPECIALIDADE ID_ESPECIALIDADE,
E.DES_ESPECIALIDADE DES_ESPECIALIDADE,
E.QTD_VAGAS QTD_VAGAS,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL1} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL1,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL2} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL2,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL3} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL3,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL4} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL4,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL5} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL5,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL6} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL6,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL7} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL7,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL8} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL8,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL9} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL9,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL10} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL10,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL11} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL11,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL12} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL12,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL13} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL13,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL14} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL14,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL15} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL15,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL16} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL16,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL17} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL17,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL18} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL18,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL19} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL19,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL20} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL20,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL21} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL21,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL22} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL22,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL23} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_LOCAL23,
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE AND AC1.CRM = C.CRM) NUM_VOTOS_CANDIDATO_TOTAL,
C.DAT_COOPERACAO DAT_COOPERACAO,
C.DAT_NASCIMENTO DAT_NASCIMENTO,
'1' ORDEM
FROM AP_APURACAO_LOCAL AL, AP_ESPECIALIDADE E, AP_APURACAO_CANDID AC, AP_CANDIDATO C
WHERE AL.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE
	AND AL.ID_APURACAO_LOC = AC.ID_APURACAO_LOC
	AND AC.CRM = C.CRM
ORDER BY  DES_ESPECIALIDADE, ORDEM, NUM_VOTOS_CANDIDATO_TOTAL DESC, DAT_COOPERACAO, DAT_NASCIMENTO, NOME)

UNION ALL

(SELECT DISTINCT('TOTAL V�LIDOS'),
E.ID_ESPECIALIDADE,
E.DES_ESPECIALIDADE,
'',
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL1} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL2} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL3} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL4} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL5} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL6} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL7} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL8} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL9} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL10} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL11} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL12} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL13} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL14} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL15} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL16} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL17} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL18} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL19} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL20} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL21} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL22} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL23} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
'',
'',
'3' ORDEM
FROM AP_APURACAO_LOCAL AL, AP_ESPECIALIDADE E
WHERE AL.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE
ORDER BY  DES_ESPECIALIDADE)

UNION ALL



(SELECT distinct('BRANCOS'),
AL.ID_ESPECIALIDADE,
E.DES_ESPECIALIDADE,
'',
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL1} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL2} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL3} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL4} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL5} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL6} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL7} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL8} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL9} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL10} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL11} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL12} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL13} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL14} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL15} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL16} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL17} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL18} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL19} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL20} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL21} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL22} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL23} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
'', '', '4' ORDEM
FROM AP_APURACAO_LOCAL AL,
     AP_ESPECIALIDADE E
WHERE AL.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE
ORDER BY  DES_ESPECIALIDADE
)

UNION ALL

(SELECT distinct('NULOS'),
AL.ID_ESPECIALIDADE,
E.DES_ESPECIALIDADE,
'',
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL1} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL2} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL3} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL4} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL5} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL6} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL7} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL8} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL9} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL10} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL11} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL12} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL13} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL14} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL15} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL16} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL17} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL18} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL19} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL20} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL21} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL22} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL23} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
'', '', '5' ORDEM
FROM AP_APURACAO_LOCAL AL,
     AP_ESPECIALIDADE E
WHERE AL.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE
ORDER BY  DES_ESPECIALIDADE
)

UNION ALL

(SELECT DISTINCT('TOTAL'),
E.ID_ESPECIALIDADE,
E.DES_ESPECIALIDADE,
'',
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL1} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL1} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL1} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL2} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL2} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL2} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL3} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL3} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL3} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL4} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL4} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL4} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL5} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL5} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL5} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL6} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL6} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL6} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL7} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL7} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL7} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL8} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL8} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL8} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL9} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL9} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL9} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL10} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL10} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL10} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL11} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL11} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL11} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL12} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL12} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL12} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL13} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL13} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL13} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL14} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL14} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL14} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL15} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL15} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL15} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL16} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL16} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL16} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL17} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL17} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL17} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL18} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL18} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL18} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL19} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL19} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL19} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL20} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL20} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL20} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL21} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL21} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL21} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL22} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL22} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL22} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_LOCAL = $P{ID_LOCAL23} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL23} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_LOCAL = $P{ID_LOCAL23} AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
(SELECT IFNULL(SUM(AC1.QTD_VOTOS_CANDID),0) FROM AP_APURACAO_CANDID AC1, AP_APURACAO_LOCAL AL1 WHERE AC1.ID_APURACAO_LOC = AL1.ID_APURACAO_LOC AND AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_BRANCOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE) + (SELECT IFNULL(SUM(AL1.QTD_VOTOS_NULOS),0) FROM AP_APURACAO_LOCAL AL1 WHERE AL1.ID_ESPECIALIDADE = AL.ID_ESPECIALIDADE),
'',
'',
'6' ORDEM
FROM AP_APURACAO_LOCAL AL, AP_ESPECIALIDADE E
WHERE AL.ID_ESPECIALIDADE = E.ID_ESPECIALIDADE
ORDER BY  DES_ESPECIALIDADE)

ORDER BY DES_ESPECIALIDADE, ORDEM, NUM_VOTOS_CANDIDATO_TOTAL DESC, DAT_COOPERACAO, DAT_NASCIMENTO, NOME