<?php

	/**
	* Classe da Tabela de ApuracaoCandidato
	* @name ApuracaoCandidato
	* @version v 1.0 30/08/2011
	* @package com.algartecnologia.model
	* @access public
	*/
	
	Class ApuracaoCandidato {
	
		var $idApuracaoCadid;
		var $crm;
		var $nome;
		var $qtdVotosCandid;
		
		function ApuracaoCandidato () {
		
		}
		
		function getIdApuracaoCadid() {
			return $this->idApuracaoCadid;
		}
		
		function setIdApuracaoCadid($idApuracaoCadid) {
			$this->idApuracaoCadid = $idApuracaoCadid;
		}
						
		function getCRM(){
			return $this->crm;
		}
		
		function setCRM($crm) {
			$this->crm = $crm;
		}
		
		function getNome(){
			return $this->nome;
		}
		
		function setNome($nome) {
			$this->nome = $nome;
		}
		
		function getQtdVotosCandid(){
			return $this->qtdVotosCandid;
		}
		
		function setQtdVotosCandid($qtdVotosCandid) {
			$this->qtdVotosCandid = $qtdVotosCandid;
		}
				
	}
	
?>