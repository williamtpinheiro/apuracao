<?php

	/**
	* Classe da Tabela de CandidatoCargo
	* @name CandidatoCargo
	* @version v 1.0
	* @package com.algartecnologia.model
	* @access public
	*/
	
	Class CandidatoCargo {
		var $idCanditEspec;
		var $idEspecialidade;
		var $crm;
		var $desEspecialidade;
		var $nome;
		
		function getIdCanditEspec() {
			return $this->idCanditEspec;
		}
		
		function setIdCanditEspec($idCanditEspec) {
			$this->idCanditEspec = $idCanditEspec;
		}
		
		function getIdEspecialidade(){
			return $this->idEspecialidade;
		}
		
		function setIdEspecialidade($idEspecialidade) {
			$this->idEspecialidade = $idEspecialidade;
		}
		
		function getCRM() {
			return $this->crm;
		}
		
		function setCRM($crm) {
			$this->crm = $crm;
		}
		
		function getDesEspecialidade() {
			return $this->desEspecialidade;
		}
		
		function setDesEspecialidade($desEspecialidade) {
			$this->desEspecialidade = $desEspecialidade;
		}
		
		function getNome() {
			return $this->nome;
		}
		
		function setNome($nome) {
			$this->nome = $nome;
		}		
	}
	
?>