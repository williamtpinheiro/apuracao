<?php

	/**
	* Classe da Tabela de Candidato
	* @name Candidato
	* @version v 1.0
	* @package com.algartecnologia.model
	* @access public
	*/
	
	Class Candidato {
		var $crm;
		var $nome;
		var $datCooperacao;
		var $datNascimento;
		
		function getCRM() {
			return $this->crm;
		}
		
		function setCRM($crm) {
			$this->crm = $crm;
		}
		
		function getNome(){
			return $this->nome;
		}
		
		function setNome($nome) {
			$this->nome = $nome;
		}
		
		function getDatCooperacao() {
			return $this->datCooperacao;
		}
		
		function setDatCooperacao($datCooperacao) {
			$this->datCooperacao = $datCooperacao;
		}
		
		function getDatNascimento() {
			return $this->datNascimento;
		}
		
		function setDatNascimento($datNascimento) {
			$this->datNascimento = $datNascimento;
		}
		
	}
	
?>