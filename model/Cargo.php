<?php

	/**
	* Classe da Tabela de Cargo
	* @name Cargo
	* @version v 1.0 20/08/2011
	* @package com.algartecnologia.model
	* @access public
	*/
	
	Class Cargo {
		var $idEspecialidade;
		var $desEspecialidade;
		var $qtdVagas;
		
		function getIdEspecialidade() {
			return $this->idEspecialidade;
		}
		
		function setIdEspecialidade($idEspecialidade) {
			$this->idEspecialidade = $idEspecialidade;
		}
		
		function getDesEspecialidade(){
			return $this->desEspecialidade;
		}
		
		function setDesEspecialidade($desEspecialidade) {
			$this->desEspecialidade = $desEspecialidade;
		}
		
		function getQtdVagas() {
			return $this->qtdVagas;
		}
		
		function setQtdVagas($qtdVagas) {
			$this->qtdVagas = $qtdVagas;
		}
		
	}
	
?>