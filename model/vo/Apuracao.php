<?php

	/**
	* Classe VO da Apura��o
	* @name Apuracao
	* @version v 2.0
	* @package com.algartecnologia.model.vo
	* @access public
	*/
	
	Class Apuracao {
	
		var $idApuracaoLoc;
		var $idLocal;
		var $idEspecialidade;
		var $numVotosNulos;
		var $numVotosBrancos;
		var $numVotosValidos;
		var $numVotosGeral;
		var $numVotosCPE;
		var $numVotosLocal;
		var $qtdCedulasEnviadas;
		var $qtdCedulasDevolvidas;
		var $qtdCedulasInvalidadas;
		var $numMesaApuracao;
		var $qtdVotantesSessao;
		var $bitStatus;
		var $listaCandidatosArr;
		
		
		function getIdApuracaoLoc() {
			return $this->idApuracaoLoc;
		}
		
		function setIdApuracaoLoc($idApuracaoLoc) {
			$this->idApuracaoLoc = $idApuracaoLoc;
		}
		
		function getIdLocal() {
			return $this->idLocal;
		}
		
		function setIdLocal($idLocal) {
			$this->idLocal = $idLocal;
		}
		
		function getIdEspecialidade(){
			return $this->idEspecialidade;
		}
		
		function setIdEspecialidade($idEspecialidade) {
			$this->idEspecialidade = $idEspecialidade;
		}
		
		function getNumVotosNulos(){
			return $this->numVotosNulos;
		}
		
		function setNumVotosNulos($numVotosNulos) {
			$this->numVotosNulos = $numVotosNulos;
		}
		
		function getNumVotosBrancos(){
			return $this->numVotosBrancos;
		}
		
		function setNumVotosBrancos($numVotosBrancos) {
			$this->numVotosBrancos = $numVotosBrancos;
		}
		
		function getNumVotosValidos(){
			return $this->numVotosValidos;
		}
		
		function setNumVotosValidos($numVotosValidos) {
			$this->numVotosValidos = $numVotosValidos;
		}
		
		function getNumVotosGeral(){
			return $this->numVotosGeral;
		}
		
		function setNumVotosGeral($numVotosGeral) {
			$this->numVotosGeral = $numVotosGeral;
		}
		
		function getNumVotosCPE(){
			return $this->numVotosCPE;
		}
		
		function setNumVotosCPE($numVotosCPE) {
			$this->numVotosCPE = $numVotosCPE;
		}
		
		function getNumVotosLocal(){
			return $this->numVotosLocal;
		}
		
		function setNumVotosLocal($numVotosLocal) {
			$this->numVotosLocal = $numVotosLocal;
		}
		
		function getQtdCedulasEnviadas(){
			return $this->qtdCedulasEnviadas;
		}
		
		function setQtdCedulasEnviadas($qtdCedulasEnviadas) {
			$this->qtdCedulasEnviadas = $qtdCedulasEnviadas;
		}		
				
		function getQtdCedulasDevolvidas(){
			return $this->qtdCedulasDevolvidas;
		}
		
		function setQtdCedulasDevolvidas($qtdCedulasDevolvidas) {
			$this->qtdCedulasDevolvidas = $qtdCedulasDevolvidas;
		}		
		
		function getQtdCedulasInvalidadas(){
			return $this->qtdCedulasInvalidadas;
		}
		
		function setQtdCedulasInvalidadas($qtdCedulasInvalidadas) {
			$this->qtdCedulasInvalidadas = $qtdCedulasInvalidadas;
		}	
		
		function getNumMesaApuracao(){
			return $this->numMesaApuracao;
		}
		
		function setNumMesaApuracao($numMesaApuracao) {
			$this->numMesaApuracao = $numMesaApuracao;
		}		
				
		
		function getListaCandidatosArr() {
			return $this->listaCandidatosArr;
		}
		
		function setListaCandidatosArr($listaCandidatosArr) {
			$this->listaCandidatosArr = $listaCandidatosArr;
		}
		
		function getQtdVotantesSessao() {
			return $this->qtdVotantesSessao;
		}
		
		function setQtdVotantesSessao($qtdVotantesSessao) {
			$this->qtdVotantesSessao= $qtdVotantesSessao;
		}
		
		function getBitStatus() {
			return $this->bitStatus;
		}
		
		function setBitStatus($bitStatus) {
			$this->bitStatus = $bitStatus;
		}
				
	}
	
?>