<?php

	/**
	* Classe VO do resultado de item do processamento
	* @name ResultadoItemArquivo
	* @version v 1.0
	* @package com.algartecnologia.model.vo
	* @access public
	*/
	
	Class ResultadoItemArquivo {
		var $item;
		var $codigo;
		var $msg;
		
		function getItem() {
			return $this->item;
		}
		
		function setItem($item) {
			$this->item = $item;
		}
		
		function getCodigo(){
			return $this->codigo;
		}
		
		function setCodigo($codigo) {
			$this->codigo = $codigo;
		}
		
		function getMsg() {
			return $this->msg;
		}
		
		function setMsg($msg) {
			$this->msg = $msg;
		}
				
	}
	
?>