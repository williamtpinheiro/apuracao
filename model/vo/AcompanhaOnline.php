<?php

	/**
	* Classe VO da AcompanhaOnline
	* @name ApuracaoOnline
	* @version v 1.0
	* @package com.algartecnologia.model.vo
	* @access public
	*/
	
	Class AcompanhaOnline{
	
		var $idEspecialidade;
		var $desEspecialidade;
		var $qtdVagas;
		var $totalChapa1;
		var $totalPercChapa1;
		var $totalChapa2;
		var $totalPercChapa2;
		var $totalValidos;
		var $totalPercValidos;
		var $totalVotosBrancos;
		var $totalPercBrancos;
		var $totalVotosNulos;
		var $totalPercNulos;
		var $somaTotal;
		var $somaPercTotal;
		
		function getIdEspecialidade() {
			return $this->idEspecialidade;
		}
		
		function setIdEspecialidade($idEspecialidade) {
			$this->idEspecialidade= $idEspecialidade;
		}
		
		function getDesEspecialidade() {
			return $this->desEspecialidade;
		}
		
		function setDesEspecialidade($desEspecialidade) {
			$this->desEspecialidade= $desEspecialidade;
		}
		
		function getQtdVagas(){
			return $this->qtdVagas;
		}
		
		function setQtdVagas($qtdVagas) {
			$this->qtdVagas= $qtdVagas;
		}
		
		function getTotalChapa1(){
			return $this->totalChapa1;
		}
		
		function setTotalChapa1($totalChapa1) {
			$this->totalChapa1= $totalChapa1;
		}
		
		function getTotalPercChapa1(){
			return $this->totalPercChapa1;
		}
		
		function setTotalPercChapa1($totalPercChapa1) {
			$this->totalPercChapa1= $totalPercChapa1;
		}
		
		function getTotalChapa2(){
			return $this->totalChapa2;
		}
		
		function setTotalChapa2($totalChapa2) {
			$this->totalChapa2= $totalChapa2;
		}
		
		function getTotalPercChapa2(){
			return $this->totalPercChapa2;
		}
		
		function setTotalPercChapa2($totalPercChapa2) {
			$this->totalPercChapa2= $totalPercChapa2;
		}
		
		function getTotalValidos(){
			return $this->totalValidos;
		}
		
		function setTotalValidos($totalValidos) {
			$this->totalValidos= $totalValidos;
		}
		
		function getTotalPercValidos(){
			return $this->totalPercValidos;
		}
		
		function setTotalPercValidos($totalPercValidos) {
			$this->totalPercValidos= $totalPercValidos;
		}
		
		function getTotalBrancos(){
			return $this->totalVotosBrancos;
		}
		
		function setTotalBrancos($totalVotosBrancos) {
			$this->totalVotosBrancos= $totalVotosBrancos;
		}
		
		function getTotalPercBrancos(){
			return $this->totalPercBrancos;
		}
		
		function setTotalPercBrancos($totalPercBrancos) {
			$this->totalPercBrancos= $totalPercBrancos;
		}
		
		function getTotalNulos(){
			return $this->totalVotosNulos;
		}
		
		function setTotalNulos($totalVotosNulos) {
			$this->totalVotosNulos= $totalVotosNulos;
		}
		
		function getTotalPercNulos(){
			return $this->totalPercNulos;
		}
		
		function setTotalPercNulos($totalPercNulos) {
			$this->totalPercNulos= $totalPercNulos;
		}
		
		function getSomaTotal(){
			return $this->somaTotal;
		}
		
		function setSomaTotal($somaTotal) {
			$this->somaTotal= $somaTotal;
		}
		
		function getSomaPercTotal(){
			return $this->somaPercTotal;
		}
		
		function setSomaPercTotal($somaPercTotal) {
			$this->somaPercTotal= $somaPercTotal;
		}		
				
	}
	
?>