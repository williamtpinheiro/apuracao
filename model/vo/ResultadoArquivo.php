<?php

	/**
	* Classe VO do resultado geral do processamento
	* @name ResultadoArquivo
	* @version v 1.0 22/08/2011
	* @package com.algartecnologia.model.vo
	* @access public
	*/
	
	Class ResultadoArquivo {
		var $totalRegistrosIncluido;
		var $totalRegistrosErro;
		var $listaRegistrosArr;
		
		function getTotalRegistrosIncluido() {
			return $this->totalRegistrosIncluido;
		}
		
		function setTotalRegistrosIncluido($totalRegistrosIncluido) {
			$this->totalRegistrosIncluido = $totalRegistrosIncluido;
		}
		
		function getTotalRegistrosErro(){
			return $this->totalRegistrosErro;
		}
		
		function setTotalRegistrosErro($totalRegistrosErro) {
			$this->totalRegistrosErro = $totalRegistrosErro;
		}
		
		function getListaRegistrosArr() {
			return $this->listaRegistrosArr;
		}
		
		function setListaRegistrosArr($listaRegistrosArr) {
			$this->listaRegistrosArr = $listaRegistrosArr;
		}
				
	}
	
?>