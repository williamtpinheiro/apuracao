<?php

	/**
	* Classe da Tabela de LocalVotacao
	* @name LocalVotacao
	* @version v 1.0
	* @package com.algartecnologia.model
	* @access public
	*/
	
	Class LocalVotacao {
		var $idLocal;
		var $desLocal;
		var $cidade;
		var $qtdPresentes;
		
		function getIdLocal() {
			return $this->idLocal;
		}
		
		function setIdLocal($idLocal) {
			$this->idLocal = $idLocal;
		}
		
		function getDesLocal(){
			return $this->desLocal;
		}
		
		function setDesLocal($desLocal) {
			$this->desLocal = $desLocal;
		}
		
		function getCidade() {
			return $this->cidade;
		}
		
		function setCidade($cidade) {
			$this->cidade = $cidade;
		}
		
		function getQtdPresentes() {
			return $this->qtdPresentes;
		}
		
		function setQtdPresentes($qtdPresentes) {
			$this->qtdPresentes = $qtdPresentes;
		}
		
	}
	
?>