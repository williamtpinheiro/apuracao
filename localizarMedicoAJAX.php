<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/dao/CandidatosDAO.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/library/uteis.php");
	
	$crm=$_GET["crm"];
	$crm =  AntiSqlInjection($crm);

 	//lookup all hints from array if length of q>0
 	if (strlen($crm) > 0){
   		$hint="";
   		$dao = new CandidatosDAO();
		$resultado = $dao->consultarCandidatoByCRM($crm);
	
		if($resultado){
			$hint = $resultado->getNome();
		}
   }

	// Set output to "no suggestion" if no hint were found
	// or to the correct values
	if ($hint == ""){
	   $response="M&eacute;dico com este CRM n&atilde;o cadastrado como Candidato.";
	} else {
	   $response=$hint;
	}

	 //output the response
 	echo $response;
?>