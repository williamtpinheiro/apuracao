<?php
require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/business/GruposEspecialidadesBO.php"); 
 
  $acao = isset($_REQUEST["action"]) ? $_REQUEST["action"] : ""; 
  $acao = (empty($acao) && !empty($_REQUEST) && isset($_REQUEST["botao"])) ? $_REQUEST["botao"] : $acao;
$bo = new GruposEspecialidadesBO($acao);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once('head.php');?>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="gerenciaBotoes()">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
      <tr>
        <td colspan="2"><?php include_once('cabecalho.php');?></td>
      </tr>
      <tr>
        <td width="210" valign="top"><?php include_once('menuAdm.php');?></td>
        <td valign="top">
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="form" name="form" enctype="multipart/form-data" onsubmit="return OnSubmitForm();" >
        <table width="99%" border="0" cellspacing="0" cellpadding="0">
	        <tr>
				<td height="10">&nbsp;</td>
			</tr>
			<tr>
				<td class="TituloSecao"><img src="../images/spacer.gif" width="15"									border="0" alt="">GERIR CARGOS
				</td>
			</tr>
			<tr>
				<td height="10">&nbsp;</td>
			</tr>
            <tr>
				<td height="10"> 
				<?php 
					$msgSucessos = $bo->getMsgSucessosArr(); 
					if (!empty($msgSucessos)){
				?>
            <div class="MensagemSucesso">
            	<?php 
					foreach ($msgSucessos as $row) {
						echo $row;
					}
				?>
			</div>
            	<?php 
					} else { echo "&nbsp;"; }
					$msgErros = $bo->getMsgErrosArr();
					if(!empty($msgErros)){
				?>
            <div class="MensagemErro">
				<?php 
					foreach ($msgErros as $row) {
						echo $row;
					}
				?>
			</div>
            <?php 
					} else { echo "&nbsp;"; }
			?>
</td>
			</tr>
            <tr>
				<td height="10">&nbsp;</td>
			</tr>
			<tr>
			  <td valign="top">
              
              <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td>
                  
                  <?php 
				  
				  	if($bo->getGrupoEspecialidade() != null) { 
						$registro = $bo->getGrupoEspecialidade();	
				  ?>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td>Cargo<span class="campoObrigatorio">*</span>
                        <input type="hidden" name="id" id="id" value="<?php echo $registro->getIdEspecialidade(); ?>" />
                      <input type="hidden" name="action" id="action" value="<?php echo $acao; ?>" />
                     </td>
                    </tr>
                    <tr>
                      <td><input name="desEspecialidade" type="text" id="desEspecialidade" size="60" maxlength="100" value="<?php echo $registro->getDesEspecialidade(); ?>" /></td>
                    </tr>
                    <tr>
                      <td>Qtde de Vagas <span class="campoObrigatorio">*</span><img src="../images/spacer.gif" width="1" height="20"									border="0" alt=""></td>
                    </tr>
                    <tr>
                      <td><input name="qtdVagas" type="text" id="qtdVagas" size="30" maxlength="10" value="<?php echo $registro->getQtdVagas(); ?>" onkeypress="return Bloqueia_Caracteres(event);"  autocomplete="off" /></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                  <?php } else { ?>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td>Cargo <span class="campoObrigatorio">*</span><img src="../images/spacer.gif" width="1" height="20"									border="0" alt="">
					    <input type="hidden" name="action" id="action" value="<?php echo $acao; ?>" /></td>
                    </tr>
                    <tr>
                      <td><input name="desEspecialidade" type="text" id="desEspecialidade" size="60" maxlength="100" /></td>
                    </tr>
                    <tr>
                      <td>Qtde Vagas <span class="campoObrigatorio">*</span><img src="../images/spacer.gif" width="1" height="20"									border="0" alt=""></td>
                    </tr>
                    <tr>
                      <td><input name="qtdVagas" type="text" id="qtdVagas" size="30" maxlength="10" onkeypress="return Bloqueia_Caracteres(event);" /></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                  
                  <?php } ?>
                  
                  </td>
                </tr>
                <tr>
                  <td align="right"><input type="submit" name="botao" id="btnIncluir" value="Incluir" class="BotaoMaior" />
                    <input type="submit" name="botao" id="btnConsultar" value="Consultar" class="BotaoMaior" />
                    <input type="submit" name="botao" id="btnAlterar" value="Alterar" class="BotaoMaior" />
                  <input type="submit" name="botao" id="btnExcluir" value="Excluir" class="BotaoMaior" onclick="document.pressed=this.value" /></td>
                </tr>
                <tr>
                  <td align="right">&nbsp;</td>
                </tr>
              </table>
             
              <br /></td>
		  </tr>
			<tr>
			  <td valign="top">
              	<fieldset >
			      <legend>Cargos Cadastrados </legend>
                  <?php 
				  		$lista = $bo->getListaGruposEspecialidadesArr();
						if(!empty($lista)){
				  ?>
			      <table class="paginador" width="98%">
							<tr>
								<td width="66%" class="header">Cargo</td>
								<td width="34%" class="header">Qtde de Vagas</td>
							</tr>					    
							
							<?php
                            	foreach ($lista as $row) {
							?>
			                  <tr onMouseOver="this.className='selecionada'" onMouseOut="this.className='inicial'">
									<td class="body"><a href="gerirGruposEspecialidades.php?action=edit&id=<?php echo $row->getIdEspecialidade(); ?>" class="TabelaLink"><?php echo $row->getDesEspecialidade(); ?></a></td>
									<td class="body"><a href="gerirGruposEspecialidades.php?action=edit&id=<?php echo $row->getIdEspecialidade(); ?>" class="TabelaLink"><?php echo $row->getQtdVagas(); ?></a></td>
					</tr>
                    		<?php } ?>
			    </table>
				<table>
                	<tr>
                    	<td>
					    <input type="submit" name="btnPrimeira" id="btnPrimeira" value="" onclick="document.pressed='Primeira'" class="PaginacaoPrimeira"/>&nbsp;
                        <input type="submit" name="btnAnterior" id="btnAnterior" value="" onclick="document.pressed='Anterior'" class="PaginacaoAnterior"/>&nbsp;
                        <td valign="middle">
                        <input type="hidden" id="paginaAtual" name="paginaAtual" value="<?php echo $bo->getPaginaAtual(); ?>"/><?php echo $bo->getPaginaAtual(); ?>  / <?php echo $bo->getNumPaginas(); ?> <input type="hidden" id="totalPaginas" name="totalPaginas" value="<?php echo $bo->getNumPaginas(); ?>"/> &nbsp;
                        </td>
                        <td>
                        <input type="submit" name="btnPosterior" id="btnPosterior" value="" onclick="document.pressed='Posterior'" class="PaginacaoPosterior"/>&nbsp;
                        <input type="submit" name="btnUltima" id="btnUltima" value="" onclick="document.pressed='Ultima';" class="PaginacaoUltima"/>
                  	<?php 
				  		} else {
							if(isset($_REQUEST["botao"] ) && $_REQUEST["botao"] != "Consultar"){
								echo "&nbsp;&nbsp;&nbsp;N&atilde;o foram encontrados registros neste cadastro.";
							} else {
								echo "&nbsp;&nbsp;&nbsp;N&atilde;o foram encontrados registros nesta consulta.";
							}
						}	
					?>
                    	</td>
                    </tr>
                </table>
					</fieldset>
</td>
		  </tr>
		</table>
 </form>
        </td>
      </tr>
      <tr>
        <td colspan="2"><?php include_once('rodape.php');?></td>
      </tr>
</table>

</body>
</html>
