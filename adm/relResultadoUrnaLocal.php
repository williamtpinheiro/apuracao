<?php 
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/library/phpjasperxml_0.8/class/fpdf/fpdf.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/library/phpjasperxml_0.8/class/PHPJasperXML.inc.php");
	include_once ($_SERVER['DOCUMENT_ROOT']."/apuracao/library/phpjasperxml_0.8/setting.php");
	include_once ($_SERVER['DOCUMENT_ROOT']."/apuracao/library/uteis.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/dao/BaseApuracaoDAO.php");
	
	
	/**
	* Arquivo PHP de acesso ao Relat�rio de Resultado por Urna / Local
	* @name relResultadoUrnaLocal.php
	* @version v 1.0 
	* @package com.algartecnologia
	* @access public
	*/	
	
	$daoBaseApuracao = new BaseApuracaoDAO();
	$existeApuracao = $daoBaseApuracao->verificarExistenciaApuracao();
	if($existeApuracao){
		$qtdLocaisVotacaoApuracao = $daoBaseApuracao->verificarExistenciaLocaisApuracao();
		
		if(count($qtdLocaisVotacaoApuracao) > 1){
			
			if(isset($_REQUEST['idLocal'])){
				$idLocal = $_REQUEST['idLocal'];
				$idLocal =  AntiSqlInjection($idLocal);	
					
				$diretorio = getcwd();  
				$diretorio.= "/reports/";
				$xml = simplexml_load_file($diretorio."resultadoUrnaLocal.jrxml");
				$PHPJasperXML = new PHPJasperXML();
				$PHPJasperXML->debugsql=false; 
				//Passo os parametros
				$PHPJasperXML->arrayParameter=array("TITULO"=>utf8_decode('ELEIÇÃO DO CONSELHO FISCAL DA UNIMED-BH'),"TITULO2"=>'',"SUBTITULO"=>'DATA: '.date('d/m/Y'),"ID_LOCAL"=>$idLocal,"ID_CRM1"=>1,"ID_CRM2"=>2,"NOME_CRM1"=>utf8_decode('Experiência e Responsabilidade'),"NOME_CRM2"=>utf8_decode('Novos Caminhos, Novas Conquistas')); 
				$PHPJasperXML->xml_dismantle($xml);
				//$PHPJasperXML->connect($server,$user,$pass,$db);
				$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db);
				$PHPJasperXML->outpage("I");
			} else {
				echo "<script>alert('O local informado nao foi encontrado.');</script> <a href='javascript:window.close()'>Clique aqui para fechar esta Janela e retornar ao sistema.</a>";
			}
		} else {
			echo "<script>alert('Para exibicao deste relatorio, a Apuracao deve conter pelo menos 1 local de votacao.');</script> <a href='javascript:window.close()'>Clique aqui para fechar esta Janela e retornar ao sistema.</a>";
		}
	} else {
		echo "<script>alert('Nao existe base de apuracao para exibicao deste relatorio.');</script> <a href='javascript:window.close()'>Clique aqui para fechar esta Janela e retornar ao sistema.</a>";
	}

?>