<?php


require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/business/ApuracaoBO.php"); 
 
$acao = isset($_REQUEST["action"])?$_REQUEST["action"]:""; 
$acao = !isset($acao) ? $_REQUEST["botao"] : $acao;
$bo = new ApuracaoBO($acao);
$registro;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once('head.php');?>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
      <tr>
        <td colspan="2"><?php include_once('cabecalho.php');?></td>
      </tr>
      <tr>
        <td width="210" valign="top"><?php include_once('menuAdm.php');?></td>
        <td valign="top">
         <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="form" name="form" enctype="multipart/form-data">
        <table width="99%" border="0" cellspacing="0" cellpadding="0">
	        <tr>
				<td height="10">&nbsp;</td>
			</tr>
			<tr>
				<td class="TituloSecao"><img src="../images/spacer.gif" width="15"									border="0" alt="">GERIR APURAÇÃO
				</td>
			</tr>
			<tr>
				<td height="10">&nbsp;</td>
			</tr>
            <tr>
				<td height="10"> 
				<?php 
					$msgSucessos = $bo->getMsgSucessosArr(); 
					if (!empty($msgSucessos)){
				?>
            <div class="MensagemSucesso">
            	<?php 
					foreach ($msgSucessos as $row) {
						echo $row;
					}
				?>
			</div>
            	<?php 
					} else { echo "&nbsp;"; }
					$msgErros = $bo->getMsgErrosArr();
					if(!empty($msgErros)){
				?>
            <div class="MensagemErro">
				<?php 
					foreach ($msgErros as $row) {
						echo $row;
					}
				?>
			</div>
            <?php 
					} else { echo "&nbsp;"; }
			?>
</td>
			</tr>
            <tr>
				<td height="10">&nbsp;</td>
			</tr>
			<tr>
			  <td valign="top">
             
              <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td>
                  
                  <?php 
				  
				  	if($bo->getApuracao() != null) { 
						$registro = $bo->getApuracao();	
				  ?>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td colspan="4">Local de Votação <span class="campoObrigatorio">*</span>
                        <input type="hidden" name="action" id="action" value="<?php echo $acao; ?>" />
                        <input type="hidden" name="idApuracaoLoc" id="idApuracaoLoc" value="<?php echo $registro->getIdApuracaoLoc(); ?>" /></td>
                      <td rowspan="2" valign="bottom"><div align="center"><span class="campoObrigatorio"></span></div></td>
                      <td rowspan="2"><div align="center"></div>                        <div align="center"></div></td>
                      <td rowspan="2" valign="bottom">&nbsp;</td>
                    </tr> 
                    <tr>
                      <td colspan="4">
                      <?php 
					  		$listaLocaisVotacao = $bo->getListaLocaisVotacaoArr();
							if(!empty($listaLocaisVotacao)){
						?>
                      <select name="idLocal" class="SelectGrande" id="idLocal" onchange="document.forms['form'].submit();" >
                        <option>SELECIONAR</option>
                        <?php 
							foreach ($listaLocaisVotacao as $row) {
						?>
                        <option value="<?php echo $row->getIdLocal(); ?>" <?php if($registro->getIdLocal() == $row->getIdLocal()) echo "selected='selected'";?>><?php echo $row->getDesLocal(); ?></option>
                         <?php } ?>
                      </select>
                      <?php
                      	} else { echo "N&atilde;o existem Locais de Vota&ccedil;&atilde;o cadastrado."; }
					  ?></td>
                      </tr>
                    <tr>
                      <td colspan="4">Cargo <span class="campoObrigatorio">*</span><img src="../images/spacer.gif" width="1" height="18" border="0" alt=""> </td>
                      <td width="12%" rowspan="2"><div align="center">
                        
                      </div></td>
                      <td width="4%" rowspan="2"><div align="center"></div></td>
                      <td width="12%" rowspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">
                      <?php 
					  		$listaGruposEspecialidades = $bo->getListaGruposEspecialidadesArr();
							if(!empty($listaGruposEspecialidades)){
						?>
                      <select name="idEspecialidade" class="SelectGrande" id="idEspecialidade" onchange="document.forms['form'].submit();" >
                        <option>SELECIONAR</option>
                        <?php 
							foreach ($listaGruposEspecialidades as $row) {
						?>
                        <option value="<?php echo $row->getIdEspecialidade(); ?>" <?php if($registro->getIdEspecialidade() == $row->getIdEspecialidade() || count($listaGruposEspecialidades) == 1) echo "selected='selected'";?>><?php echo $row->getDesEspecialidade(); ?></option>
                         <?php } ?>
                      </select>
                      <?php
                      	} else { echo "N&atilde;o existem Grupos de Especialidades cadastrado."; }
					  ?>
                      </td>
                      </tr>
                    <tr>
                      <td width="18%">Votos Nulos<img src="../images/spacer.gif" width="1" height="18" border="0" alt=""><span class="campoObrigatorio">*</span> </td>
                      <td width="18%" valign="bottom">Votos Brancos<span class="campoObrigatorio"> *</span> </td>
                      <td width="18%" valign="bottom">Votos Válidos</td>
                      <td width="18%" valign="bottom">Total Geral</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td><input name="numVotosNulos" type="text" id="numVotosNulos" size="10" maxlength="10" value="<?php echo $registro->getNumVotosNulos(); ?>" <?php if(is_null($registro->getIdApuracaoLoc())){ echo "readonly='readonly' class='ReadOnly'"; } ?> onkeypress="return Bloqueia_Caracteres(event);"  autocomplete="off"/></td>
                      <td><input name="numVotosBrancos" type="text" id="numVotosBrancos" size="10" maxlength="10" value="<?php echo $registro->getNumVotosBrancos(); ?>" <?php if(is_null($registro->getIdApuracaoLoc())){ echo "readonly='readonly' class='ReadOnly'"; } ?> onkeypress="return Bloqueia_Caracteres(event);"  autocomplete="off"/></td>
                      <td><input name="numVotosValidos" type="text" id="numVotosValidos" size="10" maxlength="10" value="<?php echo $registro->getNumVotosValidos(); ?>" readonly='readonly' class="ReadOnly"/></td>
                      <td><input name="numVotosGeral" type="text" class="ReadOnly" id="numVotosGeral" value="<?php echo $registro->getNumVotosGeral(); ?>" size="10" maxlength="10" readonly='readonly'/></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    
                    
                    
                    <tr>
                      <td width="18%">Quant. Cédulas Enviadas<img src="../images/spacer.gif" width="1" height="18" border="0" alt=""><span class="campoObrigatorio">*</span> </td>
                      <td width="18%" valign="bottom">Quant. Cédulas Devolvidas<span class="campoObrigatorio"> *</span> </td>
                      <td width="18%" valign="bottom">Quant. Cédulas Invalidadas<span class="campoObrigatorio"> *</span></td>
                      <td width="18%" valign="bottom">Nº Votantes Sessão<span class="campoObrigatorio"> *</span></td>
                      <td width="18%" valign="bottom">Nº Mesa Apuração<span class="campoObrigatorio"> *</span></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td><input name="qtdCedulasEnviadas" type="text" id="qtdCedulasEnviadas" size="10" maxlength="10" value="<?php echo $registro->getQtdCedulasEnviadas(); ?>" <?php if(is_null($registro->getIdApuracaoLoc())){ echo "readonly='readonly' class='ReadOnly'"; } ?> onkeypress="return Bloqueia_Caracteres(event);"  autocomplete="off"/></td>
                      <td><input name="qtdCedulasDevolvidas" type="text" id="qtdCedulasDevolvidas" size="10" maxlength="10" value="<?php echo $registro->getQtdCedulasDevolvidas(); ?>" <?php if(is_null($registro->getIdApuracaoLoc())){ echo "readonly='readonly' class='ReadOnly'"; } ?> onkeypress="return Bloqueia_Caracteres(event);"  autocomplete="off"/></td>
                      <td><input name="qtdCedulasInvalidadas" type="text" id="qtdCedulasInvalidadas" size="10" maxlength="10"  value="<?php echo $registro->getQtdCedulasInvalidadas(); ?>" <?php if(is_null($registro->getIdApuracaoLoc())){ echo "readonly='readonly' class='ReadOnly'"; } ?> onkeypress="return Bloqueia_Caracteres(event);"  autocomplete="off"/></td>
                       <td><input name="qtdVotantesSessao" type="text" id="qtdVotantesSessao" size="10" maxlength="20"  value="<?php echo $registro->getQtdVotantesSessao(); ?>" <?php if(is_null($registro->getIdApuracaoLoc())){ echo "readonly='readonly' class='ReadOnly'"; } ?> onkeypress="return Bloqueia_Caracteres(event);"  autocomplete="off"/></td>
                      <td><input name="numMesaApuracao" type="text" id="numMesaApuracao" size="10" maxlength="20"  value="<?php echo $registro->getNumMesaApuracao(); ?>" <?php if(is_null($registro->getIdApuracaoLoc())){ echo "readonly='readonly' class='ReadOnly'"; } ?> onkeypress="return Bloqueia_Caracteres(event);"  autocomplete="off"/></td>
                      <td>&nbsp;<input type="hidden" name="bitStatus" id="bitStatus" value="<?php echo $registro->getBitStatus(); ?>" /></td>
                      <td>&nbsp;</td>
                    </tr>
                    
                    
                    
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                  <?php } else { ?>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td colspan="4">Local de Votação <span class="campoObrigatorio">*</span>
                        <input type="hidden" name="action" id="action" value="<?php echo $acao; ?>" /></td>
                      <td rowspan="2" valign="bottom"><div align="center">Número de votantes CPE<span class="campoObrigatorio"></span></div></td>
                      <td rowspan="2"><div align="center"></div>                        <div align="center"></div></td>
                      <td rowspan="2" valign="bottom"><div align="center">&nbsp;</div>                        <div align="center"></div></td>
                    </tr> 
                    <tr>
                      <td colspan="4">
                       <?php 
					  		$listaLocaisVotacao = $bo->getListaLocaisVotacaoArr();
							if(!empty($listaLocaisVotacao)){
						?>
                      <select name="idLocal" class="SelectGrande" id="idLocal" >
                        <option>SELECIONAR</option>
                        <?php 
							foreach ($listaLocaisVotacao as $row) {
						?>
                        <option value="<?php echo $row->getIdLocal(); ?>" <?php //if($registro->getCidade() == "BELO HORIZONTE") echo "selected='selected'";?>><?php echo $row->getDesLocal(); ?></option>
                        <?php } ?>
                      </select>
                      <?php
                      	} else { echo "N&atilde;o existem Locais de Vota&ccedil;&atilde;o cadastrado."; }
					  ?></td>
                      </tr>
                    <tr>
                      <td colspan="4">Grupo de Especialidade<img src="../images/spacer.gif" width="1" height="18" border="0" alt=""><span class="campoObrigatorio"> *</span> </td>
                      <td width="12%" rowspan="2"><div align="center">
                        <input name="numVotosCPE" type="text" id="numVotosCPE" size="10" maxlength="10" readonly='readonly' class="ReadOnly"/>
                      </div></td>
                      <td width="4%" rowspan="2"><div align="center"></div></td>
                      <td width="12%" rowspan="2"><div align="center">
                        <input name="numVotosLocal" type="text" id="numVotosLocal" size="10" maxlength="10" readonly='readonly' class="ReadOnly" />
                      </div></td>
                    </tr>
                    <tr>
                      <td colspan="4">
                      <?php 
					  		$listaGruposEspecialidades = $bo->getListaGruposEspecialidadesArr();
							if(!empty($listaGruposEspecialidades)){
						?>
                      <select name="idEspecialidade" class="SelectGrande" id="idEspecialidade" >
                        <option>SELECIONAR</option>
                        <?php 
							foreach ($listaGruposEspecialidades as $row) {
						?>
                        <option value="<?php echo $row->getIdEspecialidade(); ?>" <?php //if($registro->getCidade() == "BELO HORIZONTE") echo "selected='selected'";?>><?php echo $row->getDesEspecialidade(); ?></option>
                         <?php } ?>
                      </select>
                      <?php
                      	} else { echo "N&atilde;o existem Cargos cadastrado."; }
					  ?>
                      </td>
                      </tr>
                    <tr>
                      <td width="18%">Votos Nulos<img src="../images/spacer.gif" width="1" height="18" border="0" alt=""></td>
                      <td width="18%" valign="bottom">Votos Brancos</td>
                      <td width="18%" valign="bottom">Votos Válidos</td>
                      <td width="18%" valign="bottom">Total Geral</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td><input name="numVotosNulos" type="text" id="numVotosNulos" size="10" maxlength="10" readonly='readonly' class="ReadOnly"/></td>
                      <td><input name="numVotosBrancos" type="text" id="numVotosBrancos" size="10" maxlength="10" readonly='readonly' class="ReadOnly"/></td>
                      <td><input name="numVotosValidos" type="text" id="numVotosValidos" size="10" maxlength="10" readonly='readonly' class="ReadOnly"/></td>
                      <td><input name="numVotosGeral" type="text" class="ReadOnly" id="numVotosGeral" size="10" maxlength="10" readonly='readonly'/></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                  <?php } ?>                  </td>
                </tr>
                <tr>
                  <td align="center"><input type="submit" name="botao" id="btnApurar" value="Apurar" class="BotaoMaior" onclick="document.pressed=this.value" />
                    <input type="submit" name="botao" id="btnSalvar" value="Salvar" class="BotaoMaior" onclick="document.pressed=this.value" />
                    <input type="submit" name="botao" id="btnImprimirMapa" value="Imprimir Mapa" class="BotaoMaior" onclick="document.pressed=this.value" /></td>
                </tr>
                <tr>
                  <td align="right">&nbsp;</td>
                </tr>
              </table>
              <br /></td>
		  </tr>
			<tr>
			  <td valign="top">
              <?php
      			  	if(isset($registro) ){
      					if(!is_null($registro->getIdApuracaoLoc())){
      			  ?>
              	<fieldset >
			      <legend>Votos por Candidato</legend>
            <?php 
				  		$lista = $registro->getListaCandidatosArr();
						if(!empty($lista)){
				  ?>
			      <table class="paginador" width="98%">
							<tr>
								<td width="85%" class="header">Nome do Candidato - (CRM)</td>
								<td width="15%" class="header"><div align="center">Total de Votos</div></td>
							</tr>					    
							
							<?php
                            	foreach ($lista as $row) {
							?>
			                  <tr onMouseOver="this.className='selecionada'" onMouseOut="this.className='inicial'">
									<td class="body"><?php echo $row->getNome(); ?><input name="idApuracaoCadid[]" type="hidden" id="idApuracaoCadid[]" value="<?php echo $row->getIdApuracaoCadid(); ?>" /></td>
									<td class="body"><div align="right">
									  <input name="qtdVotosCandid[]" type="text" id="qtdVotosCandid[]" size="15" maxlength="10" value="<?php echo $row->getQtdVotosCandid(); ?>" onkeypress="return Bloqueia_Caracteres(event);" autocomplete="off"/></div></td>
					</tr>
                    		<?php } ?>
			    </table>
                  	<?php 
				  		} else {
							echo "&nbsp;&nbsp;&nbsp;N&atilde;o foram encontrados registros nesta consulta.";
						}	
					?>
					</fieldset>
                    <?php } } ?>
</td>
		  </tr>
		</table>
		</form>
        </td>
      </tr>
      <tr>
        <td colspan="2"><?php include_once('rodape.php');?></td>
      </tr>
</table>

</body>
</html>