<?php 
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/library/phpjasperxml_0.8/class/fpdf/fpdf.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/library/phpjasperxml_0.8/class/PHPJasperXML.inc.php");
	include_once ($_SERVER['DOCUMENT_ROOT']."/apuracao/library/phpjasperxml_0.8/setting.php");
	include_once ($_SERVER['DOCUMENT_ROOT']."/apuracao/library/uteis.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/dao/BaseApuracaoDAO.php");
	
	
	/**
	* Arquivo PHP de acesso ao Relat�rio de Resultado Consolidado
	* @name relResultadoConsolidado.php
	* @version v 2.0 
	* @package com.algartecnologia
	* @access public
	*/	
	
	$daoBaseApuracao = new BaseApuracaoDAO();
	$existeApuracao = $daoBaseApuracao->verificarExistenciaApuracao();
	if($existeApuracao){
		$qtdLocaisVotacaoApuracao = $daoBaseApuracao->verificarExistenciaLocaisApuracao();
		
		if(count($qtdLocaisVotacaoApuracao) >= 23){
			$listaLocaisArr = $daoBaseApuracao->verificarExistenciaLocaisApuracao();
			$nomesLocaisArr;
			$idLocaisArr;
			foreach($listaLocaisArr as $row){
				$nomeLocal = utf8_decode($row->getDesLocal());
				$idLocal = $row->getIdLocal();
				$nomesLocaisArr[] = $nomeLocal;
				$idLocaisArr[] = $idLocal;
			}
			
			$diretorio = getcwd();  
			$diretorio.= "/reports/";
			$xml = simplexml_load_file($diretorio."resultadoConsolidado.jrxml");
			$PHPJasperXML = new PHPJasperXML();
			$PHPJasperXML->debugsql=false; 
			//Passo os parametros
			$PHPJasperXML->arrayParameter=array("TITULO"=>'Elei��o de Delegados / Conselho Social � APURA��O',"SUBTITULO"=>'RESULTADO CONSOLIDADO',"DATA"=>formataDataBr(date("d/m/Y")), "NOME_LOCAL1"=>$nomesLocaisArr[0], "NOME_LOCAL2"=>$nomesLocaisArr[1], "NOME_LOCAL3"=>$nomesLocaisArr[2], "NOME_LOCAL4"=>$nomesLocaisArr[3], "NOME_LOCAL5"=>$nomesLocaisArr[4], "NOME_LOCAL6"=>$nomesLocaisArr[5], "NOME_LOCAL7"=>$nomesLocaisArr[6], "NOME_LOCAL8"=>$nomesLocaisArr[7], "NOME_LOCAL9"=>$nomesLocaisArr[8], "NOME_LOCAL10"=>$nomesLocaisArr[9], "NOME_LOCAL11"=>$nomesLocaisArr[10], "NOME_LOCAL12"=>$nomesLocaisArr[11], "NOME_LOCAL13"=>$nomesLocaisArr[12], "NOME_LOCAL14"=>$nomesLocaisArr[13], "NOME_LOCAL15"=>$nomesLocaisArr[14], "NOME_LOCAL16"=>$nomesLocaisArr[15], "NOME_LOCAL17"=>$nomesLocaisArr[16], "NOME_LOCAL18"=>$nomesLocaisArr[17], "NOME_LOCAL19"=>$nomesLocaisArr[18], "NOME_LOCAL20"=>$nomesLocaisArr[19], "NOME_LOCAL21"=>$nomesLocaisArr[20], "NOME_LOCAL22"=>$nomesLocaisArr[21], "NOME_LOCAL23"=>$nomesLocaisArr[22],"ID_LOCAL1"=>$idLocaisArr[0],"ID_LOCAL2"=>$idLocaisArr[1],"ID_LOCAL3"=>$idLocaisArr[2],"ID_LOCAL4"=>$idLocaisArr[3],"ID_LOCAL5"=>$idLocaisArr[4],"ID_LOCAL6"=>$idLocaisArr[5],"ID_LOCAL7"=>$idLocaisArr[6],"ID_LOCAL8"=>$idLocaisArr[7],"ID_LOCAL9"=>$idLocaisArr[8],"ID_LOCAL10"=>$idLocaisArr[9],"ID_LOCAL11"=>$idLocaisArr[10],"ID_LOCAL12"=>$idLocaisArr[11],"ID_LOCAL13"=>$idLocaisArr[12],"ID_LOCAL14"=>$idLocaisArr[13],"ID_LOCAL15"=>$idLocaisArr[14],"ID_LOCAL16"=>$idLocaisArr[15],"ID_LOCAL17"=>$idLocaisArr[16],"ID_LOCAL18"=>$idLocaisArr[17],"ID_LOCAL19"=>$idLocaisArr[18],"ID_LOCAL20"=>$idLocaisArr[19],"ID_LOCAL21"=>$idLocaisArr[20],"ID_LOCAL22"=>$idLocaisArr[21],"ID_LOCAL23"=>$idLocaisArr[22]); 
			$PHPJasperXML->xml_dismantle($xml);
			//$PHPJasperXML->connect($server,$user,$pass,$db);
			$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db);
			$PHPJasperXML->outpage("I");
		} else {
			echo "<script>alert('Para exibicao deste relatorio, a Apuracao deve conter 23 locais de votacao.');</script> <a href='javascript:window.close()'>Clique aqui para fechar esta Janela e retornar ao sistema.</a>";
		}
	} else {
		echo "<script>alert('Nao existe base de apuracao para exibicao deste relatorio.');</script> <a href='javascript:window.close()'>Clique aqui para fechar esta Janela e retornar ao sistema.</a>";
	}

?>