<?php
require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/business/VincularCandidatoEspecialidadeBO.php"); 

  $acao = isset($_REQUEST["action"]) ? $_REQUEST["action"] : ""; 
  $acao = (empty($acao) && !empty($_REQUEST) && isset($_REQUEST["botao"])) ? $_REQUEST["botao"] : $acao;
$bo = new VincularCandidatoEspecialidadeBO($acao);

$cand = $bo->getCandidatoEspecialidade();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once('head.php');?>
<script type="text/javascript">
	 function localizarMedico(crm){
		 if (crm.length==0){ 
		 	document.getElementById("txtNomeMedico").innerHTML="";
		   	return;
		 }
		 if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		 	xmlhttp=new XMLHttpRequest();
		 } else {// code for IE6, IE5
		   	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		 }
		 xmlhttp.onreadystatechange=function(){
		   	if (xmlhttp.readyState==4 && xmlhttp.status==200){
				document.getElementById("txtNomeMedico").innerHTML=xmlhttp.responseText;
			}
		 }
		 xmlhttp.open("GET","localizarMedicoAJAX.php?crm="+crm,true);
		 xmlhttp.send();
	 }
 </script>
 
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" <? if(!is_null($cand)){ echo "onload='localizarMedico(".$cand->getCRM().")'"; }?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
      <tr>
        <td colspan="2"><?php include_once('cabecalho.php');?></td>
      </tr>
      <tr>
        <td width="210" valign="top"><?php include_once('menuAdm.php');?></td>
        <td valign="top">
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="form" name="form" enctype="multipart/form-data" onsubmit="return OnSubmitFormVincularCandidatoEspecialidade();" >
        <table width="99%" border="0" cellspacing="0" cellpadding="0">
	        <tr>
				<td height="10">&nbsp;</td>
			</tr>
			<tr>
				<td class="TituloSecao"><img src="../images/spacer.gif" width="15"									border="0" alt="">VINCULAR CANDIDATO	A	CARGOS		</td>
		  </tr>
			<tr>
				<td height="10">&nbsp;</td>
			</tr>
            <tr>
				<td height="10"> 
				<?php 
					$msgSucessos = $bo->getMsgSucessosArr(); 
					if (!empty($msgSucessos)){
				?>
            <div class="MensagemSucesso">
            	<?php 
					foreach ($msgSucessos as $row) {
						echo $row;
					}
				?>
			</div>
            	<?php 
					} else { echo "&nbsp;"; }
					$msgErros = $bo->getMsgErrosArr();
					if(!empty($msgErros)){
				?>
            <div class="MensagemErro">
				<?php 
					foreach ($msgErros as $row) {
						echo $row;
					}
				?>
			</div>
            <?php 
					} else { echo "&nbsp;"; }
			?>
</td>
			</tr>
            <tr>
				<td height="10">&nbsp;</td>
			</tr>
			<tr>
			  <td valign="top">
              
              <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td colspan="2">Cargo <span class="campoObrigatorio">*</span>
                      <input type="hidden" name="action" id="action" value="<?php echo $acao; ?>" />                     </td>
                    </tr>
                    <tr>
                      <td colspan="2"><select name="idEspecialidade" id="idEspecialidade" >
                          <option>SELECIONAR</option>
                          <?php  
						  		$listaGruposEspecialidadesArr = $bo->getListaGruposEspecialidadesArr();
						  		if(!empty($listaGruposEspecialidadesArr)){
									foreach($listaGruposEspecialidadesArr as $row){
						  ?>
		                          <option value="<?php echo $row->getIdEspecialidade(); ?>" <?php if(is_null($cand)){ if($row->getIdEspecialidade() == $bo->getGrupoEspecialidadeAtual()){ echo "selected='selected'"; } } else { if($row->getIdEspecialidade() == $cand->getIdEspecialidade()){ echo "selected='SELECTED'";}} ?>><?php echo $row->getDesEspecialidade(); ?></option> 
                        	<?php
								} }
							?>
                                              </select></td>
                    </tr>
                    <tr>
                      <td width="23%">CRM <span class="campoObrigatorio">*</span><img src="../images/spacer.gif" width="1" height="20"									border="0" alt=""></td>
                      <td width="77%">Nome do Candidato<img src="../images/spacer.gif" width="1" height="20"									border="0" alt="" /></td>
                    </tr>
                    <tr>
                      <td><input name="crm" type="text" id="crm" size="10" maxlength="6" value="<?php  if(!is_null($cand)){ echo $cand->getCRM();}?>" onkeyup="localizarMedico(this.value)" onkeypress="return Bloqueia_Caracteres(event);"  autocomplete="off" /></td>
                      <td><span id="txtNomeMedico"></span></td>
                    </tr>
                    <tr>
                      <td colspan="2">&nbsp;</td>
                    </tr>
                  </table>
                  </td>
                </tr>
                <tr>
                  <td align="right"><input type="submit" name="botao" id="btnVincular" value="Vincular" class="BotaoMaior" />
                    <input type="submit" name="botao" id="btnConsultar" value="Consultar" class="BotaoMaior" />
                    <input type="submit" name="botao" id="btnDesvincular" value="Desvincular" class="BotaoMaior" onclick="document.pressed=this.value"/></td>
                </tr>
                <tr>
                  <td align="right">&nbsp;</td>
                </tr>
              </table>
             
              <br /></td>
		  </tr>
			<tr>
			  <td valign="top">
              	<fieldset >
			      <legend>V&iacute;nculos Cadastrados</legend>
                  <?php 
				  		$lista = $bo->getListaVinculosCandidatoEspecialidadeArr();
						if(!empty($lista)){
				  ?>
			      <table class="paginador" width="98%">
							<tr>
							  <td width="55%" class="header">Cargo</td>
								<td width="10%" class="header"><div align="center">CRM</div></td>
								<td width="30%" class="header">Nome do Candidato</td>
								<td width="5%" class="header">Desv.</td>
							</tr>					    
							
							<?php
                            	foreach ($lista as $row) {
							?>
			                  <tr onMouseOver="this.className='selecionada'" onMouseOut="this.className='inicial'">
			                    <td class="body"><?php echo $row->getDesEspecialidade(); ?></td>
									<td class="body"><div align="right"><?php echo $row->getCRM(); ?></div></td>
									<td class="body"><?php echo $row->getNome(); ?></td>
									<td class="body"><div align="center">
									  <input name="excluir[]" type="checkbox" value="<?php echo $row->getIdCanditEspec();?>" />
								    </div></td>
					</tr>
                    		<?php } ?>
			    </table>
    <table>
                	<tr>
                    	<td>
					    <input type="submit" name="btnPrimeira" id="btnPrimeira" value="" onclick="document.pressed='Primeira'" class="PaginacaoPrimeira"/>&nbsp;
                        <input type="submit" name="btnAnterior" id="btnAnterior" value="" onclick="document.pressed='Anterior'" class="PaginacaoAnterior"/>&nbsp;
                        <td valign="middle">
                        <input type="hidden" id="paginaAtual" name="paginaAtual" value="<?php echo $bo->getPaginaAtual(); ?>"/><?php echo $bo->getPaginaAtual(); ?>  / <?php echo $bo->getNumPaginas(); ?> <input type="hidden" id="totalPaginas" name="totalPaginas" value="<?php echo $bo->getNumPaginas(); ?>"/> &nbsp;
                        </td>
                        <td>
                        <input type="submit" name="btnPosterior" id="btnPosterior" value="" onclick="document.pressed='Posterior'" class="PaginacaoPosterior"/>&nbsp;
                        <input type="submit" name="btnUltima" id="btnUltima" value="" onclick="document.pressed='Ultima';" class="PaginacaoUltima"/>
                  	<?php 
				  		} else {
							if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] != "Consultar"){
								echo "&nbsp;&nbsp;&nbsp;N&atilde;o foram encontrados registros neste cadastro.";
							} else {
								echo "&nbsp;&nbsp;&nbsp;N&atilde;o foram encontrados registros nesta consulta.";
							}
						}	
					?>
                    	</td>
                    </tr>
                </table>
					</fieldset>
</td>
		  </tr>
		</table>
	 </form>
        </td>
      </tr>
      <tr>
        <td colspan="2"><?php include_once('rodape.php');?></td>
      </tr>
</table>

</body>
</html>
