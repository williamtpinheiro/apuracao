<?php
require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/business/BaseApuracaoBO.php"); 
 
  $acao = isset($_REQUEST["action"]) ? $_REQUEST["action"] : ""; 
  $acao = (empty($acao) && !empty($_REQUEST) && isset($_REQUEST["botao"])) ? $_REQUEST["botao"] : $acao;
$bo = new BaseApuracaoBO($acao);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once('head.php');?>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
      <tr>
        <td colspan="2"><?php include_once('cabecalho.php');?></td>
      </tr>
      <tr>
        <td width="210" valign="top"><?php include_once('menuAdm.php');?></td>
        <td valign="top">
        <table width="99%" border="0" cellspacing="0" cellpadding="0">
	        <tr>
				<td height="10">&nbsp;</td>
			</tr>
			<tr>
				<td class="TituloSecao"><img src="../images/spacer.gif" width="15"									border="0" alt="">GERIR BASE APURAÇÃO
				</td>  
			</tr>
			<tr>
				<td height="10">&nbsp;</td>
			</tr>
            <tr>
				<td height="10"> 
				<?php 
					$msgSucessos = $bo->getMsgSucessosArr(); 
					if (!empty($msgSucessos)){
				?>
            <div class="MensagemSucesso">
            	<?php 
					foreach ($msgSucessos as $row) {
						echo $row;
					}
				?>
			</div>
            	<?php 
					} else { echo "&nbsp;"; }
					$msgErros = $bo->getMsgErrosArr();
					if(!empty($msgErros)){
				?>
            <div class="MensagemErro">
				<?php 
					foreach ($msgErros as $row) {
						echo $row;
					}
				?>
			</div>
            <?php 
					} else { echo "&nbsp;"; }
			?>
</td>
			</tr>
            <tr>
				<td height="10">&nbsp;</td>
			</tr>
			<tr>
			  <td valign="top">
              <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="form" name="form" enctype="multipart/form-data"  onsubmit="return OnSubmitFormGerirBaseApuracao();" >
              <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="20%" valign="top"><input type="submit" name="botao" id="btnGerarBase" value="Gerar Base" class="BotaoMaior" onclick="document.pressed=this.value;document.getElementById('aguarde').style.display='inline'" /></td>
                  <td width="80%"> Gerar a base de dados necessária para apurar a votação, serão gerados registros vinculando os Candidatos aos Cargos vinculados e aos Locais de votação.<br />
Para Iniciar a Apuração é necessário Gerar a Base.</td>
                </tr>
                <tr>
                  <td colspan="2">&nbsp;
<div align="center"><div align="center" id="aguarde" style="display: none;"> <img src="../images/ajax-loader.gif" width="31" height="31" /><br />  
Aguarde...</div></div></td>
                </tr>
                <tr>
                  <td><input type="submit" name="botao" id="btnExcluirBase" value="Excluir Base" class="BotaoMaior" onclick="document.pressed=this.value" /></td>
                  <td><span color="red">Cuidado!</span> Esta funcionalidade exclui toda a base de dados de Apuração.</td>
                </tr>
                <tr>
                  <td colspan="2" align="right">&nbsp;</td>
                </tr>
              </table>
              </form>
              <br /></td>
		  </tr>
			<tr>
			  <td valign="top">&nbsp;</td>
		  </tr>
		</table>

        </td>
      </tr>
      <tr>
        <td colspan="2"><?php include_once('rodape.php');?></td>
      </tr>
</table>

</body>
</html>
