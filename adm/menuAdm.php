<br />
<table border="0" align="left" cellpadding="1" cellspacing="1"
	valign="top" width="207">
	
	<tr>
		<td class="MenuTitulo">
			FUNCIONALIDADES</td>
  </tr>
	<tr>
		<td>
            <a href="/apuracao/adm/gerirLocaisVotacao.php" class="MenuLink2">
				&nbsp;Gerir Locais de Votação<img src="../images/spacer.gif" width="13" border="0" alt="">
			</a>
		</td>
	</tr>	
    <tr>
       <td>
        <a href="gerirGruposEspecialidades.php" class="MenuLink2">
			&nbsp;Gerir Cargos<img src="../images/spacer.gif" width="13" border="0" alt="">
	    </a>
       </td>
    </tr>
    <tr>
		<td>
			<a href="gerirCandidatos.php" class="MenuLink2">
			&nbsp;Gerir Candidatos<img src="../images/spacer.gif" width="13" border="0" alt="">
			</a>
		</td>
	</tr>

    <tr>
       <td>
        <a href="vincularCandidatoEspecialidade.php" class="MenuLink2">
			&nbsp;Vincular Candidatos a Cargos<img src="../images/spacer.gif" width="1" height="13" border="0" alt="">
	    </a>
       </td>
    </tr>
    <tr>
       <td>
        <a href="gerirBaseApuracao.php" class="MenuLink2">
			&nbsp;Gerar Base de Apuração<img src="../images/spacer.gif" width="13" border="0" alt="">
	    </a>
       </td>
    </tr>
    <tr>
       <td>
        <a href="gerirQtdeVotantesLocais.php" class="MenuLink2">
			&nbsp;Informar Qtde Votantes CPE<img src="../images/spacer.gif" width="13" border="0" alt="">
	    </a>
       </td>
    </tr>
    <tr>
       <td>
        <a href="gerirApuracao.php" class="MenuLink2">
			&nbsp;Gerir Apuração<img src="../images/spacer.gif" width="13" border="0" alt="">
	    </a>
       </td>
    </tr>            
<tr>
		<td class="MenuTitulo">
			RELATÓRIOS
		</td>
  </tr>
    <?php /* Relatorios utilizados na Eleição de Delegados para 23 especialidades.
    <tr>
        <td>
            <a href="relVotosGrupoLocal.php" class="MenuLink2" target="_blank">
            &nbsp;Total de Votos por Grupo / Local<img src="../images/spacer.gif" width="1" height="13" border="0" alt="">
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="relResultadoConsolidado.php" class="MenuLink2" target="_blank">
            &nbsp;Consolidado Final<img src="../images/spacer.gif" width="13"									border="0" alt="">
            </a>
        </td>
    </tr>
    <tr>
    */
    ?>
    <tr>
        <td>
            <a href="relResultadoConsolidadoLocais.php" class="MenuLink2" target="_blank">
            &nbsp;Consolidado Final por Locais<img src="../images/spacer.gif" width="13" border="0" alt="">
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="relResultadoTotaisVotos.php" class="MenuLink2" target="_blank">
            &nbsp;Total de Votos<img src="../images/spacer.gif" width="1" height="13" border="0" alt="">
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="relResultadoConsolidadoLocais2.php" class="MenuLink2" target="_blank">
            &nbsp;Consolidado Final por Locais CPE<img src="../images/spacer.gif" width="13" border="0" alt="">
            </a>
        </td>
    </tr>
	<tr>
		<td class="MenuTitulo">&nbsp; 
		 
		</td>
	</tr>
</table>
<br />