<?php 
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/library/phpjasperxml_0.8/class/fpdf/fpdf.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/library/phpjasperxml_0.8/class/PHPJasperXML.inc.php");
	include_once ($_SERVER['DOCUMENT_ROOT']."/apuracao/library/phpjasperxml_0.8/setting.php");
	include_once ($_SERVER['DOCUMENT_ROOT']."/apuracao/library/uteis.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/dao/BaseApuracaoDAO.php");
	
	
	/**
	* Arquivo PHP de acesso ao Relat�rio de Resultado Consolidado
	* @name relResultadoConsolidado.php
	* @version v 2.0 
	* @package com.algartecnologia
	* @access public
	*/	
	
	$daoBaseApuracao = new BaseApuracaoDAO();
	$existeApuracao = $daoBaseApuracao->verificarExistenciaApuracao();
	if($existeApuracao){
		$qtdLocaisVotacaoApuracao = $daoBaseApuracao->verificarExistenciaLocaisApuracao();
		
		if(count($qtdLocaisVotacaoApuracao) > 1){
			$listaLocaisArr = $daoBaseApuracao->verificarExistenciaLocaisApuracao();
			$nomesLocaisArr;
			$idLocaisArr;
			foreach($listaLocaisArr as $row){
				$nomeLocal = utf8_decode($row->getDesLocal());
				$idLocal = $row->getIdLocal();
				$nomesLocaisArr[] = $nomeLocal;
				$idLocaisArr[] = $idLocal;
			}
			
			$diretorio = getcwd();  
			$diretorio.= "/../reports/";
			$xml = simplexml_load_file($diretorio."resultadoTotaisVotos.jrxml");
			$PHPJasperXML = new PHPJasperXML();
			$PHPJasperXML->debugsql=false; 
			//Passo os parametros
			$PHPJasperXML->arrayParameter=array("TITULO"=>utf8_decode('ELEIÇÃO DO CONSELHO FISCAL DA UNIMED-BH'),"TITULO2"=>'',"SUBTITULO"=>'RESULTADO TOTAIS DE VOTOS',"DATA"=>date('d/m/Y'),"ID_CRM1"=>1,"ID_CRM2"=>2,"NOME_CRM1"=>utf8_decode('Experiência e Responsabilidade'),"NOME_CRM2"=>utf8_decode('Novos Caminhos, Novas Conquistas')); 
			$PHPJasperXML->xml_dismantle($xml);
			//$PHPJasperXML->connect($server,$user,$pass,$db);
			$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db);
			$PHPJasperXML->outpage("I");
		} else {
			echo "<script>alert('Para exibicao deste relatorio, a Apuracao deve conter 23 locais de votacao.');</script> <a href='javascript:window.close()'>Clique aqui para fechar esta Janela e retornar ao sistema.</a>";
		}
	} else {
		echo "<script>alert('Nao existe base de apuracao para exibicao deste relatorio.');</script> <a href='javascript:window.close()'>Clique aqui para fechar esta Janela e retornar ao sistema.</a>";
	}

?>