<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/dao/GruposEspecialidadesDAO.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/library/uteis.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/business/Constantes.php");

	/**
	* Classe de negocio de Grupo de Especialidades
	* @name 	GruposEspecialidadesBO
	* @version v 1.0 20/08/2011
	* @package com.algartecnologia.business
	* @access public
	*/
	Class GruposEspecialidadesBO {

		var $msgErrosArr;
		var $msgSucessosArr;
		var $numPaginas;
		var $paginaAtual;

		var $listaGruposEspecialidadesArr;
		var $listaGruposEspecialidades;

		var $grupoEspecialidade;

    	/**
		* M�todo Construtor da classe.
		* @name GruposEspecialidadesBO
		* @param $action Recebe a fun��o que dever� ser executada por padr�o recebe null.
		*/
		function GruposEspecialidadesBO ($action = null) {
			  $this->setGrupoEspecialidade(null);
			  $this->limparMsgGeral();
			  //echo $_REQUEST["botao"];
			  //echo "<BR>".$action;
			  if($action == "edit"){
			  		if(isset($_REQUEST["id"]) && empty($_REQUEST["botao"])){
				  		$this->setGrupoEspecialidade($this->carregarGrupoEspecialidade($_REQUEST["id"]));
						$this->carregarListaGruposEspecialidades();
					} 
					else if(isset($_REQUEST["id"]) && $_REQUEST["botao"] == "Alterar"){
						$this->alterar($_REQUEST["id"],$_REQUEST["desEspecialidade"],$_REQUEST["qtdVagas"]);
			  		} 
			  		else if(isset($_REQUEST["id"]) && $_REQUEST["botao"] == "Incluir"){
						$this->incluir($_REQUEST["desEspecialidade"],$_REQUEST["qtdVagas"]);
			  		} 
			  		else if(isset($_REQUEST["id"]) && $_REQUEST["botao"] == "Excluir"){
						$this->excluir($_REQUEST["id"],$_REQUEST["desEspecialidade"],$_REQUEST["qtdVagas"]);
					}
					else if(isset($_REQUEST["botao"]) &&  $_REQUEST["botao"] == "Consultar"){
						$this->consultar($_REQUEST["desEspecialidade"],$_REQUEST["qtdVagas"]);
			  		}
			  }else{
			  		if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Incluir"){
						$this->incluir($_REQUEST["desEspecialidade"],$_REQUEST["qtdVagas"]);
					} 
					else if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Excluir"){
						$this->excluir($_REQUEST["id"],$_REQUEST["desEspecialidade"],$_REQUEST["qtdVagas"]);
					} 
					else if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Consultar"){
						$this->consultar($_REQUEST["desEspecialidade"],$_REQUEST["qtdVagas"]);
					} else {
						$this->carregarListaGruposEspecialidades();
					}
					if($action == "Consultar"){
						if(isset($_REQUEST["btnUltima"])){
							$this->consultar($_REQUEST["desEspecialidade"],$_REQUEST["qtdVagas"],$_REQUEST["totalPaginas"]);
						}else if(isset($_REQUEST["btnPrimeira"])){
							$this->consultar($_REQUEST["desEspecialidade"],$_REQUEST["qtdVagas"]);
						}else if(isset($_REQUEST["btnPosterior"])){
							if($_REQUEST["paginaAtual"]<$_REQUEST["totalPaginas"]){
								$this->consultar($_REQUEST["desEspecialidade"],$_REQUEST["qtdVagas"],$_REQUEST["paginaAtual"]+1);
							}else{
								$this->consultar($_REQUEST["desEspecialidade"],$_REQUEST["qtdVagas"],$_REQUEST["paginaAtual"]);
							}
						}else if(isset($_REQUEST["btnAnterior"])){
							if($_REQUEST["paginaAtual"]==1){
								$this->consultar($_REQUEST["desEspecialidade"],$_REQUEST["qtdVagas"],$_REQUEST["paginaAtual"]);
							}else{
								$this->consultar($_REQUEST["desEspecialidade"],$_REQUEST["qtdVagas"],$_REQUEST["paginaAtual"]-1);
							}
						}
					} else {
						if(isset($_REQUEST["btnUltima"])){
							$this->carregarListaGruposEspecialidades($_REQUEST["totalPaginas"]);
						}else if(isset($_REQUEST["btnPrimeira"])){
							$this->carregarListaGruposEspecialidades();
						}else if(isset($_REQUEST["btnPosterior"])){
							if($_REQUEST["paginaAtual"]<$_REQUEST["totalPaginas"]){
								$this->carregarListaGruposEspecialidades($_REQUEST["paginaAtual"]+1);
							}else{
								$this->carregarListaGruposEspecialidades($_REQUEST["paginaAtual"]);
							}
						}else if(isset($_REQUEST["btnAnterior"])){
							if($_REQUEST["paginaAtual"]==1){
								$this->carregarListaGruposEspecialidades($_REQUEST["paginaAtual"]);
							}else{
								$this->carregarListaGruposEspecialidades($_REQUEST["paginaAtual"]-1);
							}
						}
					}
			  }
		}

		function getMsgErrosArr() {
			return $this->msgErrosArr;
		}

		function setMsgErrosArr($msgErrosArr) {
			$this->msgErrosArr = $msgErrosArr;
		}

		function adicionarMsgErro($msg) {
			if(empty($this->msgErrosArr)){
				$this->msgErrosArr[] = "&bull;&nbsp;".$msg;
			} else {
				$this->msgErrosArr[] = "<br />&bull;&nbsp;".$msg;
			}
		}

		function getMsgSucessosArr() {
			return $this->msgSucessosArr;
		}

		function setMsgSucessosArr($msgSucessosArr) {
			$this->msgSucessosArr = $msgSucessosArr;
		}

		function adicionarMsgSucesso($msg) {
			if(empty($this->msgSucessosArr)){
				$this->msgSucessosArr[] = "&bull;&nbsp;".$msg;
			} else {
				$this->msgSucessosArr[] = "<br />&bull;&nbsp;".$msg;
			}
		}

		function limparMsgGeral() {
			$this->msgSucessosArr = null;
			$this->msgErrosArr = null;
		}

		function getListaGruposEspecialidadesArr() {
			return $this->listaGruposEspecialidades;
		}

		function setListaGruposEspecialidadesArr($listaGruposEspecialidades) {
			$this->listaGruposEspecialidades = $listaGruposEspecialidades;
		}

		function getGrupoEspecialidade() {
			return $this->grupoEspecialidade;
		}

		function getNumPaginas() {
			return $this->numPaginas;
		}

		function setNumPaginas($numPaginas) {
			$this->numPaginas = $numPaginas;
		}

		function getPaginaAtual() {
			return $this->paginaAtual;
		}

		function setPaginaAtual($paginaAtual) {
			$this->paginaAtual = $paginaAtual;
		}

		function setGrupoEspecialidade($grupoEspecialidade) {
			$this->grupoEspecialidade = $grupoEspecialidade;
		}

		function carregarGrupoEspecialidade($idGrupoEspecialidade){
			$dao = new GruposEspecialidadesDAO();
			return $dao->consultarGrupoEspecialidadeByIdEspecialidade($idGrupoEspecialidade);
		}

		function carregarListaGruposEspecialidades($paginaAtual=1){
			$dao = new GruposEspecialidadesDAO();
			$this->setListaGruposEspecialidadesArr($this->paginar($dao->listarGruposEspecialidades(),$paginaAtual));
		}

		private function paginar($listaArr, $paginaAtual=1){
			$resultadoArr;
			$this->setPaginaAtual($paginaAtual);
			$numPaginas = round(count($listaArr)/NUM_REGISTROS_PAGINA);
			if((count($listaArr) % NUM_REGISTROS_PAGINA < 5) && (count($listaArr) % NUM_REGISTROS_PAGINA != 0)){
				$numPaginas++;
			}
			$this->setNumPaginas($numPaginas);
			$final = $paginaAtual*NUM_REGISTROS_PAGINA;
			$inicial = $final - NUM_REGISTROS_PAGINA;
			for($i=$inicial; $i < $final; $i++){
				if(isset($listaArr[$i])){
					$resultadoArr[] = $listaArr[$i];
				}
			}
			return $resultadoArr;
		}

		function alterar($idEspecialidade, $desEspecialidade, $qtdVagas) {
			$dao = new GruposEspecialidadesDAO();
			$desEspecialidade =  AntiSqlInjection($desEspecialidade);
			$qtdVagas =  AntiSqlInjection($qtdVagas);

			if(empty($idEspecialidade)){
				$this->adicionarMsgErro("Selecione na lista abaixo um Cargo para ser alterado.");
			} else if(!valNumero($idEspecialidade)){
				$this->adicionarMsgErro("Selecione na lista abaixo um Cargo para ser alterado.");
			}
			if(empty($desEspecialidade)){
				$this->adicionarMsgErro("Favor informar o Cargl.");
			}
			$numRegistros = count($dao->verificarExistenciaGrupoEspecialidade($desEspecialidade,$idEspecialidade));
			if($numRegistros > 0){
				$this->adicionarMsgErro("Cargo j&aacute; cadastrado.");
			}
			if(empty($qtdVagas)){
				$this->adicionarMsgErro("Favor informar a Qtde. de Vagas");
			} else if(!valNumero($qtdVagas)){
				$this->adicionarMsgErro("Quantidade de Vagas deve ser um n�mero entre 0 e 999.");
			} else if(($qtdVagas < 0) || ($qtdVagas > 999)){
				$this->adicionarMsgErro("Quantidade de Vagas deve ser um n�mero entre 0 e 999.");
			}

			if(empty($this->msgErrosArr)){
				$resultado = $dao->atualizarGrupoEspecialidade($idEspecialidade, $desEspecialidade, $qtdVagas);
				if($resultado){
					$this->adicionarMsgSucesso("Cargo alterado com sucesso!");
				} else {
					$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel atualizar o registro devido a um erro de comunica&ccedil;&atilde;o com o banco de dados.");
				}
			}else{
				$grupoEspecialidade = new GrupoEspecialidade();
				$grupoEspecialidade->setIdEspecialidade($idEspecialidade);
				$grupoEspecialidade->setDesEspecialidade($desEspecialidade);
				$grupoEspecialidade->setQtdVagas($qtdVagas);
				$this->setGrupoEspecialidade($grupoEspecialidade);
			}

			$this->carregarListaGruposEspecialidades();

		}

		function incluir($desEspecialidade, $qtdVagas) {
			$dao = new GruposEspecialidadesDAO();
			$desEspecialidade =  AntiSqlInjection($desEspecialidade);
			$qtdVagas =  AntiSqlInjection($qtdVagas);

			if(empty($desEspecialidade)){
				$this->adicionarMsgErro("Favor informar o Cargo.");
			}
			$numRegistros = count($dao->verificarExistenciaGrupoEspecialidade($desEspecialidade));
			if($numRegistros > 0){
				$this->adicionarMsgErro("Cargo j&aacute; cadastrado.");
			}
			if(empty($qtdVagas)){
				$this->adicionarMsgErro("Favor informar a Qtde. de Vagas");
			} else if(!valNumero($qtdVagas)){
				$this->adicionarMsgErro("Quantidade de Vagas deve ser um n&uacute;mero entre 0 e 999.");
			} else if(($qtdVagas < 0) || ($qtdVagas > 999)){
				$this->adicionarMsgErro("Quantidade de Vagas deve ser um n&uacute;mero entre 0 e 999.");
			}

			if(empty($this->msgErrosArr)){
				$resultado = $dao->incluirGrupoEspecialidade($desEspecialidade, $qtdVagas);
				if($resultado){
					$this->adicionarMsgSucesso("Cargo inclu&iacute;do  com sucesso!");
				} else {
					$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel incluir o registro devido a um erro de comunica&ccedil;&atilde;o com o banco de dados.");
				}
			}else{
				$grupoEspecialidade = new GrupoEspecialidade();
				$grupoEspecialidade->setDesEspecialidade($desEspecialidade);
				$grupoEspecialidade->setQtdVagas($qtdVagas);
				$this->setGrupoEspecialidade($grupoEspecialidade);
			}

			$this->carregarListaGruposEspecialidades();

		}

		function excluir($idEspecialidade, $desEspecialidade, $qtdVagas) {
			$dao = new GruposEspecialidadesDAO();

			if(empty($idEspecialidade)){
				$this->adicionarMsgErro("Selecione na lista abaixo um Cargo para ser exclu&iexcl;do.");
			}else if(!valNumero($idEspecialidade)){
				$this->adicionarMsgErro("Selecione na lista abaixo um Cargo para ser exclu&iexcl;do.");
			}

			$numRegistros = $dao->verificarExistenciaApuracao($idEspecialidade);
			if($numRegistros > 0){
				$this->adicionarMsgErro("Cargo n&atilde;o pode ser exclu&iacute;do,  pois est&aacute; vinculado a uma Apura&ccedil;&atilde;o!");
			}

			$numRegistrosCand = $dao->verificarExistenciaCandidato($idEspecialidade);
			if($numRegistrosCand > 0){
				$this->adicionarMsgErro("Cargo n&atilde;o pode ser exclu&iacute;do,  pois est&aacute; vinculado a $numRegistrosCand Candidato(s)!");
			}


			if(empty($this->msgErrosArr)){
				$resultado = $dao->excluirGrupoEspecialidade($idEspecialidade);
				if($resultado){
					$this->adicionarMsgSucesso("Cargo exclu&iacute;do  com sucesso!");
				} else {
					$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel excluir o registro devido a um erro de comunica&ccedil;&atilde;o com o banco de dados.");
				}
			}else{
				$grupoEspecialidade = new GrupoEspecialidade();
				$grupoEspecialidade->setIdEspecialidade($idEspecialidade);
				$grupoEspecialidade->setDesEspecialidade($desEspecialidade);
				$grupoEspecialidade->setQtdVagas($qtdVagas);
				$this->setGrupoEspecialidade($grupoEspecialidade);
			}

			$this->carregarListaGruposEspecialidades();

		}

		function consultar($desEspecialidade, $qtdVagas, $paginaAtual=1) {
			$dao = new GruposEspecialidadesDAO();
			$desEspecialidade =  AntiSqlInjection($desEspecialidade);
			$qtdVagas =  AntiSqlInjection($qtdVagas);

			if(empty($desEspecialidade) && empty($qtdVagas)){
				$this->adicionarMsgErro("Para consultar, informe o Cargo  e/ou a Qtde. de Vagas que deseja filtrar!");
				$this->carregarListaGruposEspecialidades();
			}
			if(empty($this->msgErrosArr)){
				if(!empty($desEspecialidade) && !empty($qtdVagas)){
					$resultado = $dao->listarGruposEspecialidades($desEspecialidade, $qtdVagas);
				} else if (!empty($desEspecialidade)){
					$resultado = $dao->listarGruposEspecialidades($desEspecialidade, null);
				} else if (!empty($qtdVagas)){
					$resultado = $dao->listarGruposEspecialidades(null, $qtdVagas);
				}

				$this->setListaGruposEspecialidadesArr($this->paginar($resultado,$paginaAtual));

			}

			$grupoEspecialidade = new GrupoEspecialidade();
			$grupoEspecialidade->setDesEspecialidade($desEspecialidade);
			$grupoEspecialidade->setQtdVagas($qtdVagas);
			$this->setGrupoEspecialidade($grupoEspecialidade);

		}
	}
?>
