<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/dao/LocaisVotacaoDAO.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/library/uteis.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/business/Constantes.php");

	/**
	* Classe de negocio de Locais de Vota��o
	* @name 	LocaisVotacaoBO
	* @version v 1.0
	* @package com.algartecnologia.business
	* @access public
	*/
	Class LocaisVotacaoBO {

		var $msgErrosArr;
		var $msgSucessosArr;
		var $numPaginas;
		var $paginaAtual;

		var $listaLocaisVotacaoArr;

		var $localVotacao;

    	/**
		* M�todo Construtor da classe.
		* @name LocaisVotacaoBO
		* @param $action Recebe a fun��o que dever� ser executada por padr�o recebe null.
		*/
		function LocaisVotacaoBO ($action = null) {
			  $this->setLocalVotacao(null);
			  $this->limparMsgGeral();
			  //echo $_REQUEST["botao"];
			  //echo "<BR>".$action;

			  if($action == "edit"){
			  		if(empty($_REQUEST["botao"]) && isset($_REQUEST["id"])){
				  		$this->setLocalVotacao($this->carregarLocalVotacao($_REQUEST["id"]));
						$this->carregarListaLocaisVotacao();
					} 
					else if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Alterar" ){
						$this->alterar($_REQUEST["id"],$_REQUEST["desLocal"],$_REQUEST["cidade"]);
			  		} 
			  		else if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Incluir"){
						$this->incluir($_REQUEST["desLocal"],$_REQUEST["cidade"]);
			  		} 
			  		else if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Excluir"){
						$this->excluir($_REQUEST["id"],$_REQUEST["desLocal"],$_REQUEST["cidade"]);
					}
					else if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Consultar"){
						$this->consultar($_REQUEST["desLocal"],$_REQUEST["cidade"]);
			  		}
					if(isset($_REQUEST["btnUltima"])){
						$this->carregarListaLocaisVotacao($_REQUEST["totalPaginas"]);
					}else if(isset($_REQUEST["btnPrimeira"])){
						$this->carregarListaLocaisVotacao();
					}else if(isset($_REQUEST["btnPosterior"])){
						if($_REQUEST["paginaAtual"]<$_REQUEST["totalPaginas"]){
							$this->carregarListaLocaisVotacao($_REQUEST["paginaAtual"]+1);
						}else{
							$this->carregarListaLocaisVotacao($_REQUEST["paginaAtual"]);
						}
					}else if(isset($_REQUEST["btnAnterior"])){
						if($_REQUEST["paginaAtual"]==1){
							$this->carregarListaLocaisVotacao($_REQUEST["paginaAtual"]);
						}else{
							$this->carregarListaLocaisVotacao($_REQUEST["paginaAtual"]-1);
						}
					}
			  }
			  else{
				  	if(!empty($_REQUEST)){
				  		if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Incluir"){
							$this->incluir($_REQUEST["desLocal"],$_REQUEST["cidade"]);
						} 
						else if(isset($_REQUEST["botao"]) &&  $_REQUEST["botao"] == "Excluir"){
							$this->excluir($_REQUEST["id"],$_REQUEST["desLocal"],$_REQUEST["cidade"]);
						} 
						else if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Consultar"){
							$this->consultar($_REQUEST["desLocal"],$_REQUEST["cidade"]);
						} else {
							$this->carregarListaLocaisVotacao();
						}
					}
					if($action == "Consultar"){
						if(isset($_REQUEST["btnUltima"])){
							$this->consultar($_REQUEST["desLocal"],$_REQUEST["cidade"],$_REQUEST["totalPaginas"]);
						}else if(isset($_REQUEST["btnPrimeira"])){
							$this->consultar($_REQUEST["desLocal"],$_REQUEST["cidade"]);
						}else if(isset($_REQUEST["btnPosterior"])){
							if($_REQUEST["paginaAtual"]<$_REQUEST["totalPaginas"]){
								$this->consultar($_REQUEST["desLocal"],$_REQUEST["cidade"],$_REQUEST["paginaAtual"]+1);
							}else{
								$this->consultar($_REQUEST["desLocal"],$_REQUEST["cidade"],$_REQUEST["paginaAtual"]);
							}
						}else if(isset($_REQUEST["btnAnterior"])){
							if($_REQUEST["paginaAtual"]==1){
								$this->consultar($_REQUEST["desLocal"],$_REQUEST["cidade"],$_REQUEST["paginaAtual"]);
							}else{
								$this->consultar($_REQUEST["desLocal"],$_REQUEST["cidade"],$_REQUEST["paginaAtual"]-1);
							}
						}
					} else {
						if(isset($_REQUEST["btnUltima"])){
							$this->carregarListaLocaisVotacao($_REQUEST["totalPaginas"]);
						}else if(isset($_REQUEST["btnPrimeira"])){
							$this->carregarListaLocaisVotacao();
						}else if(isset($_REQUEST["btnPosterior"])){
							if($_REQUEST["paginaAtual"]<$_REQUEST["totalPaginas"]){
								$this->carregarListaLocaisVotacao($_REQUEST["paginaAtual"]+1);
							}else{
								$this->carregarListaLocaisVotacao($_REQUEST["paginaAtual"]);
							}
						}else if(isset($_REQUEST["btnAnterior"])){
							if($_REQUEST["paginaAtual"]==1){
								$this->carregarListaLocaisVotacao($_REQUEST["paginaAtual"]);
							}else{
								$this->carregarListaLocaisVotacao($_REQUEST["paginaAtual"]-1);
							}
						}
					}
			  }
		}

		function getMsgErrosArr() {
			return $this->msgErrosArr;
		}

		function setMsgErrosArr($msgErrosArr) {
			$this->msgErrosArr = $msgErrosArr;
		}

		function adicionarMsgErro($msg) {
			if(empty($this->msgErrosArr)){
				$this->msgErrosArr[] = "&bull;&nbsp;".$msg;
			} else {
				$this->msgErrosArr[] = "<br />&bull;&nbsp;".$msg;
			}
		}

		function getMsgSucessosArr() {
			return $this->msgSucessosArr;
		}

		function setMsgSucessosArr($msgSucessosArr) {
			$this->msgSucessosArr = $msgSucessosArr;
		}

		function adicionarMsgSucesso($msg) {
			if(empty($this->msgSucessosArr)){
				$this->msgSucessosArr[] = "&bull;&nbsp;".$msg;
			} else {
				$this->msgSucessosArr[] = "<br />&bull;&nbsp;".$msg;
			}
		}

		function limparMsgGeral() {
			$this->msgSucessosArr = null;
			$this->msgErrosArr = null;
		}

		function getListaLocaisVotacaoArr() {
			return $this->listaLocaisVotacaoArr;
		}

		function setListaLocaisVotacaoArr($listaLocaisVotacao) {
			$this->listaLocaisVotacaoArr = $listaLocaisVotacao;
		}

		function getLocalVotacao() {
			return $this->localVotacao;
		}

		function setLocalVotacao($localVotacao) {
			$this->localVotacao = $localVotacao;
		}

		function getNumPaginas() {
			return $this->numPaginas;
		}

		function setNumPaginas($numPaginas) {
			$this->numPaginas = $numPaginas;
		}

		function getPaginaAtual() {
			return $this->paginaAtual;
		}

		function setPaginaAtual($paginaAtual) {
			$this->paginaAtual = $paginaAtual;
		}

		function carregarLocalVotacao($idLocalVotacao){
			$dao = new LocaisVotacaoDAO();
			return $dao->consultarLocalVotacaoByIdLocal($idLocalVotacao);
		}

		function carregarListaLocaisVotacao($paginaAtual=1){
			$dao = new LocaisVotacaoDAO();
			$this->setListaLocaisVotacaoArr($this->paginar($dao->listarLocaisVotacao(),$paginaAtual));
		}

		private function paginar($listaArr, $paginaAtual=1){
			$resultadoArr = array();
			$this->setPaginaAtual($paginaAtual);
			$numPaginas = round(count($listaArr)/NUM_REGISTROS_PAGINA);
			if((count($listaArr) % NUM_REGISTROS_PAGINA < 5) && (count($listaArr) % NUM_REGISTROS_PAGINA != 0)){
				$numPaginas++;
			}
			$this->setNumPaginas($numPaginas);
			$final = $paginaAtual*NUM_REGISTROS_PAGINA;
			$inicial = $final - NUM_REGISTROS_PAGINA;
			for($i=$inicial; $i < $final; $i++){
				if(isset($listaArr[$i])){
					$resultadoArr[] = $listaArr[$i];
				}
			}
			return $resultadoArr;
		}

		function alterar($idLocal, $desLocal, $cidade) {
			$dao = new LocaisVotacaoDAO();
			$desLocal =  AntiSqlInjection($desLocal);
			$cidade =  AntiSqlInjection($cidade);

			if(empty($idLocal)){
				$this->adicionarMsgErro("Selecione na lista abaixo um Local para ser alterado.");
			}else if(!valNumero($idLocal)){
				$this->adicionarMsgErro("Selecione na lista abaixo um Local para ser alterado.");
			}
			if(empty($desLocal)){
				$this->adicionarMsgErro("Favor informar o Local.");
			}
			$numRegistros = count($dao->verificarExistenciaLocalVotacao($desLocal,$idLocal));
			if($numRegistros > 0){
				$this->adicionarMsgErro("Local j&aacute; cadastrado.");
			}
			if($cidade == "SELECIONAR"){
				$this->adicionarMsgErro("Favor informar uma Cidade.");
			}

			if(empty($this->msgErrosArr)){
				$resultado = $dao->atualizarLocalVotacao($idLocal, $desLocal, $cidade);
				if($resultado){
					$this->adicionarMsgSucesso("Local de Vota&ccedil;&atilde;o alterado com sucesso!");
				} else {
					$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel atualizar o registro devido a um erro de comunica&ccedil;&atilde;o com o banco de dados.");
				}
			}else{
				$localVotacao = new LocalVotacao();
				$localVotacao->setIdLocal($idLocal);
				$localVotacao->setDesLocal($desLocal);
				$localVotacao->setCidade($cidade);
				$this->setLocalVotacao($localVotacao);
			}

			$this->carregarListaLocaisVotacao();

		}

		function incluir($desLocal, $cidade) {
			$dao = new LocaisVotacaoDAO();
			$desLocal =  AntiSqlInjection($desLocal);
			$cidade =  AntiSqlInjection($cidade);

			if(empty($desLocal)){
				$this->adicionarMsgErro("Favor informar o Local.");
			}
			$numRegistros = count($dao->verificarExistenciaLocalVotacao($desLocal));
			if($numRegistros > 0){
				$this->adicionarMsgErro("Local j&aacute; cadastrado.");
			}
			if($cidade == "SELECIONAR"){
				$this->adicionarMsgErro("Favor informar uma Cidade.");
			}

			if(empty($this->msgErrosArr)){
				$resultado = $dao->incluirLocalVotacao($desLocal, $cidade);
				if($resultado){
					$this->adicionarMsgSucesso("Local de Vota&ccedil;&atilde;o inclu&iacute;do  com sucesso!");
				} else {
					$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel incluir o registro devido a um erro de comunica&ccedil;&atilde;o com o banco de dados.");
				}
			}else{
				$localVotacao = new LocalVotacao();
				$localVotacao->setDesLocal($desLocal);
				$localVotacao->setCidade($cidade);
				$this->setLocalVotacao($localVotacao);
			}

			$this->carregarListaLocaisVotacao();

		}

		function excluir($idLocal, $desLocal, $cidade) {
			$dao = new LocaisVotacaoDAO();

			if(empty($idLocal)){
				$this->adicionarMsgErro("Selecione na lista abaixo um Local para ser exclu&iexcl;do.");
			}else if(!valNumero($idLocal)){
				$this->adicionarMsgErro("Selecione na lista abaixo um Local para ser exclu&iexcl;do.");
			}

			$numRegistros = $dao->verificarExistenciaApuracao($idLocal);
			if($numRegistros > 0){
				$this->adicionarMsgErro("Local n&atilde;o pode ser exclu&iacute;do,  pois est&aacute; vinculado a uma Apura&ccedil;&atilde;o!");
			}


			if(empty($this->msgErrosArr)){
				$resultado = $dao->excluirLocalVotacao($idLocal);
				if($resultado){
					$this->adicionarMsgSucesso("Local de Vota&ccedil;&atilde;o exclu&iacute;do  com sucesso!");
				} else {
					$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel excluir o registro devido a um erro de comunica&ccedil;&atilde;o com o banco de dados.");
				}
			}else{
				$localVotacao = new LocalVotacao();
				$localVotacao->setIdLocal($idLocal);
				$localVotacao->setDesLocal($desLocal);
				$localVotacao->setCidade($cidade);
				$this->setLocalVotacao($localVotacao);
			}

			$this->carregarListaLocaisVotacao();

		}

		function consultar($desLocal, $cidade, $paginaAtual=1) {
			$dao = new LocaisVotacaoDAO();
			$desLocal =  AntiSqlInjection($desLocal);
			$cidade =  AntiSqlInjection($cidade);

			if(empty($desLocal) && $cidade == "SELECIONAR"){
				$this->adicionarMsgErro("Para consultar, informe o Local  e/ou a Cidade que deseja filtrar!");
				$this->carregarListaLocaisVotacao();
			}

			if(empty($this->msgErrosArr)){
				if(!empty($desLocal) && $cidade != "SELECIONAR"){
					$resultado = $dao->listarLocaisVotacao($desLocal, $cidade);
				} else if (!empty($desLocal)){
					$resultado = $dao->listarLocaisVotacao($desLocal);
				} else {
					$resultado = $dao->listarLocaisVotacao(null, $cidade);
				}

				$this->setListaLocaisVotacaoArr($this->paginar($resultado,$paginaAtual));

			}

			$localVotacao = new LocalVotacao();
			$localVotacao->setDesLocal($desLocal);
			$localVotacao->setCidade($cidade);
			$this->setLocalVotacao($localVotacao);



		}
	}
?>
