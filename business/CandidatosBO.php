<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/dao/CandidatosDAO.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/model/vo/ResultadoItemArquivo.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/model/vo/ResultadoArquivo.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/library/uteis.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/business/Constantes.php");

	/**
	* Classe de negocio de Candidatos
	* @name 	CandidatosBO
	* @version v 1.0 20/08/2011
	* @package com.algartecnologia.business
	* @access public
	*/
	Class CandidatosBO {

		var $msgErrosArr;
		var $msgSucessosArr;
		var $numPaginas;
		var $paginaAtual;

		var $listaCandidatosArr;
		var $resultadoArquivo;

		var $candidato;

    	/**
		* M�todo Construtor da classe.
		* @name CandidatosBO
		* @param $action Recebe a fun��o que dever� ser executada por padr�o recebe null.
		*/
		function CandidatosBO ($action = null) {
			  $this->setCandidato(null);
			  $this->setResultadoArquivo(null);
			  $this->limparMsgGeral();
			  //echo $_REQUEST["botao"];
			  //echo "<BR>".$action;
			  if($action == "edit"){
			  		if(empty($_REQUEST["botao"])){
				  		$this->setCandidato($this->carregarCandidato($_REQUEST["crm"]));
						$this->carregarListaCandidatos();
					} else if($_REQUEST["botao"] == "Alterar"){
						$this->alterar($_REQUEST["crm"],$_REQUEST["nome"],$_REQUEST["datCooperacao"],$_REQUEST["datNascimento"]);
			  		} else if($_REQUEST["botao"] == "Incluir"){
						$this->incluir($_REQUEST["crm"],$_REQUEST["nome"],$_REQUEST["datCooperacao"],$_REQUEST["datNascimento"]);
			  		} else if($_REQUEST["botao"] == "Excluir"){
						$this->excluir($_REQUEST["crm"],$_REQUEST["nome"],$_REQUEST["datCooperacao"],$_REQUEST["datNascimento"]);
					}else if($_REQUEST["botao"] == "Consultar"){
						$this->consultar($_REQUEST["crm"],$_REQUEST["nome"],$_REQUEST["datCooperacao"],$_REQUEST["datNascimento"]);
			  		}
			  }else{
			  		if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Incluir"){
						$this->incluir($_REQUEST["crm"],$_REQUEST["nome"],$_REQUEST["datCooperacao"],$_REQUEST["datNascimento"]);
					} 
					else if(isset($_REQUEST["botao"]) &&  $_REQUEST["botao"] == "Excluir"){
						$this->excluir($_REQUEST["crm"],$_REQUEST["nome"],$_REQUEST["datCooperacao"],$_REQUEST["datNascimento"]);
					} 
					else if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Consultar"){
						$this->consultar($_REQUEST["crm"],$_REQUEST["nome"],$_REQUEST["datCooperacao"],$_REQUEST["datNascimento"]);
					} 
					else if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Enviar"){
						$this->processarArquivoXML($_FILES["arquivoXML"]);
					} else {
						$this->carregarListaCandidatos();
					}
					if($action == "Consultar"){
						if(isset($_REQUEST["btnUltima"])){
							$this->consultar($_REQUEST["crm"],$_REQUEST["nome"],$_REQUEST["datCooperacao"],$_REQUEST["datNascimento"],$_REQUEST["totalPaginas"]);
						}else if(isset($_REQUEST["btnPrimeira"])){
							$this->consultar($_REQUEST["crm"],$_REQUEST["nome"],$_REQUEST["datCooperacao"],$_REQUEST["datNascimento"]);
						}else if(isset($_REQUEST["btnPosterior"])){
							if($_REQUEST["paginaAtual"]<$_REQUEST["totalPaginas"]){
								$this->consultar($_REQUEST["crm"],$_REQUEST["nome"],$_REQUEST["datCooperacao"],$_REQUEST["datNascimento"],$_REQUEST["paginaAtual"]+1);
							}else{
								$this->consultar($_REQUEST["crm"],$_REQUEST["nome"],$_REQUEST["datCooperacao"],$_REQUEST["datNascimento"],$_REQUEST["paginaAtual"]);
							}
						}else if(isset($_REQUEST["btnAnterior"])){
							if($_REQUEST["paginaAtual"]==1){
								$this->consultar($_REQUEST["crm"],$_REQUEST["nome"],$_REQUEST["datCooperacao"],$_REQUEST["datNascimento"],$_REQUEST["paginaAtual"]);
							}else{
								$this->consultar($_REQUEST["crm"],$_REQUEST["nome"],$_REQUEST["datCooperacao"],$_REQUEST["datNascimento"],$_REQUEST["paginaAtual"]-1);
							}
						}
					} else {
						if(isset($_REQUEST["btnUltima"])){
							$this->carregarListaCandidatos($_REQUEST["totalPaginas"]);
						}else if(isset($_REQUEST["btnPrimeira"])){
							$this->carregarListaCandidatos();
						}else if(isset($_REQUEST["btnPosterior"])){
							if($_REQUEST["paginaAtual"]<$_REQUEST["totalPaginas"]){
								$this->carregarListaCandidatos($_REQUEST["paginaAtual"]+1);
							}else{
								$this->carregarListaCandidatos($_REQUEST["paginaAtual"]);
							}
						}else if(isset($_REQUEST["btnAnterior"])){
							if($_REQUEST["paginaAtual"]==1){
								$this->carregarListaCandidatos($_REQUEST["paginaAtual"]);
							}else{
								$this->carregarListaCandidatos($_REQUEST["paginaAtual"]-1);
							}
						}
					}
			  }
		}

		function getMsgErrosArr() {
			return $this->msgErrosArr;
		}

		function setMsgErrosArr($msgErrosArr) {
			$this->msgErrosArr = $msgErrosArr;
		}

		function adicionarMsgErro($msg) {
			if(empty($this->msgErrosArr)){
				$this->msgErrosArr[] = "&bull;&nbsp;".$msg;
			} else {
				$this->msgErrosArr[] = "<br />&bull;&nbsp;".$msg;
			}
		}

		function getMsgSucessosArr() {
			return $this->msgSucessosArr;
		}

		function setMsgSucessosArr($msgSucessosArr) {
			$this->msgSucessosArr = $msgSucessosArr;
		}

		function adicionarMsgSucesso($msg) {
			if(empty($this->msgSucessosArr)){
				$this->msgSucessosArr[] = "&bull;&nbsp;".$msg;
			} else {
				$this->msgSucessosArr[] = "<br />&bull;&nbsp;".$msg;
			}
		}

		function limparMsgGeral() {
			$this->msgSucessosArr = null;
			$this->msgErrosArr = null;
		}

		function getListaCandidatosArr() {
			return $this->listaCandidatos;
		}

		function setListaCandidatosArr($listaCandidatos) {
			$this->listaCandidatos = $listaCandidatos;
		}

		function getResultadoArquivo() {
			return $this->resultadoArquivo;
		}

		function setResultadoArquivo($resultadoArquivo) {
			$this->resultadoArquivo = $resultadoArquivo;
		}

		function getCandidato() {
			return $this->candidato;
		}

		function setCandidato($candidato) {
			$this->candidato = $candidato;
		}

		function getNumPaginas() {
			return $this->numPaginas;
		}

		function setNumPaginas($numPaginas) {
			$this->numPaginas = $numPaginas;
		}

		function getPaginaAtual() {
			return $this->paginaAtual;
		}

		function setPaginaAtual($paginaAtual) {
			$this->paginaAtual = $paginaAtual;
		}

		function carregarCandidato($idCandidato){
			$dao = new CandidatosDAO();
			return $dao->consultarCandidatoByCRM($idCandidato);
		}

		function carregarListaCandidatos($paginaAtual=1){
			$dao = new CandidatosDAO();
			$this->setListaCandidatosArr($this->paginar($dao->listarCandidatos(),$paginaAtual));
		}

		private function paginar($listaArr, $paginaAtual=1){
			$resultadoArr;
			$this->setPaginaAtual($paginaAtual);
			$numPaginas = round(count($listaArr)/NUM_REGISTROS_PAGINA);
			if((count($listaArr) % NUM_REGISTROS_PAGINA < 5) && (count($listaArr) % NUM_REGISTROS_PAGINA != 0)){
				$numPaginas++;
			}
			$this->setNumPaginas($numPaginas);
			$final = $paginaAtual*NUM_REGISTROS_PAGINA;
			$inicial = $final - NUM_REGISTROS_PAGINA;
			for($i=$inicial; $i < $final; $i++){
				if(isset($listaArr[$i])){
					$resultadoArr[] = $listaArr[$i];
				}
			}
			return $resultadoArr;
		}

		function alterar($crm, $nome, $datCooperacao, $datNascimento) {
			$dao = new CandidatosDAO();
			$crm =  AntiSqlInjection($crm);
			$nome =  AntiSqlInjection($nome);
			$datCooperacao =  AntiSqlInjection($datCooperacao);
			$datNascimento =  AntiSqlInjection($datNascimento);

			if(empty($crm)){
				$this->adicionarMsgErro("Selecione na lista abaixo um Candidato para ser alterado.");
			} else if(!valNumero($crm)){
				$this->adicionarMsgErro("Selecione na lista abaixo um Candidato para ser alterado.");
			}
			if(empty($nome)){
				$this->adicionarMsgErro("Favor informar o Nome do Candidato.");
			}
			//$numRegistros = count($dao->verificarExistenciaCandidato($crm));
			//if($numRegistros > 0){
			//	$this->adicionarMsgErro("Candidato j&aacute; cadastrado.");
			//}
			if(empty($datCooperacao)){
				$this->adicionarMsgErro("Favor informar a Data de Coopera&ccedil;&atilde;o.");
			} else if(!valData($datCooperacao)){
				$this->adicionarMsgErro("Data de Coopera&ccedil;&atilde;o deve ser uma data v&aacute;lida.");
			}
			if(empty($datNascimento)){
				$this->adicionarMsgErro("Favor informar a Data de Nascimento.");
			} else if(!valData($datNascimento)){
				$this->adicionarMsgErro("Data de Nascimento deve ser uma data v&aacute;lida.");
			}

			if(empty($this->msgErrosArr)){
				$resultado = $dao->atualizarCandidato($crm, $nome, $datCooperacao, $datNascimento);
				if($resultado){
					$this->adicionarMsgSucesso("Candidato alterado com sucesso!");
				} else {
					$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel atualizar o registro devido a um erro de comunica&ccedil;&atilde;o com o banco de dados.");
				}
			}else{
				$candidato = new Candidato();
				$candidato->setCRM($crm);
				$candidato->setNome($nome);
				$candidato->setDatCooperacao($datCooperacao);
				$candidato->setDatNascimento($datNascimento);
				$this->setCandidato($candidato);
			}

			$this->carregarListaCandidatos();

		}

		function incluir($crm, $nome, $datCooperacao, $datNascimento) {
			$dao = new CandidatosDAO();
			$crm =  AntiSqlInjection($crm);
			$nome =  AntiSqlInjection($nome);
			$datCooperacao =  AntiSqlInjection($datCooperacao);
			$datNascimento =  AntiSqlInjection($datNascimento);

			if(empty($crm)){
				$this->adicionarMsgErro("Favor informar o CRM do Candidato.");
			} else if(!valNumero($crm)){
				$this->adicionarMsgErro("O CRM dever&aacute; ter apenas n&uacute;meros e no m&aacute;ximo 6 d&iacute;gitos.");
			}
			if(empty($nome)){
				$this->adicionarMsgErro("Favor informar o Nome do Candidato.");
			}
			$numRegistros = count($dao->verificarExistenciaCandidato($crm));
			if($numRegistros > 0){
				$this->adicionarMsgErro("Candidato j&aacute; cadastrado.");
			}
			if(empty($datCooperacao)){
				$this->adicionarMsgErro("Favor informar a Data de Coopera&ccedil;&atilde;o.");
			} else if(!valData($datCooperacao)){
				$this->adicionarMsgErro("Data de Coopera&ccedil;&atilde;o deve ser uma data v&aacute;lida.");
			}
			if(empty($datNascimento)){
				$this->adicionarMsgErro("Favor informar a Data de Nascimento.");
			} else if(!valData($datNascimento)){
				$this->adicionarMsgErro("Data de Nascimento deve ser uma data v&aacute;lida.");
			}

			if(empty($this->msgErrosArr)){
				$resultado = $dao->incluirCandidato($crm, $nome, $datCooperacao, $datNascimento);
				if($resultado){
					$this->adicionarMsgSucesso("Candidato inclu&iacute;do  com sucesso!");
				} else {
					$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel incluir o registro devido a um erro de comunica&ccedil;&atilde;o com o banco de dados.");
				}
			}else{
				$candidato = new Candidato();
				$candidato->setCRM($crm);
				$candidato->setNome($nome);
				$candidato->setDatCooperacao($datCooperacao);
				$candidato->setDatNascimento($datNascimento);
				$this->setCandidato($candidato);
			}

			$this->carregarListaCandidatos();

		}

		function excluir($crm, $nome, $datCooperacao, $datNascimento) {
			$dao = new CandidatosDAO();

			if(empty($crm)){
				$this->adicionarMsgErro("Selecione na lista abaixo um Candidato para ser exclu&iexcl;do.");
			}else if(!valNumero($crm)){
				$this->adicionarMsgErro("Selecione na lista abaixo um Candidato para ser exclu&iexcl;do.");
			}

			$numRegistros = $dao->verificarExistenciaApuracao($crm);
			if($numRegistros > 0){
				$this->adicionarMsgErro("Candidato n&atilde;o pode ser exclu&iacute;do,  pois est&aacute; vinculado a uma Apura&ccedil;&atilde;o!");
			}

			$numRegistrosEspec = $dao->verificarExistenciaEspecialidade($crm);
			if($numRegistrosEspec > 0){
				$this->adicionarMsgErro("Candidato n&atilde;o pode ser exclu&iacute;do,  pois est&aacute; vinculado a $numRegistrosEspec Especialidade(s)!");
			}


			if(empty($this->msgErrosArr)){
				$resultado = $dao->excluirCandidato($crm);
				if($resultado){
					$this->adicionarMsgSucesso("Candidato exclu&iacute;do  com sucesso!");
				} else {
					$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel excluir o registro devido a um erro de comunica&ccedil;&atilde;o com o banco de dados.");
				}
			}else{
				$candidato = new Candidato();
				$candidato->setCRM($crm);
				$candidato->setNome($nome);
				$candidato->setDatCooperacao($datCooperacao);
				$candidato->setDatNascimento($datNascimento);
				$this->setCandidato($candidato);
			}

			$this->carregarListaCandidatos();

		}

		function consultar($crm, $nome, $datCooperacao, $datNascimento, $paginaAtual=1) {
			$dao = new CandidatosDAO();
			$crm =  AntiSqlInjection($crm);
			$nome =  AntiSqlInjection($nome);

			if(empty($crm) && empty($nome)){
				$this->adicionarMsgErro("Para consultar, informe o CRM do Candidato  e/ou o Nome do Candidato que deseja filtrar!");
				$this->carregarListaCandidatos();
			}

			if(empty($this->msgErrosArr)){
				if(!empty($crm) && !empty($nome)){
					$resultado = $dao->listarCandidatos($crm, $nome);
				} else if (!empty($crm)){
					$resultado = $dao->listarCandidatos($crm, null);
				} else if (!empty($nome)){
					$resultado = $dao->listarCandidatos(null, $nome);
				}

				if(!empty($datCooperacao)){
					$this->adicionarMsgErro("N&atilde;o foi considerado Data de Coopera&ccedil;&atilde;o nesta consulta!");
				}
				if(!empty($datNascimento)){
					$this->adicionarMsgErro("N&atilde;o foi considerado Data de Nascimento nesta consulta!");
				}

				$this->setListaCandidatosArr($this->paginar($resultado,$paginaAtual));

			}

			$candidato = new Candidato();
			$candidato->setCRM($crm);
			$candidato->setNome($nome);
			$candidato->setDatCooperacao($datCooperacao);
			$candidato->setDatNascimento($datNascimento);
			$this->setCandidato($candidato);

		}

		function lerArquivoXMLCandidatos($caminhoArquivo){
			$candidatosArr;
			$xml = simplexml_load_file($caminhoArquivo);
			foreach($xml->Row as $candidatoFrom){
				//print_r($candidatoFrom);
				$candidatoTo = new Candidato();
				$candidatoTo->setCRM(utf8_decode($candidatoFrom->Column1));
				$candidatoTo->setNome(utf8_decode($candidatoFrom->Column2));
				$candidatoTo->setDatCooperacao(utf8_decode($candidatoFrom->Column3));
				$candidatoTo->setDatNascimento(utf8_decode($candidatoFrom->Column4));
				//print_r($candidatoTo);
			  	$candidatosArr[] = $candidatoTo;
			}
			return $candidatosArr;
		}

		function processarArquivoXML($file){
			$dao = new CandidatosDAO();
			$resultadoArquivo = new ResultadoArquivo();
			$resultadoProcessamentoArr;
			$totalItensSucesso = 0;
			$totalItensErro = 0;

			$diretorio = getcwd();
			$diretorio.= "/xml";
			if($file["error"] == 4){
				$this->adicionarMsgErro("Voc&ecirc; deve selecionar um arquivo antes de enviar.");
			}else{
				if (verExtensao($file['name']) != "xml") {
					$this->adicionarMsgErro("O arquivo a ser processado dever&aacute; ter exten&ccedil;&atilde;o .xml");
				}
			}
			if(empty($this->msgErrosArr)){
				$resultado = upload($file,$diretorio);
				if($resultado){
					$candidatosArr = $this->lerArquivoXMLCandidatos($resultado);
					$numRegistros = count($candidatosArr);
					if($numRegistros > 0){
						for($i = 0; $i < $numRegistros; $i++){
							//Verifico se a uma tentativa de ataque.
							$crm =  AntiSqlInjection($candidatosArr[$i]->getCRM());
							$nome =  AntiSqlInjection($candidatosArr[$i]->getNome());
							$datCooperacao =  AntiSqlInjection($candidatosArr[$i]->getDatCooperacao());
							$datNascimento =  AntiSqlInjection($candidatosArr[$i]->getDatNascimento());
							//guardo quantas mensagem j� foram geradas antes deste item.
							$numRegistrosResultadoAntesItem = count($resultadoProcessamentoArr);
							//Fa�o a verifica��o das regras de neg�cio.
							if(empty($crm)){
								$resultadoItemArquivo = new ResultadoItemArquivo();
								$resultadoItemArquivo->setItem($i+1);
								$resultadoItemArquivo->setCodigo(2);
								$resultadoItemArquivo->setMsg("Favor informar o CRM do Candidato.");
								$resultadoProcessamentoArr[] = $resultadoItemArquivo;
							} else if(!valNumero($crm)){
								$resultadoItemArquivo = new ResultadoItemArquivo();
								$resultadoItemArquivo->setItem($i+1);
								$resultadoItemArquivo->setCodigo(2);
								$resultadoItemArquivo->setMsg("O CRM dever&aacute; ter apenas n&uacute;meros e no m&aacute;ximo 6 d&iacute;gitos.");
								$resultadoProcessamentoArr[] = $resultadoItemArquivo;
							} else if(strlen(trim($crm)) > 6){
								$resultadoItemArquivo = new ResultadoItemArquivo();
								$resultadoItemArquivo->setItem($i+1);
								$resultadoItemArquivo->setCodigo(2);
								$resultadoItemArquivo->setMsg("O CRM dever&aacute; ter no m&aacute;ximo 6 d&iacute;gitos.");
								$resultadoProcessamentoArr[] = $resultadoItemArquivo;
							} else {
								$numRegistrosCandExiste = count($dao->verificarExistenciaCandidato($crm));
								if($numRegistrosCandExiste > 0){
									$resultadoItemArquivo = new ResultadoItemArquivo();
									$resultadoItemArquivo->setItem($i+1);
									$resultadoItemArquivo->setCodigo(2);
									$resultadoItemArquivo->setMsg("Candidato j&aacute; cadastrado.");
									$resultadoProcessamentoArr[] = $resultadoItemArquivo;
								}
							}

							if(empty($nome)){
								$resultadoItemArquivo = new ResultadoItemArquivo();
								$resultadoItemArquivo->setItem($i+1);
								$resultadoItemArquivo->setCodigo(2);
								$resultadoItemArquivo->setMsg("Favor informar o Nome do Candidato.");
								$resultadoProcessamentoArr[] = $resultadoItemArquivo;
							}else if(strlen(trim($nome)) > 100){
								$resultadoItemArquivo = new ResultadoItemArquivo();
								$resultadoItemArquivo->setItem($i+1);
								$resultadoItemArquivo->setCodigo(2);
								$resultadoItemArquivo->setMsg("O Nome do Candidato dever&aacute; ter no m&aacute;ximo 100 caracteres inclu&iacute;ndo espa&ccedil;os.");
								$resultadoProcessamentoArr[] = $resultadoItemArquivo;
							}

							if(empty($datCooperacao)){
								$resultadoItemArquivo = new ResultadoItemArquivo();
								$resultadoItemArquivo->setItem($i+1);
								$resultadoItemArquivo->setCodigo(2);
								$resultadoItemArquivo->setMsg("Favor informar a Data de Coopera&ccedil;&atilde;o.");
								$resultadoProcessamentoArr[] = $resultadoItemArquivo;
							} else if(!valData($datCooperacao)){
								$resultadoItemArquivo = new ResultadoItemArquivo();
								$resultadoItemArquivo->setItem($i+1);
								$resultadoItemArquivo->setCodigo(2);
								$resultadoItemArquivo->setMsg("Data de Coopera&ccedil;&atilde;o deve ser uma data v&aacute;lida.");
								$resultadoProcessamentoArr[] = $resultadoItemArquivo;
							}
							if(empty($datNascimento)){
								$resultadoItemArquivo = new ResultadoItemArquivo();
								$resultadoItemArquivo->setItem($i+1);
								$resultadoItemArquivo->setCodigo(2);
								$resultadoItemArquivo->setMsg("Favor informar a Data de Nascimento.");
								$resultadoProcessamentoArr[] = $resultadoItemArquivo;
							} else if(!valData($datNascimento)){
								$resultadoItemArquivo = new ResultadoItemArquivo();
								$resultadoItemArquivo->setItem($i+1);
								$resultadoItemArquivo->setCodigo(2);
								$resultadoItemArquivo->setMsg("Data de Nascimento deve ser uma data v&aacute;lida.");
								$resultadoProcessamentoArr[] = $resultadoItemArquivo;
							}
							//Verifico se ocorreu algum erro na valida��o deste item
							if(count($resultadoProcessamentoArr) == $numRegistrosResultadoAntesItem){
								//N�o houve erro, continuo a importa��o.
								$resultado = $dao->incluirCandidato($crm, $nome, $datCooperacao, $datNascimento);
								if($resultado){
									$resultadoItemArquivo = new ResultadoItemArquivo();
									$resultadoItemArquivo->setItem($i+1);
									$resultadoItemArquivo->setCodigo(1);
									$resultadoItemArquivo->setMsg("Candidato inclu&iacute;do  com sucesso!");
									$resultadoProcessamentoArr[] = $resultadoItemArquivo;
									$totalItensSucesso++;
								} else {
									$resultadoItemArquivo = new ResultadoItemArquivo();
									$resultadoItemArquivo->setItem($i+1);
									$resultadoItemArquivo->setCodigo(2);
									$resultadoItemArquivo->setMsg("N&atilde;o foi poss&iacute;vel incluir o registro devido a um erro de comunica&ccedil;&atilde;o com o banco de dados.");
									$resultadoProcessamentoArr[] = $resultadoItemArquivo;
									$totalItensErro++;
								}
							}else{
								$totalItensErro++;
							}

							//Limpo as v�riaveis para o pr�ximo registro.
							$crm =  '';
							$nome = '';
							$datCooperacao = '';
							$datNascimento = '';
						}
						//print_r($resultadoProcessamentoArr);
						$resultadoArquivo->setListaRegistrosArr($resultadoProcessamentoArr);

						$this->adicionarMsgSucesso("Arquivo processado com sucesso! Veja o resultado do processamento mais abaixo nesta p&aacute;gina.");
					} else {
						$this->adicionarMsgErro("O arquivo estava sem registros.");
					}
				}else{
					$this->adicionarMsgErro("Ocorreu um erro na transfer&ecirc;ncia do arquivo.");
				}
			}
			$resultadoArquivo->setTotalRegistrosErro($totalItensErro);
			$resultadoArquivo->setTotalRegistrosIncluido($totalItensSucesso);
			$this->setResultadoArquivo($resultadoArquivo);
		}


	}
?>
