<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/dao/BaseApuracaoDAO.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/library/uteis.php");

	/**
	* Classe de negocio de GerirBaseApuracao
	* @name 	BaseApuracaoBO
	* @version v 1.0
	* @package com.algartecnologia.business
	* @access public
	*/
	Class BaseApuracaoBO {

		var $msgErrosArr;
		var $msgSucessosArr;

    	/**
		* M�todo Construtor da classe.
		* @name BaseApuracaoBO
		* @param $action Recebe a fun��o que dever� ser executada por padr�o recebe null.
		*/
		function BaseApuracaoBO ($action = null) {
			$this->limparMsgGeral();
			if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Gerar Base"){
				$this->gerarBase();
			} 
			else if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Excluir Base"){
				$this->excluirBase();
			}

		}

		function getMsgErrosArr() {
			return $this->msgErrosArr;
		}

		function setMsgErrosArr($msgErrosArr) {
			$this->msgErrosArr = $msgErrosArr;
		}

		function adicionarMsgErro($msg) {
			if(empty($this->msgErrosArr)){
				$this->msgErrosArr[] = "&bull;&nbsp;".$msg;
			} else {
				$this->msgErrosArr[] = "<br />&bull;&nbsp;".$msg;
			}
		}

		function getMsgSucessosArr() {
			return $this->msgSucessosArr;
		}

		function setMsgSucessosArr($msgSucessosArr) {
			$this->msgSucessosArr = $msgSucessosArr;
		}

		function adicionarMsgSucesso($msg) {
			if(empty($this->msgSucessosArr)){
				$this->msgSucessosArr[] = "&bull;&nbsp;".$msg;
			} else {
				$this->msgSucessosArr[] = "<br />&bull;&nbsp;".$msg;
			}
		}

		function limparMsgGeral() {
			$this->msgSucessosArr = null;
			$this->msgErrosArr = null;
		}


		function gerarBase() {
			$dao = new BaseApuracaoDAO();

			$existeApuracao = $dao->verificarExistenciaApuracao();
			if($existeApuracao){
				$this->adicionarMsgErro("BASE DE DADOS PARA APURA&Ccedil;&Atilde;O J&Aacute; FOI GERADA!");
			}

			if(empty($this->msgErrosArr)){
				$resultado = $dao->gerarBaseApuracao();
				if(!$resultado){
					$this->adicionarMsgSucesso("Base de Apura&ccedil;&atilde;o gerada com sucesso!");
				} else {
					$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel gerar a base de apura&ccedil;&atilde;o devido a um erro de comunica&ccedil;&atilde;o com o banco de dados.");
				}
			}

		}

		function excluirBase() {
			$dao = new BaseApuracaoDAO();

			$existeApuracao = $dao->verificarExistenciaApuracao();
			if(!$existeApuracao){
				$this->adicionarMsgErro("N&Atilde;O EXISTE BASE DE DADOS DE APURA&Ccedil;&Atilde;O PARA SER EXCLU&Iacute;DA!");
			}

			if(empty($this->msgErrosArr)){
				$resultado = $dao->excluirBaseApuracao();
				if(!$resultado){
					$this->adicionarMsgSucesso("Processo finalizado!");
				} else {
					$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel excluir a base de apura&ccedil;&atilde;o devido a um erro de comunica&ccedil;&atilde;o com o banco de dados.");
				}
			}
		}

	}
?>
