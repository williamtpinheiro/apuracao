<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/dao/LocaisVotacaoDAO.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/library/uteis.php");

	/**
	* Classe de negocio de Controlar a quantidade de Votantes por Locais de Vota��o
	* @name QtdeVotantesLocaisBO
	* @version v 1.0
	* @package com.algartecnologia.business
	* @access public
	*/
	Class QtdeVotantesLocaisBO {

		var $msgErrosArr;
		var $msgSucessosArr;

		var $listaLocaisVotacaoArr;

    	/**
		* M�todo Construtor da classe.
		* @name QtdeVotantesLocaisBO
		* @param $action Recebe a fun��o que dever� ser executada por padr�o recebe null.
		*/
		function QtdeVotantesLocaisBO ($action = null) {
			$this->limparMsgGeral();
			//echo $_REQUEST["botao"];
			//echo "<BR>".$action;
			if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Salvar"){
				$this->salvar($_REQUEST["idLocaisVotacao"],$_REQUEST["qtdPresentes"]);
			} else {
				$this->carregarListaLocaisVotacao();
			}

		}

		function getMsgErrosArr() {
			return $this->msgErrosArr;
		}

		function setMsgErrosArr($msgErrosArr) {
			$this->msgErrosArr = $msgErrosArr;
		}

		function adicionarMsgErro($msg) {
			if(empty($this->msgErrosArr)){
				$this->msgErrosArr[] = "&bull;&nbsp;".$msg;
			} else {
				$this->msgErrosArr[] = "<br />&bull;&nbsp;".$msg;
			}
		}

		function getMsgSucessosArr() {
			return $this->msgSucessosArr;
		}

		function setMsgSucessosArr($msgSucessosArr) {
			$this->msgSucessosArr = $msgSucessosArr;
		}

		function adicionarMsgSucesso($msg) {
			if(empty($this->msgSucessosArr)){
				$this->msgSucessosArr[] = "&bull;&nbsp;".$msg;
			} else {
				$this->msgSucessosArr[] = "<br />&bull;&nbsp;".$msg;
			}
		}

		function limparMsgGeral() {
			$this->msgSucessosArr = null;
			$this->msgErrosArr = null;
		}

		function getListaLocaisVotacaoArr() {
			return $this->listaLocaisVotacaoArr;
		}

		function setListaLocaisVotacaoArr($listaLocaisVotacao) {
			$this->listaLocaisVotacaoArr = $listaLocaisVotacao;
		}

		function carregarListaLocaisVotacao(){
			$dao = new LocaisVotacaoDAO();
			$this->setListaLocaisVotacaoArr($dao->listarLocaisVotacao());
		}

		function salvar($listaIdLocaisVotacaoArr, $listaQtdPresentesArr) {
			$dao = new LocaisVotacaoDAO();
			if(empty($listaIdLocaisVotacaoArr)){
				$this->adicionarMsgErro("Ocorreu um erro ao salvar. N&atilde;o foi poss&iacute;vel recuperar a lista de locais de vota&ccedil;&atilde;o enviada para salvar.");
			}
			if(empty($listaQtdPresentesArr)){
				$this->adicionarMsgErro("Ocorreu um erro ao salvar. N&atilde;o foi poss&iacute;vel recuperar a lista de qtde. de presentes nos locais de vota&ccedil;&atilde;o enviada para salvar.");
			}
			$numRegistros = count($listaIdLocaisVotacaoArr);
			if(empty($this->msgErrosArr)){
				for($i = 0; $i < $numRegistros; $i++){
					$idLocal =  AntiSqlInjection($listaIdLocaisVotacaoArr[$i]);
					$qtdPresentes =  AntiSqlInjection($listaQtdPresentesArr[$i]);
					$local = $dao->consultarLocalVotacaoByIdLocal($idLocal);
					$resultado = $dao->atualizarQtdPresentes($idLocal, $qtdPresentes);
					if($resultado){
						$this->adicionarMsgSucesso("Local de Vota&ccedil;&atilde;o ".strtoupper($local->getDesLocal())." foi atualizado com sucesso!");
					} else {
						$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel atualizar o local de vota&ccedil;&atilde;o ".strtoupper($local->getDesLocal())." devido a um erro de comunica&ccedil;&atilde;o com o banco de dados.");
					}
				}
			}

			$this->carregarListaLocaisVotacao();

		}
	}
?>
