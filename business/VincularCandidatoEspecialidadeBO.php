<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/dao/GruposEspecialidadesDAO.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/dao/CandidatosEspecialidadesDAO.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/library/uteis.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/business/Constantes.php");

	/**
	* Classe de negocio de Vincular Candidato ao Grupo de Especialidade no qual ser� candidato
	* @name VincularCandidatoEspecialidadeBO
	* @version v 1.0
	* @package com.algartecnologia.business
	* @access public
	*/
	Class VincularCandidatoEspecialidadeBO {

		var $msgErrosArr;
		var $msgSucessosArr;
		var $numPaginas;
		var $paginaAtual;

		var $listaVinculosCandidatoEspecialidadeArr;
		var $listaGruposEspecialidadesArr;

		var $grupoEspecialidadeAtual;

		var $candidatoEspecialidade;

    	/**
		* M�todo Construtor da classe.
		* @name VincularCandidatoEspecialidadeBO
		* @param $action Recebe a fun��o que dever� ser executada por padr�o recebe null.
		*/
		function VincularCandidatoEspecialidadeBO ($action = null) {
			  $this->setCandidatoEspecialidade(null);
			  $this->limparMsgGeral();

			  $this->carregarListaGruposEspecialidades();

			  if(isset($_REQUEST["idEspecialidade"] )){
			  	$this->setGrupoEspecialidadeAtual($_REQUEST["idEspecialidade"]);
			  }
			  

			  if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Vincular"){
					$this->vincular($_REQUEST["idEspecialidade"],$_REQUEST["crm"]);
			  } 
			  else if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Desvincular"){
					$this->desvincular($_REQUEST["excluir"]);
			  } 
			  else if(isset($_REQUEST["botao"]) && $_REQUEST["botao"] == "Consultar"){
					$this->consultar($_REQUEST["idEspecialidade"],$_REQUEST["crm"]);
			  } 
			  else {
					$this->carregarListaVinculosCandidatoEspecialidade();
			  }
			  if($action == "Consultar"){
						if(isset($_REQUEST["btnUltima"])){
							$this->consultar($_REQUEST["idEspecialidade"],$_REQUEST["crm"],$_REQUEST["totalPaginas"]);
						}else if(isset($_REQUEST["btnPrimeira"])){
							$this->consultar($_REQUEST["idEspecialidade"],$_REQUEST["crm"]);
						}else if(isset($_REQUEST["btnPosterior"])){
							if($_REQUEST["paginaAtual"]<$_REQUEST["totalPaginas"]){
								$this->consultar($_REQUEST["idEspecialidade"],$_REQUEST["crm"],$_REQUEST["paginaAtual"]+1);
							}else{
								$this->consultar($_REQUEST["idEspecialidade"],$_REQUEST["crm"],$_REQUEST["paginaAtual"]);
							}
						}else if(isset($_REQUEST["btnAnterior"])){
							if($_REQUEST["paginaAtual"]==1){
								$this->consultar($_REQUEST["idEspecialidade"],$_REQUEST["crm"],$_REQUEST["paginaAtual"]);
							}else{
								$this->consultar($_REQUEST["idEspecialidade"],$_REQUEST["crm"],$_REQUEST["paginaAtual"]-1);
							}
						}
					} else {
						if(isset($_REQUEST["btnUltima"])){
							$this->carregarListaVinculosCandidatoEspecialidade($_REQUEST["totalPaginas"]);
						}else if(isset($_REQUEST["btnPrimeira"])){
							$this->carregarListaVinculosCandidatoEspecialidade();
						}else if(isset($_REQUEST["btnPosterior"])){
							if($_REQUEST["paginaAtual"]<$_REQUEST["totalPaginas"]){
								$this->carregarListaVinculosCandidatoEspecialidade($_REQUEST["paginaAtual"]+1);
							}else{
								$this->carregarListaVinculosCandidatoEspecialidade($_REQUEST["paginaAtual"]);
							}
						}else if(isset($_REQUEST["btnAnterior"])){
							if($_REQUEST["paginaAtual"]==1){
								$this->carregarListaVinculosCandidatoEspecialidade($_REQUEST["paginaAtual"]);
							}else{
								$this->carregarListaVinculosCandidatoEspecialidade($_REQUEST["paginaAtual"]-1);
							}
						}
					}

		}

		function getMsgErrosArr() {
			return $this->msgErrosArr;
		}

		function setMsgErrosArr($msgErrosArr) {
			$this->msgErrosArr = $msgErrosArr;
		}

		function adicionarMsgErro($msg) {
			if(empty($this->msgErrosArr)){
				$this->msgErrosArr[] = "&bull;&nbsp;".$msg;
			} else {
				$this->msgErrosArr[] = "<br />&bull;&nbsp;".$msg;
			}
		}

		function getMsgSucessosArr() {
			return $this->msgSucessosArr;
		}

		function setMsgSucessosArr($msgSucessosArr) {
			$this->msgSucessosArr = $msgSucessosArr;
		}

		function adicionarMsgSucesso($msg) {
			if(empty($this->msgSucessosArr)){
				$this->msgSucessosArr[] = "&bull;&nbsp;".$msg;
			} else {
				$this->msgSucessosArr[] = "<br />&bull;&nbsp;".$msg;
			}
		}

		function limparMsgGeral() {
			$this->msgSucessosArr = null;
			$this->msgErrosArr = null;
		}

		function getListaVinculosCandidatoEspecialidadeArr() {
			return $this->listaVinculosCandidatoEspecialidadeArr;
		}

		function setListaVinculosCandidatoEspecialidadeArr($listaVinculosCandidatoEspecialidadeArr) {
			$this->listaVinculosCandidatoEspecialidadeArr = $listaVinculosCandidatoEspecialidadeArr;
		}

		function getListaGruposEspecialidadesArr() {
			return $this->listaGruposEspecialidadesArr;
		}

		function setListaGruposEspecialidadesArr($listaGruposEspecialidadesArr) {
			$this->listaGruposEspecialidadesArr = $listaGruposEspecialidadesArr;
		}

		function getCandidatoEspecialidade() {
			return $this->candidatoEspecialidade;
		}

		function setCandidatoEspecialidade($candidatoEspecialidade) {
			$this->candidatoEspecialidade = $candidatoEspecialidade;
		}

		function getGrupoEspecialidadeAtual() {
			return $this->grupoEspecialidadeAtual;
		}

		function setGrupoEspecialidadeAtual($grupoEspecialidadeAtual) {
			$this->grupoEspecialidadeAtual = $grupoEspecialidadeAtual;
		}

		function getNumPaginas() {
			return $this->numPaginas;
		}

		function setNumPaginas($numPaginas) {
			$this->numPaginas = $numPaginas;
		}

		function getPaginaAtual() {
			return $this->paginaAtual;
		}

		function setPaginaAtual($paginaAtual) {
			$this->paginaAtual = $paginaAtual;
		}

		function carregarListaVinculosCandidatoEspecialidade($paginaAtual=1){
			$dao = new CandidatosEspecialidadesDAO();
			$this->setListaVinculosCandidatoEspecialidadeArr($this->paginar($dao->listarCandidatosEspecilidades(),$paginaAtual));
		}

		function carregarListaGruposEspecialidades(){
			$dao = new GruposEspecialidadesDAO();
			$this->setListaGruposEspecialidadesArr($dao->listarGruposEspecialidades());
		}

		private function paginar($listaArr, $paginaAtual=1){
			$resultadoArr = array();
			$this->setPaginaAtual($paginaAtual);
			$numPaginas = round(count($listaArr)/NUM_REGISTROS_PAGINA);
			if((count($listaArr) % NUM_REGISTROS_PAGINA < 5) && (count($listaArr) % NUM_REGISTROS_PAGINA != 0)){
				$numPaginas++;
			}
			$this->setNumPaginas($numPaginas);
			$final = $paginaAtual*NUM_REGISTROS_PAGINA;
			$inicial = $final - NUM_REGISTROS_PAGINA;
			for($i=$inicial; $i < $final; $i++){
				if(isset($listaArr[$i])){
					$resultadoArr[] = $listaArr[$i];
				}
			}
			return $resultadoArr;
		}

		function vincular($idEspecialidade, $crm) {
			$dao = new CandidatosEspecialidadesDAO();
			$idEspecialidade =  AntiSqlInjection($idEspecialidade);
			$crm =  AntiSqlInjection($crm);

			if($idEspecialidade == "SELECIONAR"){
				$this->adicionarMsgErro("Favor informar um Cargo.");
			}
			if(empty($crm)){
				$this->adicionarMsgErro("Favor informar o CRM do Candidato.");
			} else if(!valNumero($crm)){
				$this->adicionarMsgErro("O CRM dever&aacute; ter apenas n&uacute;meros e no m&aacute;ximo 6 d&iacute;gitos.");
			} else if(strlen(trim($crm)) > 6){
				$this->adicionarMsgErro("O CRM dever&aacute; ter no m&aacute;ximo 6 d&iacute;gitos.");
			}

			if(empty($this->msgErrosArr)){

				$numRegistrosCandEspec = count($dao->verificarExistenciaCandidatoEspecialidade($crm, $idEspecialidade));
				if($numRegistrosCandEspec > 0){
					$this->adicionarMsgErro("Candidato j&aacute; vinculado a este Cargo.");
				}
				if(empty($this->msgErrosArr)){
					$resultado = $dao->incluirCandidatoEspecialidade($crm, $idEspecialidade);
					if($resultado){
						$this->adicionarMsgSucesso("Candidato vinculado ao Cargo com sucesso!");
					} else {
						$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel vincular o registro devido a um erro de comunica&ccedil;&atilde;o com o banco de dados.");
					}
				}else{
					$candidatoEspecialidade = new CandidatoEspecialidade();
					$candidatoEspecialidade->setIdEspecialidade($idEspecialidade);
					$candidatoEspecialidade->setCRM($crm);
					$this->setCandidatoEspecialidade($candidatoEspecialidade);
				}
			}else{
				$candidatoEspecialidade = new CandidatoEspecialidade();
				$candidatoEspecialidade->setIdEspecialidade($idEspecialidade);
				$candidatoEspecialidade->setCRM($crm);
				$this->setCandidatoEspecialidade($candidatoEspecialidade);
			}

			$this->carregarListaVinculosCandidatoEspecialidade();

		}

		function desvincular($listaIdExclusaoArr) {
			$dao = new CandidatosEspecialidadesDAO();
			if(empty($listaIdExclusaoArr)){
				$this->adicionarMsgErro("Selecione na lista abaixo um registro a ser desvinculado.");
			}
			$numRegistros = $dao->verificarExistenciaApuracao();
			if($numRegistros > 0){
				$this->adicionarMsgErro("N&atilde;o &eacute; poss&iacute;vel desvincular Candidatos, pois a Apura&ccedil;&atilde;o j&aacute; foi iniciada.");
			}

			if(empty($this->msgErrosArr)){
				foreach($listaIdExclusaoArr as $idCanditEspec){
					$canditEspec = $dao->consultarCanditatoEspecialidadeByIdCanditEspec($idCanditEspec);
					$resultado = $dao->excluirCandidatoEspecialidade($idCanditEspec);
					if($resultado){
						$this->adicionarMsgSucesso("O candidato de CRM ".$canditEspec->getCRM()." do cargo ".strtoupper($canditEspec->getDesEspecialidade())." foi desvinculado com sucesso!");
					} else {
						$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel desvincular o candidato de CRM ".$canditEspec->getCRM()." do cargo ".strtoupper($canditEspec->getDesEspecialidade())." devido a um erro de comunica&ccedil;&atilde;o com o banco de dados.");
					}
				}
			}

			$this->carregarListaVinculosCandidatoEspecialidade();

		}

		function consultar($idEspecialidade, $crm, $paginaAtual=1) {
			$dao = new CandidatosEspecialidadesDAO();
			$idEspecialidade =  AntiSqlInjection($idEspecialidade);
			$crm =  AntiSqlInjection($crm);

			if(empty($crm) && $idEspecialidade == "SELECIONAR"){
				$this->adicionarMsgErro("Para consultar, informe o CRM e/ou Cargo que deseja filtrar!");
				$this->carregarListaVinculosCandidatoEspecialidade();
			}

			if(empty($this->msgErrosArr)){
				if(!empty($crm) && $idEspecialidade != "SELECIONAR"){
					$resultado = $dao->listarCandidatosEspecilidades($crm, $idEspecialidade);
				} else if (!empty($crm)){
					$resultado = $dao->listarCandidatosEspecilidades($crm);
				} else {
					$resultado = $dao->listarCandidatosEspecilidades(null, $idEspecialidade);
				}

				$this->setListaVinculosCandidatoEspecialidadeArr($this->paginar($resultado,$paginaAtual));

			}

			$candidatoEspecialidade = new CandidatoEspecialidade();
			$candidatoEspecialidade->setCRM($crm);
			$candidatoEspecialidade->setIdEspecialidade($idEspecialidade);
			$this->setCandidatoEspecialidade($candidatoEspecialidade);

		}
	}
?>
