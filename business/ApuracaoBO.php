<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/dao/BaseApuracaoDAO.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/dao/LocaisVotacaoDAO.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/dao/GruposEspecialidadesDAO.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/apuracao/library/uteis.php");

	/**
	* Classe de negocio de Apura��o
	* @name 	ApuracaoBO
	* @version v 1.2 24/03/2014
	* @package com.algartecnologia.business
	* @access public
	*/
	Class ApuracaoBO {

		var $msgErrosArr;
		var $msgSucessosArr;

		var $listaCandidatosArr;
		var $listaLocaisVotacao;
		var $listaGruposEspecialidades;

		var $apuracao;
		var $acompanhaOnline;

    	/**
		* M�todo Construtor da classe.
		* @name ApuracaoBO
		* @param $action Recebe a fun��o que dever� ser executada por padr�o recebe null.
		*/
		function ApuracaoBO ($action = null) {
			$this->setApuracao(null);
			$this->limparMsgGeral();
			$this->carregarListaLocaisVatacao();
			$this->carregarListaGruposEspecialidades();
			  
		 	if($_REQUEST){
				if(empty($_REQUEST["botao"])){
			  		$apuracao = new Apuracao();
					$apuracao->setIdLocal($_REQUEST["idLocal"]);
					$apuracao->setIdEspecialidade($_REQUEST["idEspecialidade"]);
					$this->setApuracao($apuracao);
					if($action  == "AcompanhaOnline"){
						$this->carregarAcompanhaOnline();
			  		}
			  	}  
			  	else if($_REQUEST["botao"] == "Salvar"){
					$this->salvar($_REQUEST["idApuracaoLoc"],$_REQUEST["idLocal"],$_REQUEST["idEspecialidade"],$_REQUEST["numVotosNulos"],$_REQUEST["numVotosBrancos"],$_REQUEST["idApuracaoCadid"],$_REQUEST["qtdVotosCandid"],$_REQUEST["qtdCedulasEnviadas"],$_REQUEST["qtdCedulasDevolvidas"],$_REQUEST["qtdCedulasInvalidadas"],$_REQUEST["numMesaApuracao"],$_REQUEST["qtdVotantesSessao"],$_REQUEST["bitStatus"]);
			  	} 
			  	else if($_REQUEST["botao"] == "Apurar"){
					$this->apurar($_REQUEST["idLocal"],$_REQUEST["idEspecialidade"]);
			  	} 
			  	else if($_REQUEST["botao"] == "ATUALIZAR"){
					$this->carregarAcompanhaOnline();
			  	} 
			  	else if($_REQUEST["botao"] == "Imprimir Mapa"){
			  		$this->imprimirMapaApuracao($_REQUEST["idLocal"],$_REQUEST["idEspecialidade"]);
			  	}
			}
		}

		function getMsgErrosArr() {
			return $this->msgErrosArr;
		}

		function setMsgErrosArr($msgErrosArr) {
			$this->msgErrosArr = $msgErrosArr;
		}

		function adicionarMsgErro($msg) {
			if(empty($this->msgErrosArr)){
				$this->msgErrosArr[] = "&bull;&nbsp;".$msg;
			} else {
				$this->msgErrosArr[] = "<br />&bull;&nbsp;".$msg;
			}
		}

		function getMsgSucessosArr() {
			return $this->msgSucessosArr;
		}

		function setMsgSucessosArr($msgSucessosArr) {
			$this->msgSucessosArr = $msgSucessosArr;
		}

		function adicionarMsgSucesso($msg) {
			if(empty($this->msgSucessosArr)){
				$this->msgSucessosArr[] = "&bull;&nbsp;".$msg;
			} else {
				$this->msgSucessosArr[] = "<br />&bull;&nbsp;".$msg;
			}
		}

		function limparMsgGeral() {
			$this->msgSucessosArr = null;
			$this->msgErrosArr = null;
		}

		function getListaCandidatosArr() {
			return $this->listaCandidatos;
		}

		function setListaCandidatosArr($listaCandidatos) {
			$this->listaCandidatos = $listaCandidatos;
		}

		function getListaLocaisVotacaoArr() {
			return $this->listaLocaisVotacao;
		}

		function setListaLocaisVotacaoArr($listaLocaisVotacao) {
			$this->listaLocaisVotacao = $listaLocaisVotacao;
		}

		function getListaGruposEspecialidadesArr() {
			return $this->listaGruposEspecialidades;
		}

		function setListaGruposEspecialidadesArr($listaGruposEspecialidades) {
			$this->listaGruposEspecialidades = $listaGruposEspecialidades;
		}

		function getResultadoArquivo() {
			return $this->resultadoArquivo;
		}

		function setResultadoArquivo($resultadoArquivo) {
			$this->resultadoArquivo = $resultadoArquivo;
		}

		function getApuracao() {
			return $this->apuracao;
		}

		function setApuracao($apuracao) {
			$this->apuracao = $apuracao;
		}

		function getAcompanhaOnline() {
			return $this->acompanhaOnline;
		}

		function setAcompanhaOnline($acompanhaOnline) {
			$this->acompanhaOnline= $acompanhaOnline;
		}

		function carregarListaCandidatos(){
			$dao = new CandidatosDAO();
			$this->setListaCandidatosArr($dao->listarCandidatos());
		}

		function carregarListaLocaisVatacao(){
			$dao = new LocaisVotacaoDAO();
			$this->setListaLocaisVotacaoArr($dao->listarLocaisVotacao());
		}

		function carregarListaGruposEspecialidades(){
			$dao = new GruposEspecialidadesDAO();
			$this->setListaGruposEspecialidadesArr($dao->listarGruposEspecialidades());
		}

		function salvar($idApuracaoloc,$idLocal,$idEspecialidade,$numVotosNulos,$numVotosBrancos,$idApuracaoCadidArr,$qtdVotosCandidArr, $qtdCedulasEnviadas, $qtdCedulasDevolvidas, $qtdCedulasInvalidadas, $numMesaApuracao, $qtdVotantesSessao, $bitStatus) {
			$idApuracaoloc =  AntiSqlInjection($idApuracaoloc);
			if(!empty($idApuracaoloc)){
				$dao = new BaseApuracaoDAO();
				$idLocal =  AntiSqlInjection($idLocal);
				$idEspecialidade =  AntiSqlInjection($idEspecialidade);
				$numVotosNulos =  AntiSqlInjection($numVotosNulos);
				$numVotosBrancos =  AntiSqlInjection($numVotosBrancos);
				$qtdCedulasEnviadas =  AntiSqlInjection($qtdCedulasEnviadas);
				$qtdCedulasDevolvidas =  AntiSqlInjection($qtdCedulasDevolvidas);
				$qtdCedulasInvalidadas =  AntiSqlInjection($qtdCedulasInvalidadas);
				$numMesaApuracao =  AntiSqlInjection($numMesaApuracao);
				$qtdVotantesSessao =  AntiSqlInjection($qtdVotantesSessao);
				$bitStatus=  AntiSqlInjection($bitStatus);

				if($idLocal == "SELECIONAR"){
					$this->adicionarMsgErro("Voc&ecirc; deve selecionar um local para salvar.");
				} else if(!valNumero($idLocal)){
					$this->adicionarMsgErro("Identificador do Local inv&aacute;lido. Voc&ecirc; deve selecionar um local para salvar.");
				}
				if($idEspecialidade == "SELECIONAR"){
					$this->adicionarMsgErro("Voc&ecirc; deve selecionar um Grupo de Especialidade para salvar.");
				} else if(!valNumero($idEspecialidade)){
					$this->adicionarMsgErro("Identificador do Grupo de Especialidade inv&aacute;lido. Voc&ecirc; deve selecionar um Grupo de Especialidade para salvar.");
				}
				if(!valNumero($numVotosNulos)){
					$this->adicionarMsgErro("O campo Votos Nulos aceita apenas n&uacute;meros e entre 0 e 99999.");
				} else if(($numVotosNulos < 0) || ($numVotosNulos > 99999)){
					$this->adicionarMsgErro("O campo Votos Nulos aceita apenas n&uacute;meros entre 0 e 99999.");
				}
				if(!valNumero($numVotosBrancos)){
					$this->adicionarMsgErro("O campo Votos Brancos aceita apenas n&uacute;meros e entre 0 e 99999.");
				} else if(($numVotosBrancos < 0) || ($numVotosBrancos > 99999)){
					$this->adicionarMsgErro("O campo Votos Brancos aceita apenas n&uacute;meros entre 0 e 99999.");
				}
				if(!valNumero($qtdCedulasEnviadas)){
					$this->adicionarMsgErro("O campo Qtd. C&eacute;dulas Enviadas aceita apenas n&uacute;meros e entre 0 e 99999.");
				} else if(($qtdCedulasEnviadas< 0) || ($qtdCedulasEnviadas> 99999)){
					$this->adicionarMsgErro("O campo Qtd. C&eacute;dulas Enviadas aceita apenas n&uacute;meros entre 0 e 99999.");
				}
				if(!valNumero($qtdCedulasDevolvidas)){
					$this->adicionarMsgErro("O campo Qtd. C&eacute;dulas Devolvidas aceita apenas n&uacute;meros e entre 0 e 99999.");
				} else if(($qtdCedulasDevolvidas< 0) || ($qtdCedulasDevolvidas> 99999)){
					$this->adicionarMsgErro("O campo Qtd. C&eacute;dulas Devolvidas aceita apenas n&uacute;meros entre 0 e 99999.");
				}
				if(!valNumero($qtdCedulasInvalidadas)){
					$this->adicionarMsgErro("O campo Qtd. C&eacute;dulas Invalidadas aceita apenas n&uacute;meros e entre 0 e 99999.");
				} else if(($qtdCedulasInvalidadas< 0) || ($qtdCedulasInvalidadas> 99999)){
					$this->adicionarMsgErro("O campo Qtd. C&eacute;dulas Invalidadas aceita apenas n&uacute;meros entre 0 e 99999.");
				}
				if(!valNumero($qtdVotantesSessao)){
					$this->adicionarMsgErro("O campo N. Votantes Sess&atilde;o aceita apenas n&uacute;meros e entre 0 e 99999.");
				} else if(($qtdVotantesSessao< 0) || ($qtdVotantesSessao> 99999)){
					$this->adicionarMsgErro("O campo N. Votantes Sess&atilde;o aceita apenas n&uacute;meros entre 0 e 99999.");
				}
				if(empty($numMesaApuracao)){
					$this->adicionarMsgErro("Favor informar o N&uacute;mero da Mesa de Apura&ccedil;&atilde;o.");
				}
				if(count($idApuracaoCadidArr) != count($qtdVotosCandidArr)){
					$this->adicionarMsgErro("Ocorreu um erro na leitura dos votos dos candidatos. Tente novamente clicar em Apurar novamente e caso persista, favor solicitar suporte ao setor de tecnologia.");
				}
				if(!empty($idApuracaoCadidArr)){
					for($a = 0; $a < count($idApuracaoCadidArr); $a++){
						$idApuracaoCadid = AntiSqlInjection($idApuracaoCadidArr[$a]);
						if(empty($idApuracaoCadid)){
							$this->adicionarMsgErro("Ocorreu um erro de leitura para o candidato ".($a+1)." da listagem, n&atilde;o foi poss&iacute;vel identific&aacute;-lo.");
						} else if(!valNumero($idApuracaoCadid)){
							$this->adicionarMsgErro("Ocorreu um erro de leitura para o candidato ".($a+1)." da listagem, n&atilde;o foi poss&iacute;vel identific&aacute;-lo.");
						}
						$apuracaoCandidato = $dao->consultarCandidatoByApuracaoLocal($idApuracaoCadid);
						$numVotosCandidato = $qtdVotosCandidArr[$a];
						if(!valNumero($numVotosCandidato)){
							$this->adicionarMsgErro("O campo Total de Votos para o Candidato ".strtoupper($apuracaoCandidato->getNome())." (".$apuracaoCandidato->getCRM().") aceita apenas n&uacute;meros e entre 0 e 99999.");
						} else if(($numVotosCandidato < 0) || ($numVotosCandidato > 99999)){
							$this->adicionarMsgErro("Valor inv&aacute;lido para o Total de Votos do Candidato ".strtoupper($apuracaoCandidato->getNome())." (".$apuracaoCandidato->getCRM().") pois o campo aceita apenas n&uacute;meros entre 0 e 99999.");
						}
					}
				}

				if(empty($this->msgErrosArr)){
					$resultadoLocal = $dao->salvarApuracaoLocal($idLocal,$idEspecialidade,$numVotosNulos,$numVotosBrancos, $qtdCedulasEnviadas, $qtdCedulasDevolvidas, $qtdCedulasInvalidadas, $numMesaApuracao, $qtdVotantesSessao, $bitStatus);
					if($resultadoLocal){
						$this->adicionarMsgSucesso("Dados do Local de Vota&ccedil;&atilde;o e Cargo selecionado (Votos Nulos, Votos Brancos, Qtd. C&eacute;dulas Enviadas, Qtd. C&eacute;dulas Devolvidas, Qtd. C&eacute;dulas Invalidadas, N&uacute;mero Mesa de Apura&ccedil;&atilde;o e N&uacute;mero Votantes Sess&atilde;o) salvos com sucesso!");
					} else {
						$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel salvar os dados (Votos Nulos e Votos Brancos, Qtd. C&eacute;dulas Enviadas, Qtd. C&eacute;dulas Devolvidas, Qtd. C&eacute;dulas Invalidadas, N&uacute;mero Mesa de Apura&ccedil;&atilde;o e N&uacute;mero Votantes Sess&atilde;o) do Local de Vota&ccedil;&atilde;o devido a um erro de comunica&ccedil;&atilde;o com o banco de dados.");
					}

					if(!empty($idApuracaoCadidArr)){
						for($a = 0; $a < count($idApuracaoCadidArr); $a++){
							$idApuracaoCadid = AntiSqlInjection($idApuracaoCadidArr[$a]);
							$apuracaoCandidato = $dao->consultarCandidatoByApuracaoLocal($idApuracaoCadid);
							$numVotosCandidato = $qtdVotosCandidArr[$a];
							$resultadoCandidato = $dao->salvarApuracaoCandidato($idApuracaoCadid,$numVotosCandidato);
							if($resultadoCandidato){
								$this->adicionarMsgSucesso("Os votos do Candidato ".strtoupper($apuracaoCandidato->getNome())." (".$apuracaoCandidato->getCRM().") foi salvo com sucesso.");
							} else {
								$this->adicionarMsgErro("N�o foi poss&iacute;vel salvar os votos do Candidato ".strtoupper($apuracaoCandidato->getNome())." (".$apuracaoCandidato->getCRM().")  devido a um erro de comunica&ccedil;&atilde;o com o banco de dados.");
							}
						}
					}
					$this->apurar($idLocal,$idEspecialidade);
				}else{
					$resultado = $dao->carregarApuracao($idLocal, $idEspecialidade);
					$idApuracaoLoc = $resultado->getIdApuracaoLoc();
					if(empty($idApuracaoLoc)){
						$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel recuperar est&aacute; apura&ccedil;&atilde;o, verifique se a Base de Apura&ccedil;&atilde;o foi gerada.");
						$resultado->setIdLocal($idLocal);
						$resultado->setIdEspecialidade($idEspecialidade);
					}
					$resultado->setNumVotosNulos($numVotosNulos);
					$resultado->setNumVotosBrancos($numVotosBrancos);
					$resultado->setQtdCedulasEnviadas($qtdCedulasEnviadas);
					$resultado->setQtdCedulasDevolvidas($qtdCedulasDevolvidas);
					$resultado->setQtdCedulasInvalidadas($qtdCedulasInvalidadas);
					$resultado->setNumMesaApuracao($numMesaApuracao);
					$resultado->setQtdVotantesSessao($qtdVotantesSessao);
					$resultado->setBitStatus($bitStatus);

					$lista = $resultado->getListaCandidatosArr();
					if(!empty($lista)){
						for ($b=0; $b < count($lista); $b++){
							$row = $lista[$b];
							for($c=0; $c < count($idApuracaoCadidArr); $c++){
								$idApuracaoCadid2 = $idApuracaoCadidArr[$c];
								if($row->getIdApuracaoCadid() == $idApuracaoCadid2){
									$row->setQtdVotosCandid($qtdVotosCandidArr[$c]);
								}
							}
						}
						$resultado->setListaCandidatosArr($lista);
					}
					$this->setApuracao($resultado);
				}
			}else{
				$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel Salvar, pois &eacute; necess&aacute;rio Apurar primeiro.");
				$apuracao = new Apuracao();
				$apuracao->setIdLocal($idLocal);
				$apuracao->setIdEspecialidade($idEspecialidade);
				$this->setApuracao($apuracao);
			}
		}

		function apurar($idLocal, $idEspecialidade) {
			$dao = new BaseApuracaoDAO();
			$apuracao = new Apuracao();
			$idLocal =  AntiSqlInjection($idLocal);
			$idEspecialidade =  AntiSqlInjection($idEspecialidade);

			if(empty($idLocal) || empty($idEspecialidade)){
				$this->adicionarMsgErro("Para apurar, informe o Local de Vota&ccedil;&atilde;o e o Grupo de Especialidade!");
			}

			if($idLocal == "SELECIONAR"){
				$this->adicionarMsgErro("Favor informar um Local de Vota&ccedil;&atilde;o.");
			}

			if($idEspecialidade == "SELECIONAR"){
				$this->adicionarMsgErro("Favor informar um Cargo.");
			}

			if(empty($this->msgErrosArr)){
				$resultado = $dao->carregarApuracao($idLocal, $idEspecialidade);
				$idApuracaoLoc = $resultado->getIdApuracaoLoc();
				if(empty($idApuracaoLoc)){
					$this->adicionarMsgErro("N&atilde;o foi poss&iacute;vel recuperar est&aacute; apura&ccedil;&atilde;o, verifique se a Base de Apura&ccedil;&atilde;o foi gerada.");
					$resultado->setIdLocal($idLocal);
					$resultado->setIdEspecialidade($idEspecialidade);
				}
				$this->setApuracao($resultado);
			}else{
				$apuracao->setIdLocal($idLocal);
				$apuracao->setIdEspecialidade($idEspecialidade);
				$this->setApuracao($apuracao);
			}

		}


		function imprimirMapaApuracao($idLocal,$idEspecialidade){
			$apuracao = new Apuracao();
			if($idLocal=="SELECIONAR"){
				$this->adicionarMsgErro("Favor informar um Local de Vota&ccedil;&atilde;o.");
			}else{
				$URL = "/apuracao/relResultadoUrnaLocal.php?idLocal=".$_REQUEST["idLocal"];
				popup($URL,'Relat�rio: Mapa de Apura��o','menubar=no,width=800,height=600');
			}
			$apuracao->setIdLocal($idLocal);
			$apuracao->setIdEspecialidade($idEspecialidade);
			$this->setApuracao($apuracao);
		}

		function carregarAcompanhaOnline(){
			$dao = new BaseApuracaoDAO();
			$this->setAcompanhaOnline($dao->consultarSituacaoApuracao());
		}
	}
?>