-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: uniawsdba0004.aws.unimedbh.com.br
-- Generation Time: 06-Mar-2018 às 21:30
-- Versão do servidor: 5.6.27-log
-- PHP Version: 5.6.29-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `unimedbh_wpuApuracao`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `AP_APURACAO_CANDID`
--

CREATE TABLE `AP_APURACAO_CANDID` (
  `ID_APURACAO_CADID` int(10) NOT NULL,
  `ID_APURACAO_LOC` int(10) NOT NULL,
  `CRM` int(6) NOT NULL,
  `QTD_VOTOS_CANDID` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Cadastro das apuracoes por local / Especialidade /Candidato.';

--
-- Extraindo dados da tabela `AP_APURACAO_CANDID`
--

INSERT INTO `AP_APURACAO_CANDID` (`ID_APURACAO_CADID`, `ID_APURACAO_LOC`, `CRM`, `QTD_VOTOS_CANDID`) VALUES
(1467, 491, 1, 28),
(1468, 491, 2, 24),
(1469, 491, 22212, 0),
(1470, 492, 1, 107),
(1471, 492, 2, 106),
(1472, 492, 22212, 0),
(1473, 493, 1, 494),
(1474, 493, 2, 514),
(1475, 493, 22212, 0),
(1476, 494, 1, 41),
(1477, 494, 2, 23),
(1478, 494, 22212, 0),
(1479, 495, 1, 40),
(1480, 495, 2, 21),
(1481, 495, 22212, 0),
(1482, 496, 1, 25),
(1483, 496, 2, 27),
(1484, 496, 22212, 0),
(1485, 497, 1, 11),
(1486, 497, 2, 3),
(1487, 497, 22212, 0),
(1488, 498, 1, 18),
(1489, 498, 2, 5),
(1490, 498, 22212, 0),
(1491, 499, 1, 3),
(1492, 499, 2, 2),
(1493, 499, 22212, 0),
(1494, 500, 1, 37),
(1495, 500, 2, 30),
(1496, 500, 22212, 0),
(1497, 501, 1, 117),
(1498, 501, 2, 88),
(1499, 501, 22212, 0),
(1500, 502, 1, 7),
(1501, 502, 2, 6),
(1502, 502, 22212, 0),
(1503, 503, 1, 35),
(1504, 503, 2, 15),
(1505, 503, 22212, 0),
(1506, 504, 1, 20),
(1507, 504, 2, 26),
(1508, 504, 22212, 0),
(1509, 505, 1, 51),
(1510, 505, 2, 27),
(1511, 505, 22212, 0),
(1512, 506, 1, 13),
(1513, 506, 2, 15),
(1514, 506, 22212, 0),
(1515, 507, 1, 114),
(1516, 507, 2, 49),
(1517, 507, 22212, 0),
(1518, 508, 1, 58),
(1519, 508, 2, 41),
(1520, 508, 22212, 0),
(1521, 509, 1, 15),
(1522, 509, 2, 9),
(1523, 509, 22212, 0),
(1524, 510, 1, 23),
(1525, 510, 2, 10),
(1526, 510, 22212, 0),
(1527, 511, 1, 60),
(1528, 511, 2, 25),
(1529, 511, 22212, 0),
(1530, 512, 1, 22),
(1531, 512, 2, 31),
(1532, 512, 22212, 0),
(1533, 513, 1, 29),
(1534, 513, 2, 12),
(1535, 513, 22212, 0),
(1536, 514, 1, 78),
(1537, 514, 2, 70),
(1538, 514, 22212, 0),
(1539, 515, 1, 2),
(1540, 515, 2, 4),
(1541, 515, 22212, 0),
(1542, 516, 1, 36),
(1543, 516, 2, 34),
(1544, 516, 22212, 0),
(1545, 517, 1, 6),
(1546, 517, 2, 2),
(1547, 517, 22212, 0),
(1548, 518, 1, 35),
(1549, 518, 2, 32),
(1550, 518, 22212, 0),
(1551, 519, 1, 19),
(1552, 519, 2, 16),
(1553, 519, 22212, 0),
(1554, 520, 1, 18),
(1555, 520, 2, 14),
(1556, 520, 22212, 0),
(1557, 521, 1, 27),
(1558, 521, 2, 37),
(1559, 521, 22212, 0),
(1560, 522, 1, 29),
(1561, 522, 2, 17),
(1562, 522, 22212, 0),
(1563, 523, 1, 3),
(1564, 523, 2, 2),
(1565, 523, 22212, 0),
(1566, 524, 1, 2),
(1567, 524, 2, 1),
(1568, 524, 22212, 0),
(1569, 525, 1, 18),
(1570, 525, 2, 6),
(1571, 525, 22212, 0),
(1572, 526, 1, 51),
(1573, 526, 2, 29),
(1574, 526, 22212, 0),
(1575, 527, 1, 32),
(1576, 527, 2, 34),
(1577, 527, 22212, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `AP_APURACAO_LOCAL`
--

CREATE TABLE `AP_APURACAO_LOCAL` (
  `ID_APURACAO_LOC` int(10) NOT NULL,
  `ID_LOCAL` int(10) NOT NULL,
  `ID_ESPECIALIDADE` int(10) NOT NULL,
  `QTD_VOTOS_NULOS` int(10) NOT NULL DEFAULT '0',
  `QTD_VOTOS_BRANCOS` int(10) NOT NULL DEFAULT '0',
  `QTD_CEDULAS_ENVIADAS` int(10) NOT NULL DEFAULT '0',
  `QTD_CEDULAS_DEVOLVIDAS` int(10) NOT NULL DEFAULT '0',
  `QTD_CEDULAS_INVALIDADAS` int(10) NOT NULL DEFAULT '0',
  `NUM_MESA_APURACAO` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `QTD_VOTANTES_SESSAO` int(10) NOT NULL DEFAULT '0',
  `BIT_STATUS` int(10) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Cadastro das apuracoes por local / Especialidade.';

--
-- Extraindo dados da tabela `AP_APURACAO_LOCAL`
--

INSERT INTO `AP_APURACAO_LOCAL` (`ID_APURACAO_LOC`, `ID_LOCAL`, `ID_ESPECIALIDADE`, `QTD_VOTOS_NULOS`, `QTD_VOTOS_BRANCOS`, `QTD_CEDULAS_ENVIADAS`, `QTD_CEDULAS_DEVOLVIDAS`, `QTD_CEDULAS_INVALIDADAS`, `NUM_MESA_APURACAO`, `QTD_VOTANTES_SESSAO`, `BIT_STATUS`) VALUES
(491, 1, 1, 3, 0, 200, 145, 0, '1', 55, 1),
(492, 2, 1, 4, 3, 600, 379, 1, '2', 220, 1),
(493, 3, 1, 34, 16, 1500, 435, 7, '1', 1058, 1),
(494, 4, 1, 0, 2, 200, 134, 0, '3', 66, 1),
(495, 5, 1, 1, 0, 200, 138, 0, '5', 62, 1),
(496, 6, 1, 2, 1, 300, 245, 0, '6', 55, 1),
(497, 7, 1, 0, 0, 150, 136, 0, '4', 14, 1),
(498, 8, 1, 0, 0, 100, 76, 1, '1', 23, 1),
(499, 9, 1, 0, 0, 100, 95, 0, '6', 5, 1),
(500, 10, 1, 1, 1, 200, 131, 0, '3', 69, 1),
(501, 11, 1, 2, 2, 400, 190, 1, '4', 209, 1),
(502, 12, 1, 0, 0, 100, 85, 2, '5', 13, 1),
(503, 13, 1, 4, 1, 300, 245, 0, '1', 55, 1),
(504, 14, 1, 1, 0, 200, 153, 0, '6', 47, 1),
(505, 15, 1, 1, 0, 300, 221, 0, '2', 79, 1),
(506, 16, 1, 1, 1, 150, 119, 1, '5', 30, 1),
(507, 17, 1, 2, 1, 500, 332, 2, '3', 166, 1),
(508, 18, 1, 4, 0, 451, 342, 6, '1', 103, 1),
(509, 19, 1, 0, 0, 100, 76, 0, '6', 24, 1),
(510, 20, 1, 0, 1, 150, 116, 0, '2', 34, 1),
(511, 21, 1, 1, 1, 300, 213, 0, '4', 87, 1),
(512, 22, 1, 0, 0, 200, 146, 1, '5', 53, 1),
(513, 23, 1, 0, 0, 150, 109, 0, '6', 41, 1),
(514, 24, 1, 6, 1, 500, 344, 1, '1', 155, 1),
(515, 25, 1, 0, 0, 100, 94, 0, '2', 6, 1),
(516, 26, 1, 2, 2, 300, 226, 0, '3', 74, 1),
(517, 27, 1, 0, 0, 100, 92, 0, '6', 8, 1),
(518, 28, 1, 3, 0, 250, 180, 0, '4', 70, 1),
(519, 29, 1, 0, 0, 150, 114, 1, '2', 35, 1),
(520, 30, 1, 1, 0, 150, 117, 0, '5', 33, 1),
(521, 31, 1, 1, 0, 250, 185, 0, '6', 65, 1),
(522, 32, 1, 2, 1, 250, 201, 0, '4', 49, 1),
(523, 33, 1, 0, 0, 50, 45, 0, '5', 5, 1),
(524, 34, 1, 1, 0, 50, 46, 0, '2', 4, 1),
(525, 35, 1, 1, 0, 100, 75, 0, '3', 25, 1),
(526, 38, 1, 1, 0, 350, 268, 1, '6', 81, 1),
(527, 39, 1, 0, 0, 150, 84, 0, '5', 66, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `AP_CANDIDATO`
--

CREATE TABLE `AP_CANDIDATO` (
  `CRM` int(6) NOT NULL,
  `NOME` varchar(100) CHARACTER SET latin1 NOT NULL,
  `DAT_COOPERACAO` date DEFAULT NULL,
  `DAT_NASCIMENTO` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Cadastro dos Candidatos';

--
-- Extraindo dados da tabela `AP_CANDIDATO`
--

INSERT INTO `AP_CANDIDATO` (`CRM`, `NOME`, `DAT_COOPERACAO`, `DAT_NASCIMENTO`) VALUES
(1, 'ExperiÃªncia e ConfiancÌ§a', '2014-01-01', '2014-01-01'),
(2, 'Unimed de Todos NÃ³s', '2014-01-01', '2014-01-01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `AP_CANDIDATO_ESPECIALIDADE`
--

CREATE TABLE `AP_CANDIDATO_ESPECIALIDADE` (
  `ID_CANDIT_ESPEC` int(10) NOT NULL,
  `ID_ESPECIALIDADE` int(10) NOT NULL,
  `CRM` int(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Cadastro de vinculo dos Grupos de Especialidade e seus candi';

--
-- Extraindo dados da tabela `AP_CANDIDATO_ESPECIALIDADE`
--

INSERT INTO `AP_CANDIDATO_ESPECIALIDADE` (`ID_CANDIT_ESPEC`, `ID_ESPECIALIDADE`, `CRM`) VALUES
(1, 1, 1),
(2, 1, 2),
(6, 1, 22212);

-- --------------------------------------------------------

--
-- Estrutura da tabela `AP_ESPECIALIDADE`
--

CREATE TABLE `AP_ESPECIALIDADE` (
  `ID_ESPECIALIDADE` int(10) NOT NULL,
  `DES_ESPECIALIDADE` varchar(100) CHARACTER SET latin1 NOT NULL,
  `QTD_VAGAS` varchar(10) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Cadastro dos Grupos de Especialidade com a quantidade de vag';

--
-- Extraindo dados da tabela `AP_ESPECIALIDADE`
--

INSERT INTO `AP_ESPECIALIDADE` (`ID_ESPECIALIDADE`, `DES_ESPECIALIDADE`, `QTD_VAGAS`) VALUES
(1, 'ELEIÃ‡ÃƒO CONSELHO FISCAL', '1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `AP_LOCAL`
--

CREATE TABLE `AP_LOCAL` (
  `ID_LOCAL` int(11) NOT NULL,
  `DES_LOCAL` varchar(100) CHARACTER SET latin1 NOT NULL,
  `CIDADE` varchar(20) CHARACTER SET latin1 NOT NULL,
  `QTD_PRESENTES` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `AP_LOCAL`
--

INSERT INTO `AP_LOCAL` (`ID_LOCAL`, `DES_LOCAL`, `CIDADE`, `QTD_PRESENTES`) VALUES
(1, 'URNA 01 - GRCO - GestÃ£o de Relacionamento com Cooperados - Av, Contorno, 4265', 'BELO HORIZONTE', 55),
(2, 'URNA 02 - Unimed-BH - Rua MaranhÃ£o, 602', 'BELO HORIZONTE', 220),
(3, 'URNA 03 - Minascentro - Av. Augusto de Lima, 785', 'BELO HORIZONTE', 1058),
(4, 'URNA 04 - Centro de PromoÃ§Ã£o da SaÃºde Barro Preto - Rua Paracatu, 600', 'BELO HORIZONTE', 66),
(5, 'URNA 05 - Centro de PromoÃ§Ã£o da SaÃºde Contagem - Av. Babita Camargo, 1695', 'CONTAGEM', 62),
(6, 'URNA 06 - Centro de PromoÃ§Ã£o da SaÃºde Pedro I - Av. Dom Pedro I, 2840', 'BELO HORIZONTE', 55),
(7, 'URNA 07 - Centro OftamolÃ³gico - Rua Santa Catarina, 941', 'BELO HORIZONTE', 14),
(8, 'URNA 08 - Clinica CEMEL - Av. JosÃ© Faria da Rocha, 4133', 'CONTAGEM', 23),
(9, 'URNA 09 - Clinica Eldorado - Av. JosÃ© Faria da Rocha, 4257', 'CONTAGEM', 5),
(10, 'URNA 10 - Credicom PAC Hospital Madre Teresa - Av. Raja Gabaglia, 1002', 'BELO HORIZONTE', 69),
(11, 'URNA 11 - Credicom PAC SaÃºde - Facul. de Medicina UFMG - Av. Prof. Alfredo Balena, 190', 'BELO HORIZONTE', 209),
(12, 'URNA 12 - FundaÃ§Ã£o Vespasianense de SaÃºde - Rua Mateus Vercesi, 30', 'VESPASIANO', 13),
(13, 'URNA 13 - Hospital Unimed Betim - Av. Governador Valadares, 619', 'BETIM', 55),
(14, 'URNA 14 - Hospital Biocor - Alameda da Serra, 217', 'NOVA LIMA', 47),
(15, 'URNA 15 - Maternidade Unimed GrajaÃº - Rua ViamÃ£o, 1171', 'BELO HORIZONTE', 79),
(16, 'URNA 16 - Hospital e Maternidade Santa Rita - Av. Tito FugÃªncio, 1045', 'CONTAGEM', 30),
(17, 'URNA 17 - Hospital Felicio Rocho - Av. Contorno, 9530', 'BELO HORIZONTE', 166),
(18, 'URNA 18 - Hospital Lifecenter - Av. Contorno, 4747', 'BELO HORIZONTE', 103),
(19, 'URNA 19 - Hopital Luxemburgo - Rua Gentios, 1350', 'BELO HORIZONTE', 24),
(20, 'URNA 20 - Hospital SÃ£o Camilo - Av. Silviano BrandÃ£o, 1600', 'BELO HORIZONTE', 34),
(21, 'URNA 21 - Hospital SÃ£o Lucas - Av. Francisco Sales, 1186', 'BELO HORIZONTE', 87),
(22, 'URNA 22 - Hospital Semper - Alameda Ezequiel Dias, 389', 'BELO HORIZONTE', 53),
(23, 'URNA 23 - Hospital Socor - Av. do Contorno, 10500', 'BELO HORIZONTE', 41),
(24, 'URNA 24 - Hospital Unimed Contorno - Av. do Contorno, 3097', 'BELO HORIZONTE', 155),
(25, 'URNA 25 - Hospital UniversitÃ¡rio SÃ£o JosÃ© - Rua AimorÃ©s, 2896', 'BELO HORIZONTE', 6),
(26, 'URNA 26 - Hospital Vera Cruz - Av. Barbacena, 653', 'BELO HORIZONTE', 74),
(27, 'URNA 27 - CMHI Centro MÃ©dico Hospitalar Integrado - Rua PiauÃ­, 2007', 'BELO HORIZONTE', 8),
(28, 'URNA 28 - Maternidade Otaviano Neves - Rua CearÃ¡, 186', 'BELO HORIZONTE', 70),
(29, 'URNA 29 - Maternidade Santa FÃ© - Rua Pouso Alegre, 2111', 'BELO HORIZONTE', 35),
(30, 'URNA 30 - Centro de PromoÃ§Ã£o da SaÃºde Barreiro - Av. SinfrÃ´nio Brochado, 674', 'BELO HORIZONTE', 33),
(31, 'URNA 31 - Centro de PromoÃ§Ã£o da SaÃºde Av. Brasil - Av. Brasil, 888', 'BELO HORIZONTE', 65),
(32, 'URNA 32 - Centro de PromoÃ§Ã£o da SaÃºde GonÃ§alves Dias - Rua GonÃ§alves Dias, 201', 'BELO HORIZONTE', 49),
(33, 'URNA 33 - Loja Unimed-BH Santa Barbara - Av. Castelo Branco, 240', 'SANTA BÃRBARA', 5),
(34, 'URNA 34 - Loja Unimed-BH Lagoa Santa - Av. AcadÃªmico Nilo Figueiredo, 92', 'LAGOA SANTA', 4),
(35, 'URNA 35 - Unimed Pedro Leopoldo - Rua Comendador AntÃ´nio Alves, 186', 'PEDRO LEOPOLDO', 25),
(38, 'URNA 36 - Centro MÃ©dico Unimed - Rua Inconfidentes, 68 - FuncionÃ¡rios - BH', 'BELO HORIZONTE', 81),
(39, 'URNA 37 - Credicom PAC Nova Lima - Rua da Paisagem, Â 240 - Vila da Serra', 'NOVA LIMA', 66);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `AP_APURACAO_CANDID`
--
ALTER TABLE `AP_APURACAO_CANDID`
  ADD PRIMARY KEY (`ID_APURACAO_CADID`),
  ADD KEY `FK_AP_APURACAO_CANDID_ID_APURACAO_LOC` (`ID_APURACAO_LOC`),
  ADD KEY `FK_AP_APURACAO_CANDID_CRM` (`CRM`);

--
-- Indexes for table `AP_APURACAO_LOCAL`
--
ALTER TABLE `AP_APURACAO_LOCAL`
  ADD PRIMARY KEY (`ID_APURACAO_LOC`),
  ADD KEY `FK_AP_APURACAO_LOCAL_ID_LOCAL` (`ID_LOCAL`),
  ADD KEY `FK_AP_APURACAO_LOCAL_ID_ESPECIALIDADE` (`ID_ESPECIALIDADE`);

--
-- Indexes for table `AP_CANDIDATO`
--
ALTER TABLE `AP_CANDIDATO`
  ADD PRIMARY KEY (`CRM`);

--
-- Indexes for table `AP_CANDIDATO_ESPECIALIDADE`
--
ALTER TABLE `AP_CANDIDATO_ESPECIALIDADE`
  ADD PRIMARY KEY (`ID_CANDIT_ESPEC`),
  ADD KEY `FK_AP_CANDIDATO_ESPECIALIDADE_ID_ESPECIALIDADE` (`ID_ESPECIALIDADE`),
  ADD KEY `FK_AP_CANDIDATO_ESPECIALIDADE_CRM` (`CRM`);

--
-- Indexes for table `AP_ESPECIALIDADE`
--
ALTER TABLE `AP_ESPECIALIDADE`
  ADD PRIMARY KEY (`ID_ESPECIALIDADE`);

--
-- Indexes for table `AP_LOCAL`
--
ALTER TABLE `AP_LOCAL`
  ADD PRIMARY KEY (`ID_LOCAL`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `AP_APURACAO_CANDID`
--
ALTER TABLE `AP_APURACAO_CANDID`
  MODIFY `ID_APURACAO_CADID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1578;
--
-- AUTO_INCREMENT for table `AP_APURACAO_LOCAL`
--
ALTER TABLE `AP_APURACAO_LOCAL`
  MODIFY `ID_APURACAO_LOC` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=528;
--
-- AUTO_INCREMENT for table `AP_CANDIDATO_ESPECIALIDADE`
--
ALTER TABLE `AP_CANDIDATO_ESPECIALIDADE`
  MODIFY `ID_CANDIT_ESPEC` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `AP_ESPECIALIDADE`
--
ALTER TABLE `AP_ESPECIALIDADE`
  MODIFY `ID_ESPECIALIDADE` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `AP_LOCAL`
--
ALTER TABLE `AP_LOCAL`
  MODIFY `ID_LOCAL` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
