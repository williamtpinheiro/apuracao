<?php
require_once($_SERVER['DOCUMENT_ROOT']."/apuracao/business/LocaisVotacaoBO.php"); 

  $acao = isset($_REQUEST["action"]) ? $_REQUEST["action"] : ""; 
  $acao = isset($acao) ? $_REQUEST["botao"] : $acao;
  $bo = new LocaisVotacaoBO($acao);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include_once('head.php');?>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="gerenciaBotoes()">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
      <tr>
        <td colspan="2"><?php include_once('cabecalho.php');?></td>
      </tr>
      <tr>
        <td width="210" valign="top"><?php include_once('menu.php');?></td>
        <td valign="top">
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="form" name="form" enctype="multipart/form-data" onsubmit="return OnSubmitForm();" >
        <table width="99%" border="0" cellspacing="0" cellpadding="0">
	        <tr>
				<td height="10">&nbsp;</td>
			</tr>
			<tr>
				<td class="TituloSecao"><img src="images/spacer.gif" width="15"									border="0" alt="">GERIR LOCAIS DE VOTAÇÃO
				</td>
			</tr>
			<tr>
				<td height="10">&nbsp;</td>
			</tr>
            <tr>
				<td height="10"> 
				<?php 
					$msgSucessos = $bo->getMsgSucessosArr(); 
					if (!empty($msgSucessos)){
				?>
            <div class="MensagemSucesso">
            	<?php 
					foreach ($msgSucessos as $row) {
						echo $row;
					}
				?>
			</div>
            	<?php 
					} else { echo "&nbsp;"; }
					$msgErros = $bo->getMsgErrosArr();
					if(!empty($msgErros)){
				?>
            <div class="MensagemErro">
				<?php 
					foreach ($msgErros as $row) {
						echo $row;
					}
				?>
			</div>
            <?php 
					} else { echo "&nbsp;"; }
			?>
</td>
			</tr>
            <tr>
				<td height="10">&nbsp;</td>
			</tr>
			<tr>
			  <td valign="top">
              
              <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td>
                  
                  <?php 
				  
				  	if($bo->getLocalVotacao() != null) { 
						$registro = $bo->getLocalVotacao();	
				  ?>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td>Local <span class="campoObrigatorio">*</span>
                        <input type="hidden" name="id" id="id" value="<?php echo $registro->getIdLocal(); ?>" />
                      <input type="hidden" name="action" id="action" value="<?php echo $acao; ?>" />
                     </td>
                    </tr>
                    <tr>
                      <td><input name="desLocal" type="text" id="desLocal" size="60" maxlength="100" value="<?php echo $registro->getDesLocal(); ?>" /></td>
                    </tr>
                    <tr>
                      <td>Cidade <span class="campoObrigatorio">*</span><img src="images/spacer.gif" width="1" height="20"									border="0" alt=""></td>
                    </tr>
                    <tr>
                      <td><select name="cidade" id="cidade" >
                        <option>SELECIONAR</option>
                        <option value="BELO HORIZONTE" <?php if($registro->getCidade() == "BELO HORIZONTE") echo "selected='selected'";?>>BELO HORIZONTE</option>
                        <option value="BETIM" <?php if($registro->getCidade() == "BETIM") echo "selected='selected'";?>>BETIM</option>                        
                        <option value="CONTAGEM" <?php if($registro->getCidade() == "CONTAGEM") echo "selected='selected'";?>>CONTAGEM</option>
                        <option value="LAGOA SANTA" <?php if($registro->getCidade() == "LAGOA SANTA") echo "selected='selected'";?>>LAGOA SANTA</option>
                        <option value="NOVA LIMA" <?php if($registro->getCidade() == "NOVA LIMA") echo "selected='selected'";?>>NOVA LIMA</option>
                        <option value="PEDRO LEOPOLDO" <?php if($registro->getCidade() == "PEDRO LEOPOLDO") echo "selected='selected'";?>>PEDRO LEOPOLDO</option>
                        <option value="SANTA BÁRBARA" <?php if($registro->getCidade() == "SANTA BÁRBARA") echo "selected='selected'";?>>SANTA BÁRBARA</option>
                        <option value="SANTA LUZIA" <?php if($registro->getCidade() == "SANTA LUZIA") echo "selected='selected'";?>>SANTA LUZIA</option>
                        <option value="VESPASIANO"  <?php if($registro->getCidade() == "VESPASIANO") echo "selected='selected'";?>>VESPASIANO</option>
                      </select></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                  <?php } else { ?>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td>Local <span class="campoObrigatorio">*</span><img src="images/spacer.gif" width="1" height="20"									border="0" alt="">
					 <input type="hidden" name="action" id="action" value="<?php echo $acao; ?>" /></td>
                    </tr>
                    <tr>
                      <td><input name="desLocal" type="text" id="desLocal" size="60" maxlength="100" /></td>
                    </tr>
                    <tr>
                      <td>Cidade <span class="campoObrigatorio">*</span><img src="images/spacer.gif" width="1" height="20" border="0" alt=""></td>
                    </tr>
                    <tr>
                      <td><select name="cidade" id="cidade" >
                        <option>SELECIONAR</option>
                        <option value="BELO HORIZONTE">BELO HORIZONTE</option>
                        <option value="BETIM">BETIM</option>
                        <option value="CONTAGEM">CONTAGEM</option>
                        <option value="LAGOA SANTA">LAGOA SANTA</option>
                        <option value="NOVA LIMA">NOVA LIMA</option>
                        <option value="PEDRO LEOPOLDO">PEDRO LEOPOLDO</option>
                        <option value="SANTA BÁRBARA">SANTA BÁRBARA</option>
                        <option value="SANTA LUZIA">SANTA LUZIA</option>
                        <option value="VESPASIANO">VESPASIANO</option>
                      </select></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                  
                  <?php } ?>
                  
                  </td>
                </tr>
                <tr>
                  <td align="right"><input type="submit" name="botao" id="btnIncluir" value="Incluir" class="BotaoMaior" />
                    <input type="submit" name="botao" id="btnConsultar" value="Consultar" class="BotaoMaior" />
                    <input type="submit" name="botao" id="btnAlterar" value="Alterar" class="BotaoMaior" />
                  <input type="submit" name="botao" id="btnExcluir" value="Excluir" class="BotaoMaior" onclick="document.pressed=this.value" /></td>
                </tr>
                <tr>
                  <td align="right">&nbsp;</td>
                </tr>
              </table>
              
              <br /></td>
		  </tr>
			<tr>
			  <td valign="top">
              	<fieldset >
			      <legend>Locais Cadastrados </legend>
                  <?php 
				  		$lista = $bo->getListaLocaisVotacaoArr();
						if(!empty($lista)){
				  ?>
			      <table class="paginador" width="98%">
							<tr>
								<td width="66%" class="header">Local</td>
								<td width="34%" class="header">Cidade</td>
							</tr>					    
							
							<?php
                            	foreach ($lista as $row) {
							?>
			                  <tr onMouseOver="this.className='selecionada'" onMouseOut="this.className='inicial'">
									<td class="body"><a href="gerirLocaisVotacao.php?action=edit&id=<?php echo $row->getIdLocal(); ?>" class="TabelaLink"><?php echo $row->getDesLocal(); ?></a></td>
									<td class="body"><a href="gerirLocaisVotacao.php?action=edit&id=<?php echo $row->getIdLocal(); ?>" class="TabelaLink"><?php echo $row->getCidade(); ?></a></td>
					</tr>
                    		<?php } ?>
			    </table>
                <table>
                	<tr>
                    	<td>
					    <input type="submit" name="btnPrimeira" id="btnPrimeira" value="" onclick="document.pressed='Primeira'" class="PaginacaoPrimeira"/>&nbsp;
                        <input type="submit" name="btnAnterior" id="btnAnterior" value="" onclick="document.pressed='Anterior'" class="PaginacaoAnterior"/>&nbsp;
                        <td valign="middle">
                        <input type="hidden" id="paginaAtual" name="paginaAtual" value="<?php echo $bo->getPaginaAtual(); ?>"/><?php echo $bo->getPaginaAtual(); ?>  / <?php echo $bo->getNumPaginas(); ?> <input type="hidden" id="totalPaginas" name="totalPaginas" value="<?php echo $bo->getNumPaginas(); ?>"/> &nbsp;
                        </td>
                        <td>
                        <input type="submit" name="btnPosterior" id="btnPosterior" value="" onclick="document.pressed='Posterior'" class="PaginacaoPosterior"/>&nbsp;
                        <input type="submit" name="btnUltima" id="btnUltima" value="" onclick="document.pressed='Ultima';" class="PaginacaoUltima"/>
                  	<?php 
				  		} else {
							if($_REQUEST["botao"] != "Consultar"){
								echo "&nbsp;&nbsp;&nbsp;N&atilde;o foram encontrados registros neste cadastro.";
							} else {
								echo "&nbsp;&nbsp;&nbsp;N&atilde;o foram encontrados registros nesta consulta.";
							}
						}	
					?>
                    	</td>
                    </tr>
                </table>
					</fieldset>
</td>
		  </tr>
		</table>
		</form>
        </td>
      </tr>
      <tr>
        <td colspan="2"><?php include_once('rodape.php');?></td>
      </tr>
</table>

</body>
</html>